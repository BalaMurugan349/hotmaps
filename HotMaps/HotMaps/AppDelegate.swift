//
//  AppDelegate.swift
//  HotMaps
//
//  Created by Srishti Innovative on 29/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var addStatusBar: UIView!
    var dictNotification : NSDictionary!
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        //var st = UIStoryboard(name: "Main", bundle: nil)
        
        let thumbImage = UIImage(named: "sliderHandleUnselected")
        UISlider.appearance().setThumbImage(thumbImage, forState: UIControlState.Normal)
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
      // self.createMenuView()
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor.whiteColor()
        navigationBarAppearace.barTintColor = UIColor.blackColor()

        
//        if #available(iOS 8.0, *) {
////            var types: UIUserNotificationType = UIUserNotificationType.Badge ||
////                UIUserNotificationType.Alert ||
////                UIUserNotificationType.Sound
//            let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge], categories: nil)
//            application.registerUserNotificationSettings(settings)
//            application.registerForRemoteNotifications()
//        }
//        
////        var settings: UIUserNotificationSettings = UIUserNotificationSettings( forTypes: types, categories: nil )
//        if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
//            dictNotification = remoteNotification["aps"] as! NSDictionary
//            showProfilePage()
//                return true
//        }
        if #available(iOS 10.0, *){
            
            UNUserNotificationCenter.currentNotificationCenter().delegate = self
            UNUserNotificationCenter.currentNotificationCenter().requestAuthorizationWithOptions([.Badge, .Sound, .Alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    UIApplication.sharedApplication().registerForRemoteNotifications()
                }
                else{
                    //Do stuff if unsuccessful...
                }
            })
        }
            
        else{ //If user is not on iOS 10 use the old methods we've been using
            let notificationSettings = UIUserNotificationSettings(
                forTypes: [.Badge, .Sound, .Alert], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
            
        }


        if NSUserDefaults.standardUserDefaults().boolForKey("showMapPage") == false {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "showMapPage")
            NSUserDefaults.standardUserDefaults().synchronize()
            showSignupPage(false)
        }else{
            checkAutoLogin()

        }
        createViewInStatusBar()
        
        return true
    }

    func checkAutoLogin(){
        //User already logged in
        if (NSUserDefaults.standardUserDefaults().objectForKey("CurrentUserDetails") != nil){
            let dict:Dictionary<String, String> = NSKeyedUnarchiver.unarchiveObjectWithData(NSUserDefaults.standardUserDefaults().objectForKey("CurrentUserDetails") as! NSData) as! Dictionary<String, String>
             User.userWithDetails(dict)
            
            if (NSUserDefaults.standardUserDefaults().objectForKey("CurrentScore") != nil){
                User.currentUser().points = NSUserDefaults.standardUserDefaults().objectForKey("CurrentScore") as! String
            }
            
            didLogin()
        }
        //User not logged in
        else{
           self.createMenuView()
       }
        
    }
    func createViewInStatusBar()
    {
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        if(addStatusBar == nil)
        {
            addStatusBar = UIView()
            addStatusBar.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 20);
            addStatusBar.backgroundColor = UIColor.blackColor()
        }
        //UIColor(red: 35/255.0, green: 34/255.0, blue: 35/255.0, alpha: 1.0)
        self.window?.rootViewController?.view .addSubview(addStatusBar)
        
    }
    func didLogin(){
        self.createMenuView()
    }
    
    func showSignupPage(showBackButton : Bool)  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("SignUpViewController") as! SignUpTableViewController
        mainViewController.showMapPage = true
        mainViewController.showBackButton = showBackButton
        let navigationController = UINavigationController(rootViewController: mainViewController)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    
    func didLogout(showCancel: Bool){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginTableViewController
        mainViewController.showCancelBarButton = showCancel
        let navigationController = UINavigationController(rootViewController: mainViewController)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
       
        if (NSUserDefaults.standardUserDefaults().objectForKey("CurrentUserDetails") != nil){
        NSUserDefaults.standardUserDefaults().removeObjectForKey("CurrentUserDetails")
        NSUserDefaults.standardUserDefaults().synchronize()
        }
        if (NSUserDefaults.standardUserDefaults().objectForKey("CurrentScore") != nil){
            NSUserDefaults.standardUserDefaults().removeObjectForKey("CurrentScore")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        User.logout()
    }
    
    
     func createMenuView() {
        //create SlideMenuController
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
       
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
        mainViewController.showLeftMenu = true
        let navigationController = UINavigationController(rootViewController: mainViewController)

        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenuViewController") as! LeftMenuViewController
        leftViewController.mapViewController = navigationController
        
        let slideMenuController = SlideMenuController(mainViewController: navigationController, leftMenuViewController: leftViewController)

        
      //  self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }

    func showProfilePage(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let profilevc = storyboard.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
        profilevc.userId = dictNotification["id"] as! String
        profilevc.followStatus = dictNotification["follow_status"] as! String
        profilevc.userName = dictNotification["username"] as! String
        profilevc.isFromPushNotification = true
        let navigationController = UINavigationController(rootViewController: profilevc)
        
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenuViewController") as! LeftMenuViewController
        leftViewController.mapViewController = navigationController
        
        let slideMenuController = SlideMenuController(mainViewController: navigationController, leftMenuViewController: leftViewController)
        
        //  self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()

    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {

        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    func application( application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData ) {
        
        var tokenStr = deviceToken.description
        tokenStr = tokenStr.stringByReplacingOccurrencesOfString("<", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
        tokenStr = tokenStr.stringByReplacingOccurrencesOfString(">", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
        tokenStr = tokenStr.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
        NSLog("Device Token %@", tokenStr)
        NSUserDefaults.standardUserDefaults().setValue(tokenStr, forKey: "DeviceToken")
        NSUserDefaults.standardUserDefaults().synchronize()
      
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        dictNotification = (userInfo as NSDictionary).objectForKey("aps") as! NSDictionary
        UIAlertView(title: "Notification", message: "You have a new follower on Hotmaps", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Ok").show()

    }
/*    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for var i = 0; i < deviceToken.length; i++ {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        println("tokenString: \(tokenString)")
    }*/
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.sics.www" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("www", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as! NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

extension AppDelegate : UIAlertViewDelegate{
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if  buttonIndex == 1 {
            showProfilePage()
        }
    }
}
