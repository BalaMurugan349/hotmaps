//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "menu")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    func setNavBarImage(){
    let container: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 20))
    let logo: UIImageView = UIImageView(frame: container.frame)
    logo.image = UIImage(named: "logo")
    logo.contentMode = UIViewContentMode.ScaleAspectFill
    container.addSubview(logo)
    self.navigationItem.titleView = container;
        
   //     UIFont(name: "Futura-Medium", size: 14)

    }
    func addTapGesture(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(UIViewController.dismissKeyboard))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
}