//
//  DXAnnotationSettings.m
//  CustomCallout
//
//  Created by Selvin on 12/04/15.
//  Copyright (c) 2015 S3lvin. All rights reserved.
//

#import "DXAnnotationSettings.h"

@implementation DXAnnotationSettings

+ (instancetype)defaultSettings {
    DXAnnotationSettings *newSettings = [[super alloc] init];
    if (newSettings) {
        newSettings.calloutOffset = 18.0f;

        newSettings.shouldRoundifyCallout = YES;
        newSettings.calloutCornerRadius = 5.0f;

        newSettings.shouldAddCalloutBorder = NO;
        newSettings.calloutBorderColor = [UIColor clearColor];
        newSettings.calloutBorderWidth = 1.0;

        newSettings.animationType = DXCalloutAnimationNone;
        newSettings.animationDuration = 0.25;
    }
    return newSettings;
}

@end
