//
//  NSDictionary+CryptObject.h
//  Companion
//
//  Created by Aswin on 24/04/14.
//  Copyright (c) 2014 Sics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCCryptor.h"

@interface NSDictionary (CryptObject)

//- (id)decryptObjectForKey:(id)key;
- (id)decryptObjectForKey:(id)key withIV:(NSString *)ivKey;
@end
