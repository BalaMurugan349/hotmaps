//
//  NSDictionary+CryptObject.m
//  Companion
//
//  Created by Aswin on 24/04/14.
//  Copyright (c) 2014 Sics. All rights reserved.
//

#import "NSDictionary+CryptObject.h"

@implementation NSDictionary (CryptObject)

//- (id)decryptObjectForKey:(id)key
//{
//    return [SCCryptor decryptCipherText:[self objectForKey:key]];
//}

- (id)decryptObjectForKey:(id)key withIV:(NSString *)ivKey
{
    return [SCCryptor decryptCipherText:[self objectForKey:key] withIV:ivKey];
}

@end
