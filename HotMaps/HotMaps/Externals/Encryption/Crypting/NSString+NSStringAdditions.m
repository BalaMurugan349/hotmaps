
#import "NSString+NSStringAdditions.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (NSStringAdditions)

+ (NSString *)escapedString:(NSString *)text
{
	return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)text, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
}

+ (NSString *)stringByStrippingHtmlTags:(NSString *)text
{
    NSMutableString *outString;
    if (text){
        outString = [[NSMutableString alloc] initWithString:text];
        if ([text length] > 0){
            NSRange r;
            while ((r = [outString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound){
                [outString deleteCharactersInRange:r];
            }      
        }
    }
    return outString;
}

- (NSString *)escape
{
    return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)self, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
}

- (NSString *)md5
{
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, (unsigned)strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3], 
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];  
}

+ (NSString *)stringToCheckNull:(NSString *)string
{
    string = [string isKindOfClass:[NSNull class]] || string == (id)[NSNull null] || [string length] == 0 || [string isEqualToString:@"(null)"] ? @"" : string;
    return string;
}

+ (NSString *)addEscapeCharacterToString:(NSString *)string
{
    string = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return string;
}

+ (NSString *)stringByReplacingEscapeOccurance:(NSString *)string
{
     string = [string stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    return string;
}
@end
