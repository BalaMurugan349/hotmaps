//
//  SCAESCrypting.h
//  Companion - Staff Message
//
//  Created by Aswin on 07/08/14.
//  Copyright (c) 2014 Aswin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCAESCrypting : NSObject

-  (NSData *)encrypt:(NSData *)plainText key:(NSString *)key iv:(NSString *)iv;
-  (NSData *)decrypt:(NSData *)encryptedText key:(NSString *)key iv:(NSString *)iv;
-  (NSData *)generateRandomIV:(size_t)length;
-  (NSString *) md5:(NSString *) input;
-  (NSString*) sha256:(NSString *)key length:(NSInteger) length;

@end
