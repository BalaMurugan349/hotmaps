//
//  SCCryptor.h
//  Companion
//
//  Created by Aswin on 22/04/14.
//  Copyright (c) 2014 Sics. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SCCryptor : NSObject

//+ (NSString *)encryptText:(NSString *)plainText;
//+ (NSString *)decryptCipherText:(NSString *)cipherText;

+ (NSString *)generateRandomIV;
+ (NSString *)encryptText:(NSString *)plainText withIV:(NSString *)ivKey;
+ (NSString *)decryptCipherText:(NSString *)cipherText withIV:(NSString *)ivKey;

@end
