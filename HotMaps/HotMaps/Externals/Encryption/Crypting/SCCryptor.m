//
//  SCCryptor.m
//  Companion
//
//  Created by Aswin on 22/04/14.
//  Copyright (c) 2014 Sics. All rights reserved.
//

#import "SCCryptor.h"
//#import "RNOpenSSLDecryptor.h"
//#import "RNEncryptor.h"
//#import "Base64.h"
#import "SCAESCrypting.h"
#import "NSData+Base64.h"
#import "NSString+NSStringAdditions.h"

#define kCryptKey @"812994969surcomp656397396compsur"

@implementation SCCryptor

//+ (NSString *)encryptText:(NSString *)plainText
//{
//    if ([NSString stringToCheckNull:plainText].length == 0) return @"";
//    NSData *plainData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *error;
//    NSData *encryptedData = [RNEncryptor encryptData:plainData withSettings:kRNCryptorAES256Settings password:kCryptKey error:&error];
//    NSString *encryptedString = [encryptedData base64EncodedString];
//    return encryptedString;
//}
//
//+ (NSString *)decryptCipherText:(NSString *)cipherText
//{
//    if ([NSString stringToCheckNull:cipherText].length == 0) return @"";
//    NSData *encryptedData = [cipherText base64DecodedData];
//    NSError *error;
//    NSData *decryptedData = [RNDecryptor decryptData:encryptedData withPassword:kCryptKey error:&error];
//    NSString *decryptedString = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
//    return decryptedString;
//}

+ (NSString *)key
{
    return [[SCAESCrypting alloc] sha256:kCryptKey length:32];
}

+ (NSString *)generateRandomIV
{
    NSString * iv = [[[[SCAESCrypting alloc] generateRandomIV:11]  base64EncodingWithLineLength:0] substringToIndex:16];
    return iv;
}

+ (NSString *)encryptText:(NSString *)plainText withIV:(NSString *)ivKey
{
    if ([NSString stringToCheckNull:plainText].length == 0) return @"";
    NSData * encryptedData = [[SCAESCrypting alloc] encrypt:[plainText dataUsingEncoding:NSUTF8StringEncoding] key:[SCCryptor key] iv:ivKey];
    return [encryptedData  base64EncodingWithLineLength:0];
}

+ (NSString *)decryptCipherText:(NSString *)cipherText withIV:(NSString *)ivKey
{
    if ([NSString stringToCheckNull:cipherText].length == 0) return @"";
    NSData *encryptedData = [NSData dataWithBase64EncodedString:cipherText];
    NSData *decryptedData = [[SCAESCrypting alloc] decrypt:encryptedData  key:[SCCryptor key] iv:ivKey];
    NSString *decryptedString = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
    return decryptedString;
}
@end
