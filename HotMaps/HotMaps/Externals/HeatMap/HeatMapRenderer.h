//
//  HeatMapRenderer.h
//  HotMaps
//
//  Created by Srishti Innovative on 12/16/15.
//  Copyright © 2015 Srishti Innovative. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "HeatMap.h"

@interface HeatMapRenderer : MKOverlayRenderer

@end
