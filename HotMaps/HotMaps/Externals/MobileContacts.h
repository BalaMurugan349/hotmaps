//
//  MobileContacts.h
//  DigiDex
//
//  Created by mac on 12/10/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobileContacts : NSObject
{
    NSMutableArray *arrTemp1,*arrayAllContacts;
}

//@property(nonatomic,strong)NSString *FirstName;
//@property(nonatomic,strong)NSString *lastName;
//@property(nonatomic,strong)NSString *userName;
//@property(nonatomic,strong)NSString *phoneNum;
//@property(nonatomic,strong)UIImage *imageProfile;
//@property(nonatomic,strong)NSString *emailId;
//@property(nonatomic) BOOL isSelected;

-(NSArray *)getContactDetails;
@end
