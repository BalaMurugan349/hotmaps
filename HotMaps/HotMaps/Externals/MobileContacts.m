//
//  MobileContacts.m
//  DigiDex
//
//  Created by mac on 12/10/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "MobileContacts.h"
#import <AddressBook/AddressBook.h>
#import <UIKit/UIKit.h>
//#import "CAFriends.h"

@implementation MobileContacts

//@synthesize FirstName,lastName,userName,phoneNum,imageProfile,emailId,isSelected;


#pragma mark Fetching Mobile Contacts
-(NSArray *)getContactDetails
{
    arrTemp1 = [NSMutableArray new];
    arrayAllContacts = [NSMutableArray new];
    
    CFErrorRef err;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &err);
    __block BOOL accessGranted = NO;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 6) {
        addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
        });
    }
    else {
        accessGranted = YES;
    }
    CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFMutableArrayRef peopleMutable = CFArrayCreateMutableCopy(
                                                               kCFAllocatorDefault,
                                                               CFArrayGetCount(people),
                                                               people
                                                               );
    CFArraySortValues(
                      peopleMutable,
                      CFRangeMake(0, CFArrayGetCount(peopleMutable)),
                      (CFComparatorFunction) ABPersonComparePeopleByName,
                      (void*) ABPersonGetSortOrdering()
                      );
    arrTemp1=(__bridge NSMutableArray *)peopleMutable;
    
    for (int i=0;i<[arrTemp1 count];i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        NSString *str=(__bridge_transfer NSString *) ABRecordCopyValue((__bridge ABRecordRef)([arrTemp1 objectAtIndex:i]), kABPersonFirstNameProperty);
        
        NSString *str1=(__bridge_transfer NSString *) ABRecordCopyValue((__bridge ABRecordRef)([arrTemp1 objectAtIndex:i]), kABPersonLastNameProperty);
        NSData *contactImageData = (__bridge NSData*)ABPersonCopyImageData((__bridge ABRecordRef)(arrTemp1 [i]));
        
        UIImage *img = contactImageData == nil ? [UIImage imageNamed:@"placeholder"] : [[UIImage alloc] initWithData:contactImageData];
        
        [dict setObject:img  forKey:@"profile"];
        if (str.length!=0 && str1.length!=0){
            str=[str stringByAppendingString:@" "];
            str=[str stringByAppendingString:str1];
        }
        else if (str.length==0 && str1.length!=0)
            str = str1;
        else if (str.length!=0 && str1.length==0)
            str = str;
        else
            str = @" ";
         [dict setObject:str forKey:@"userName"];
        NSString *phone = nil;
        NSString *email = nil;
        ABMultiValueRef phoneNumbers = ABRecordCopyValue((__bridge ABRecordRef)([arrTemp1 objectAtIndex:i]),
                                                         kABPersonPhoneProperty);
        if (ABMultiValueGetCount(phoneNumbers) > 0) {
            phone = (__bridge_transfer NSString*)
            ABMultiValueCopyValueAtIndex(phoneNumbers, 0);
        } else
            phone = @"[None]";
        ABMultiValueRef emailId1 = ABRecordCopyValue((__bridge ABRecordRef)([arrTemp1 objectAtIndex:i]),
                                                    kABPersonEmailProperty);
        if (ABMultiValueGetCount(emailId1) > 0)
        {
            email = (__bridge_transfer NSString*)
            ABMultiValueCopyValueAtIndex(emailId1, 0);
        } else
            email = @"[None]";
         [dict setObject:phone forKey:@"mobile"];
        [dict setObject:email forKey:@"email"];
    
     
       ([phone isEqualToString:@"[None]"] && [email isEqualToString:@"[None]"]) ? nil :[arrayAllContacts addObject:dict];
    }
    [arrayAllContacts sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"userName" ascending:YES selector:@selector(caseInsensitiveCompare:)]]];
    return arrayAllContacts;
}

@end
