//
//  UITextField+Padding.h
//  Contact App
//
//  Created by Aswin on 02/04/14.
//  Copyright (c) 2014 Aswin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Padding)

-(void) setLeftPadding:(int) paddingValue;
-(void) setRightPadding:(int) paddingValue;

@end