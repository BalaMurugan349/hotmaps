//
//  CreateLocationViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 02/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
@objc protocol LocationCreatedDelegate{
    optional func locationCreated()
    optional func reloadData()
}

class CreateLocationViewController: UITableViewController, FilterPreferenceDelegate {

    var delegate: LocationCreatedDelegate!
    var locationName: NSString!
    var latitude: NSString!
    var longitude: NSString!
    var locationId: NSString!
    
    var trendUpCount: NSString!
    var trendDownCount: NSString!
    
    var isForRatingLocation: Bool!
    
    var fullnessFilter, energyFilter, ageFilter, genderFilter, casualFilter, singleFilter, waitFilter, inclusiveFilter, priceFilter, thumbsUpFilter : NSString!
    var filterValuesArray: NSMutableArray = NSMutableArray()
    var label: UILabel!
    var score: Int!
    var imagePicker : UIImagePickerController!
    
    @IBOutlet weak var thumbsUpSlider: ANPopoverSlider!
    @IBOutlet weak var casualSlider: ANPopoverSlider!
    @IBOutlet weak var energySlider: ANPopoverSlider!
    @IBOutlet weak var ageSlider: ANPopoverSlider!
    @IBOutlet weak var trendUpButton: UIButton!
    @IBOutlet weak var trendDownButton: UIButton!
    @IBOutlet weak var textfieldRating: UITextField!
    @IBOutlet weak var imageViewThumbnail : UIImageView!
    
    var selectedImage : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        score = 0
        self.setNavBarImage()
        // Do any additional setup after loading the view.
        trendUpCount = "0"
        trendDownCount = "0"
        
        fullnessFilter = ""; energyFilter = ""; ageFilter = "";genderFilter = "";
        casualFilter = ""; singleFilter = ""; waitFilter = ""; inclusiveFilter = ""; priceFilter = "";
        thumbsUpFilter = ""
        
        setUpUI()
        checkRatingTime()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func checkRatingTime (){
        //hotmapsapp.com/HotMaps/apilinks/RatingTime.php?userid=18&locationid=79
        if locationId == "" {
            self.trendUpButton.hidden = false
            self.trendDownButton.hidden = false
            return
        }
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        params.addValue(locationId, key: "locationid")

        ServiceManager.fetchDataFromService("apilinks/RatingTime.php?", parameters: params) { (success, result, error) -> Void in
            
            SVProgressHUD.dismiss()
            if success{
                if error == nil && result!["status"] as! Bool == true
                {
                    self.trendUpButton.hidden = false
                    self.trendDownButton.hidden = false
                }
                else if error == nil && result!["status"] as! Bool == false
                {
                    self.trendUpButton.hidden = true
                    self.trendDownButton.hidden = true

                }
            }
            else{
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
            
        }
 
    }
    
    func setUpUI(){
        let backBtn: UIButton = UIButton(type: UIButtonType.Custom)
        backBtn.frame = CGRectMake(0, 0, 25, 25) ;
        backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
        backBtn.addTarget(self, action:#selector(CreateLocationViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backButton
        
        textfieldRating.layer.borderWidth = 1.0
        textfieldRating.layer.borderColor = UIColor.whiteColor().CGColor
    }
    func backAction(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:- FilterPreferenceDelegate
    func getFilterValues(filterArray: NSMutableArray){
        self.filterValuesArray = filterArray
        for (_,item) in self.filterValuesArray.enumerate()
        {
            let dict: NSDictionary = item as! NSDictionary
            let rowVal = dict.objectForKey("row") as! String
            switch rowVal{
            case "0":
                thumbsUpFilter = dict.objectForKey("val") as! String
                thumbsUpSlider.value = dict.objectForKey("val")!.floatValue!
            case "1":
                ageFilter = dict.objectForKey("val") as! String
                ageSlider.value = dict.objectForKey("val")!.floatValue!
                
            case "2":
                energyFilter = dict.objectForKey("val") as! String
                energySlider.value = dict.objectForKey("val")!.floatValue!
           
            case "3":
                casualFilter = dict.objectForKey("val") as! String
                casualSlider.value = dict.objectForKey("val")!.floatValue!
          
            case "4":
                genderFilter = dict.objectForKey("val") as! String
            case "5":
                singleFilter = dict.objectForKey("val") as! String
            case "6":
                waitFilter = dict.objectForKey("val") as! String
            case "7":
                fullnessFilter = dict.objectForKey("val") as! String
            case "8":
                inclusiveFilter = dict.objectForKey("val") as! String
            case "9":
                priceFilter = dict.objectForKey("val") as! String
            default :
               break
                
            }
        }
    }
    
    func actionSubmit(filterArray: NSMutableArray) {
        getFilterValues(filterArray)
        actionCreateOrSubmit()
    }
    
    
    //MARK:- UISlider Action
    @IBAction func actionSlider(sender: UISlider){
        sender.selected = true
    //    sender.thumbTintColor = sliderColor
        sender.setThumbImage(sliderThumb, forState: .Normal)

        sender.minimumTrackTintColor = sliderColor
        let dict = ["row": "\(sender.tag)", "val" : String(format: "%.0f", sender.value)]
        if filterValuesArray.count == 0{
            filterValuesArray.addObject(dict)
        }else{
            filterValuesArray.enumerateObjectsUsingBlock  ({obj, idx, stop  in
                let dict1 = obj as! NSDictionary
                if dict1["row"] as! String == "\(sender.tag)"{
                    self.filterValuesArray.removeObject(dict1)
                    self.filterValuesArray.addObject(dict)
                }
                else{
                    self.filterValuesArray.addObject(dict)
                }
            })
        }
    }
    
    //MARK:- UIButtonActions
    //MARK:- Button Action Create
    @IBAction func actionCreate(sender: AnyObject){
        getFilterValues(self.filterValuesArray)
        actionCreateOrSubmit()}
    
    func actionCreateOrSubmit(){
        
        score = Int(arc4random_uniform(50)) + 100
        let totalPoints = User.currentUser().points.integerValue + score
        User.currentUser().points = String(totalPoints)
        
        NSUserDefaults.standardUserDefaults().setValue(String(totalPoints), forKey: "CurrentScore")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        //(trendDownButton.selected == false && trendUpButton.selected == false) ||
        let conditionToCheck = self.isForRatingLocation == true ? thumbsUpSlider.selected == false : (thumbsUpSlider.selected == false ||  filterValuesArray.count == 0)
        let msg = self.isForRatingLocation == true ? "Like/Unlike Filter not selected" : "Rate the location completely"
        
        if conditionToCheck
        {
             if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                
            }))
            self.parentViewController?.presentViewController(alert, animated: true, completion:  nil)
            }}
        else{
            if(textfieldRating.text == ""){
                UIAlertView(title: "Sorry", message: "Please describe the location to submit", delegate: nil, cancelButtonTitle: "OK").show()
                return
            }
//            }else if(selectedImage == nil){
//                UIAlertView(title: "Error", message: "Please select any image", delegate: nil, cancelButtonTitle: "Ok").show()
//                return
//            }
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
            var token: NSString?
            if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
                token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
            }
            token = token == nil ? "12345" : token
            let params = NSMutableData.postData() as! NSMutableData
            
            if isForRatingLocation == true{
                params.addValue("rateLocation", key: "type")
                params.addValue(locationId, key: "loaction_id")
            }
            else{
                params.addValue("createLocation", key: "type")
                params.addValue(latitude, key: "latitude")
                params.addValue(longitude, key: "longitude")
                params.addValue(locationName, key: "locationname")
            }
            
            params.addValue(token!, key: "devicetoken")
            params.addValue(fullnessFilter, key: "fullness")
            params.addValue(energyFilter, key: "energy")
            params.addValue(ageFilter, key: "age")
            params.addValue(totalPoints, key: "points")
            
            params.addValue(genderFilter, key: "genderFilter")
            params.addValue(casualFilter, key: "casualFilter")
            params.addValue(singleFilter, key: "singleFilter")
            params.addValue(waitFilter, key: "waitFilter")
            params.addValue(inclusiveFilter, key: "inclusiveFilter")
            params.addValue(priceFilter, key: "priceFilter")
            
            params.addValue(trendUpCount, key: "trendUpCount")
            params.addValue(trendDownCount, key: "trendDownCount")
            params.addValue(thumbsUpFilter, key: "likecount")
            params.addValue(textfieldRating.text!, key: "description")
            if(selectedImage != nil){
            params.addValue(UIImageJPEGRepresentation(selectedImage, 0.7)!, key: "locimg", filename:User.makeFileName())
            }
            params.addValue(User.currentUser().userId, key: "userid")
            
            if isForRatingLocation == true{
            if locationId == "" || locationId.length == 0 /*|| params.valueForKey("loaction_id") == nil*/
                {
               SVProgressHUD.showErrorWithStatus("Error in Rating Location")
            }
            else{
                ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
                    SVProgressHUD.dismiss()
                    if success{
                        if error == nil && result!["Result"] as! String == "Success"
                        {
                            self.getPoints()
                        }
                        else
                        {
                            var msg: String
                            
                            if  result!["Result"] as! String == "Failed" && result!["Error"] as? String != nil
                            {
                                msg = result!["Error"] as? String == "Sorry, you can only rate a given location once an hour" ? "Sorry, you can only rate a given location once an hour" : "Error in Rating Location"
                            }
                            else{
                            if self.isForRatingLocation == true{
                                msg = "You can only rate each location once every 12 hours"
                            }
                            else{
                                msg = result!["Details"] as! String
                            }
                            }
                            if #available(iOS 8.0, *) {
                                let alert = UIAlertController(title: "Sorry", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                                    
                                }))
                                self.parentViewController?.presentViewController(alert, animated: true, completion:  nil)
                            }
                        }
                    }
                    else{
                        SVProgressHUD.showErrorWithStatus("Please check your internet connection")
                    }
                }
                }

            }
            else{
                ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
                SVProgressHUD.dismiss()
                if success{
                    if error == nil && result!["Result"] as! String == "Success"
                    {
                          self.getPoints()
                    }
                    else
                    {
                        var msg: String
                        if self.isForRatingLocation == true{
                            msg = "You can only rate each location once every 12 hours"
                        }
                        else{
                            msg = result!["Details"] as! String
                        }
                         if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Sorry", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                            
                        }))
                        self.parentViewController?.presentViewController(alert, animated: true, completion:  nil)
                        }
                    }
                }
                else{
                        SVProgressHUD.showErrorWithStatus("Please check your internet connection")
                    }
            }
            }
        }
    }
    
    
    
    //MARK:- Button Action MoreFilters
    @IBAction func actionMoreFilters(sender: AnyObject){
        let conditionToCheck = self.isForRatingLocation == true ? thumbsUpSlider.selected == false : (thumbsUpSlider.selected == false || (trendDownButton.selected == false && trendUpButton.selected == false && trendDownButton.hidden == false && trendUpButton.hidden == false))
        let msg =  "Like or Trend not selected"
        if conditionToCheck
        {
            if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: "Warning", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                
            }))
            self.parentViewController?.presentViewController(alert, animated: true, completion:  nil)
            }}
        else{
        let moreFiltersViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MoreFiltersViewController") as! MoreFiltersViewController
        moreFiltersViewController.delegate = self
        moreFiltersViewController.isFromCreateViewController = true
        moreFiltersViewController.editedCellsArray = self.filterValuesArray
        self.navigationController?.pushViewController(moreFiltersViewController, animated: true)
        }
    }
    
    @IBAction func actionTrendUp(sender: UIButton){
        trendUpButton.selected = true
        trendDownButton.selected = false
        
        trendUpCount = "1"
        trendDownCount = "0"
    }
    @IBAction func actionTrendDown(sender: UIButton){
        trendUpButton.selected = false
        trendDownButton.selected = true
        
        trendUpCount = "0"
        trendDownCount = "1"
    }

    func getPoints(){
     // var  rankDetailsArray = NSMutableArray()
      let  ratingsArray = NSMutableArray()
        //var points1 : Int!
        //sleep(1)
        //http://52.88.60.242/HotMaps/Api.php?type=allratedusers
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("allratedusers", key: "type")
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            
            SVProgressHUD.dismiss()
            if success{
            if error == nil && result!["rseult"] as! String == "success"
            {
                if result!["Location"] as? Int == 0{
                }
                else{
                ratingsArray.addObjectsFromArray((result as! NSDictionary).objectForKey("Rates") as! [AnyObject])
                let totalLocations = result!["Location"] as? Int
                self.calculatePoints(ratingsArray, totalLocations: totalLocations!)
                }
            }
            else if error == nil && result!["rseult"] as! String == "Failed"
            {
                //points1 = 0
                User.currentUser().points = "0"
                User.currentUser().rank = "0"
                self.showAlert(0)
            }
            }
            else{
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
            
        }
    }
    func calculatePoints(ratingsArray: NSMutableArray, totalLocations: Int ){
         var  rankDetailsArray = NSMutableArray()
         var rank: Float = 0
        for (_,item) in ratingsArray.enumerate()
        {
            let dict = item as! NSDictionary
            let arrRates = dict["rates"] as? NSMutableArray
        
            let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            let sortedArray = arrRates!.sortedArrayUsingDescriptors(NSArray(object: sortDescriptor) as! [NSSortDescriptor])
            let arr = NSMutableArray(array: sortedArray)
            let myArr = self.arrayFilteredForUniqueValuesOfKeyPath("location_name",arr: arr)
            if dict["userid"] as? String == User.currentUser().userId{
                rank = Float((myArr.count * 100) / totalLocations)
            }
        let dictRankDetails: Dictionary<String, String> = ["userId": dict["userid"] as! String, "username": dict["username"] as! String, "points":  dict["points"] as! String   ]
        
        rankDetailsArray.addObject(dictRankDetails as AnyObject)
    
    }
    let descriptor =  NSSortDescriptor(key: "points.integerValue" as String, ascending: false)
    let sorted = rankDetailsArray.sortedArrayUsingDescriptors(NSArray(object: descriptor) as! [NSSortDescriptor])
    rankDetailsArray.removeAllObjects()
    rankDetailsArray = NSMutableArray(array: sorted)
        var percentile: Float = 0
        for(_,_) in rankDetailsArray.enumerate(){
            let totalScore = rankDetailsArray.count
            for (idx, item) in rankDetailsArray.enumerate(){
                let dict = item as! NSDictionary
                if dict["userId"] as? String == User.currentUser().userId{
                    percentile = (( Float(totalScore) - ( Float(idx) + 1)) * 100 ) / Float(totalScore)
                }
            }
            
        }
        showAlert(Int(percentile))
    }
    func showAlert(rank: Int){
      //  var x = arc4random_uniform(50) + 100
        let title = "Boom! You Just Earned \(score) Points"
        if #available(iOS 8.0, *) {
          let alert = UIAlertController(title: title, message: "You now have more Sway than \(rank)% of HotMappers", preferredStyle: UIAlertControllerStyle.Alert)
    
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
            
            self.delegate.locationCreated!()
            self.navigationController?.popToRootViewControllerAnimated(true)
            
        }))
        self.parentViewController?.presentViewController(alert, animated: true, completion:  nil)
        }
    }
    func arrayFilteredForUniqueValuesOfKeyPath(keyPath: String, arr: NSMutableArray) -> [AnyObject] {
        let valueSeen: NSMutableSet = NSMutableSet()
        
        
        return arr.filteredArrayUsingPredicate(NSPredicate { (evaluatedObject, bindings) -> Bool in
            let value: AnyObject = evaluatedObject!.valueForKeyPath(keyPath)!
            if !valueSeen.containsObject(value) {
                valueSeen.addObject(value)
                return true
            }
            else {
                return false
            }
            })
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    //http://52.88.60.242/HotMaps/Api.php?type=createLocation&devicetoken=12345&locationname=calicut&latitude=11.2500&longitude=75.7700&fullness=&energy=34&age=23&trendUpCount=1&trendDownCount=0&likecount=1&dislikecount=0&genderFilter=&casualFilter=&singleFilter=&waitFilter=&inclusiveFilter=&priceFilter=&userid=2
    
    
    
    @IBAction func onTakePictureButtonPressed (sender : UIButton){
        textfieldRating.resignFirstResponder()
        imagePicker = imagePicker == nil ?  UIImagePickerController() : imagePicker
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        let hasCamera : Bool = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        imagePicker.sourceType = hasCamera ? UIImagePickerControllerSourceType.Camera : UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
       // UIActionSheet(title: "Choose", delegate: self, cancelButtonTitle:nil , destructiveButtonTitle:nil, otherButtonTitles: "Camera", "PhotoAlbum","Cancel").showInView((self.view)!)

    }
}


extension CreateLocationViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
            guard let text = textField.text else { return true }
            
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 60 // Bool
        //return true
    }

}

//extension CreateLocationViewController : UIActionSheetDelegate
//{
//    //MARK:- ACTIONSHEET DELEGATE
//    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
//        imagePicker = imagePicker == nil ?  UIImagePickerController() : imagePicker
//        imagePicker.delegate = self
//        imagePicker.allowsEditing = true
//        switch (buttonIndex)
//        {
//        case 0 :
//            let hasCamera : Bool = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
//            imagePicker.sourceType = hasCamera ? UIImagePickerControllerSourceType.Camera : UIImagePickerControllerSourceType.PhotoLibrary
//            self.presentViewController(imagePicker, animated: true, completion: nil)
//            break
//        case 1 :
//            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
//            self.presentViewController(imagePicker, animated: true, completion: nil)
//            break
//        default:
//            break
//        }
//    }
//
//}

extension CreateLocationViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //MARK:- IMAGEPICKERCONTROLLER DELEGATE
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.dismissViewControllerAnimated(true) {
            self.openEditor(image)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    func openEditor(image : UIImage) {
        
        let controller : PECropViewController = PECropViewController()
        controller.delegate = self;
        controller.image = image;
        
        let width = image.size.width;
        let height = image.size.height;
        let length = min(width, height)
        controller.imageCropRect = CGRectMake((width - length) / 2, (height - length) / 2,length, length);
        let nav : UINavigationController = UINavigationController(rootViewController: controller)
        self.presentViewController(nav, animated: true, completion: nil)
    }
    
}

extension CreateLocationViewController : PECropViewControllerDelegate{
    func cropViewController(controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        selectedImage = croppedImage
        imageViewThumbnail.image = selectedImage
    }
    func cropViewControllerDidCancel(controller: PECropViewController!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
