//
//  BMDropDown.swift
//  TennisBuddy
//
//  Created by Bala on 20/04/15.
//  Copyright (c) 2015 Bala. All rights reserved.
//

import UIKit
protocol bmDropDownDelegate  {
    func selectedValueFromBMDropDowm (value : NSString, idx: NSIndexPath)
}

class BMDropDown: UIView,UITableViewDelegate,UITableViewDataSource {
        var tableViewObj : UITableView!
        var arrayToLoad : NSMutableArray = NSMutableArray()
        var selectedValues : NSString!
        var delegate : bmDropDownDelegate!
    
        var lastSelectedIndexPath: NSIndexPath!
        override init(frame: CGRect) {
            super.init(frame: frame)
//            tableViewObj = UITableView(frame:  CGRectMake(self.frame.size.width - 200, 60, 200, 0), style: UITableViewStyle.Plain)
//            tableViewObj.delegate = self
//            tableViewObj.dataSource = self
//            tableViewObj.backgroundColor = UIColor.blackColor()
//            tableViewObj.showsVerticalScrollIndicator = false
//            self .addSubview(tableViewObj)
//            
//            UIView.animateWithDuration(0.5, animations: { () -> Void in
//                self.tableViewObj.frame = CGRectMake(self.frame.size.width - 200, 60, 200, 220)
//                }) { (finished) -> Void in
//                    
//            }
        }
    
    func showDropDown(dropDownFrame dropDownFrame : CGRect ,heightOfDropDown : CGFloat){
        self.frame = UIScreen.mainScreen().bounds
        self.backgroundColor = UIColor.clearColor()
        tableViewObj = UITableView(frame:  dropDownFrame, style: UITableViewStyle.Plain)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.backgroundColor = UIColor.blackColor()
        tableViewObj.showsVerticalScrollIndicator = false
        tableViewObj.layer.cornerRadius = 5.0
        tableViewObj.separatorColor = UIColor.clearColor()
        self .addSubview(tableViewObj)
        let appde : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appde.window!.addSubview(self)

        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), heightOfDropDown)
            }) { (finished) -> Void in
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
        }
        
        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrayToLoad.count
        }
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            
            var cell : UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cel")
            if (cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                cell!.selectionStyle = UITableViewCellSelectionStyle.None;
                cell?.backgroundColor = UIColor.blackColor()
                cell?.contentView.backgroundColor = UIColor.blackColor()
            }
           cell?.accessoryType = (lastSelectedIndexPath?.row == indexPath.row) ? .Checkmark : .None
            if arrayToLoad[indexPath.row] as? String == "Percentage"{
            let myString = NSMutableAttributedString(string: "")
            let likeAttachment = NSTextAttachment()
            likeAttachment.image = UIImage(named: "up_small")
                let attachmentString = NSAttributedString(attachment: likeAttachment)
                myString.appendAttributedString(attachmentString)

                let attrString = NSAttributedString(string: " Percentage")
                myString.appendAttributedString(attrString)
                
            cell?.textLabel?.attributedText = myString
            
            }
            else
            {
                cell?.textLabel?.text = arrayToLoad[indexPath.row] as? String}
            cell?.textLabel?.font = UIFont.init(name: "Futura", size: 15)
            cell?.textLabel?.textColor = UIColor.whiteColor()
            cell?.textLabel?.textAlignment = NSTextAlignment.Center
            return cell!
        }
        
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            if indexPath.row != lastSelectedIndexPath?.row {
                if let lastSelectedIndexPath = lastSelectedIndexPath {
                    let oldCell = tableView.cellForRowAtIndexPath(lastSelectedIndexPath)
                    oldCell?.accessoryType = .None
                }
                
                let newCell = tableView.cellForRowAtIndexPath(indexPath)
                newCell?.accessoryType = .Checkmark
                
                lastSelectedIndexPath = indexPath
            }
            
            
            selectedValues = arrayToLoad[indexPath.row] as? String
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
                
                }) { (finished) -> Void in
                    self.delegate.selectedValueFromBMDropDowm(self.selectedValues == nil ? "" : self.selectedValues, idx: self.lastSelectedIndexPath)
                    self.removeFromSuperview()
            }
        }
   
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first
        let location = touch!.locationInView(touch!.view)
        if(!CGRectContainsPoint(self.tableViewObj.frame, location)){
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
                }) { (finished) -> Void in
                    self.removeFromSuperview()
            }
        }
        
    }
    
    


}
