//
//  FacebookFriendsViewController.swift
//  HotMaps
//
//  Created by Bala Murugan on 6/4/16.
//  Copyright © 2016 Srishti Innovative. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class FacebookFriendsViewController: UITableViewController,FBSDKAppInviteDialogDelegate {
    var stringFbIds : String = ""
    var arrayData : NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBarImage()
        self.fbLogin()
        let inviteButton : UIBarButtonItem = UIBarButtonItem(title: "Invite", style: .Plain, target: self, action: #selector(onInviteButtonPressed))
        inviteButton.tintColor = UIColor.redColor()
        self.navigationItem.rightBarButtonItem = inviteButton

    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.fetchUsersProfile()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fbLogin()  {
        let login : FBSDKLoginManager = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.Browser
        login.logOut()
        login.logInWithReadPermissions(["public_profile", "email" , "user_friends"], fromViewController: self) { (result, error) -> Void in
            if(error == nil && result.isCancelled == false){
                SVProgressHUD.showWithStatus("Processing...")
                let request = FBSDKGraphRequest(graphPath:"me", parameters:["fields" : "id, name, email"])
                request.startWithCompletionHandler {
                    (connection, result, error) in
                    if error != nil {
                        // SVProgressHUD.dismiss()
                    }
                    else if let userData = result as? [String:AnyObject] {
                        print("\(userData)")
                        self.fetchFacebookFriends()
                    }
                }
            }else{
                SVProgressHUD.showErrorWithStatus("Login Failed")
            }
        }

    }
    func fetchFacebookFriends()  {
        let request = FBSDKGraphRequest(graphPath:"me", parameters:["fields" : "id, name, email"])
        request.startWithCompletionHandler {
            (connection, result, error) in
            if error != nil {
                print(error)
                //    SVProgressHUD.dismiss()
            }
            else if let userData = result as? [String:AnyObject] {
                print("\(userData)")
                //   SVProgressHUD.dismiss()
                let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me/friends?limit=5000", parameters: ["fields":"name,installed,first_name"], HTTPMethod: "GET")
                graphRequest.startWithCompletionHandler({ (connection, result, error) in
                    print("result \(result)")
                    if let resultObj = result{
                        let userArray : NSArray = resultObj["data"] as! NSArray
                        if userArray.count > 0{
                            self.stringFbIds = userArray.valueForKey("id").componentsJoinedByString(",")
                            self.fetchUsersProfile()
                        }else{
                            SVProgressHUD.showErrorWithStatus("No facebook Friends")
                        }
                    }else{
                    SVProgressHUD.dismiss()
                    }
                })
            }
        }
}
    
    func fetchUsersProfile()  {
        SVProgressHUD.showWithStatus("Processing...")
        let params  = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        params.addValue(stringFbIds, key: "facebookid")
        ServiceManager.fetchDataFromService("apilinks/FBUsers.php?", parameters: params) { (success, result, error) in
            if success{
                if result!["result"] as! String == "Success"{
                    self.arrayData.removeAllObjects()
                    self.arrayData.addObjectsFromArray(result!["details"] as! [AnyObject])
                    self.tableView.reloadData()
                    SVProgressHUD.dismiss()
                    print(result)
                    
                }else{
                    SVProgressHUD.showErrorWithStatus("Please try again")
                }
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayData.count
    }
   
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")

        let dict : NSDictionary = self.arrayData[indexPath.row] as! NSDictionary
        let imgvwProfile = cell!.viewWithTag(1) as! UIImageView
        let ImageName : String = dict["profileimage"] as! String
        imgvwProfile.sd_setImageWithURL(NSURL(string: _imagePathUser + "images/users/" + ImageName) , placeholderImage: UIImage(named: "UserPlaceholder"))
        
        let labelName : UILabel = cell!.viewWithTag(2) as! UILabel
        labelName.text = dict["username"] as? String

        return cell!
    }
   

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let dict : NSDictionary = self.arrayData[indexPath.row] as! NSDictionary
        let profilevc = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
        profilevc.userId = dict["id"] as! String
        profilevc.followStatus = dict["followstatus"] as! String == "yes" ? "Yes" : "No"
        profilevc.userName = dict["username"] as! String
        profilevc.isFromPushNotification = false
        self.navigationController?.pushViewController(profilevc, animated: true)

    }
    
    func onInviteButtonPressed() {
        let content = FBSDKAppInviteContent();//"https://fb.me/1751305425141798"
        content.appLinkURL = NSURL(string: "https://fb.me/1751305425141798");
        content.appInvitePreviewImageURL = NSURL(string: "http://52.43.177.218/previewImage.jpg")

        FBSDKAppInviteDialog.showFromViewController(self, withContent: content, delegate: self);

    }

  
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        print("Invitiation sent")
        
    }
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!) {
        print("\(error)")
    }
}
