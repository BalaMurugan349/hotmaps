//
//  FeedbackViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 22/10/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import Foundation
import MessageUI

class FeedbackViewController: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var textViewfeedback: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        textViewfeedback.layer.cornerRadius = 8.0
        self.setNavBarImage()
    }
    
    @IBAction func actionSubmit(sender: AnyObject) {
        textViewfeedback.resignFirstResponder()
        
        if  textViewfeedback.text.characters.count != 0 && textViewfeedback.text != "\n"{
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
        }
        
        else{
            SVProgressHUD.showErrorWithStatus("Please enter your feedback")
        }
       
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["bugs@hotmapsapp.com"])
        mailComposerVC.setSubject("Hotmaps - Feedback")
        mailComposerVC.setMessageBody(textViewfeedback.text, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        textViewfeedback.text = ""
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
