//
//  FeedsTableViewController.swift
//  HotMaps
//
//  Created by Bala on 04/04/2016.
//  Copyright © 2016 Srishti Innovative. All rights reserved.
//

import UIKit

class FeedsTableViewController: UITableViewController {

    var arrayFeeds : NSMutableArray = NSMutableArray()
    var buttonUniverse : UIButton!
    var imageviewBorder : UIImageView!
    var buttonFriends : UIButton!
    
    var _activityIndicatorView : UIActivityIndicatorView?
    var indexCount : NSInteger = 0
    var isLoadMoreData : Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBarImage()

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.fetchNearByFeeds()
        setRightBarButton()

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        buttonUniverse.removeFromSuperview()
        buttonFriends.removeFromSuperview()
        imageviewBorder.removeFromSuperview()
    }
    
    func setRightBarButton()  {
        imageviewBorder = UIImageView(image: UIImage(named: "FeedBorder"))
        imageviewBorder.frame = CGRectMake(screenSize.width - 75, 7, 70, 30)
        self.navigationController?.navigationBar.addSubview(imageviewBorder)
        
        buttonUniverse = UIButton(type: .Custom)
        buttonUniverse.frame = CGRectMake(screenSize.width - 72, 9, 30, 25)
        buttonUniverse.backgroundColor = UIColor.clearColor()
        buttonUniverse.setImage(UIImage(named: "Universe"), forState: UIControlState.Normal)
        buttonUniverse.setImage(UIImage(named: "UniverseSelected"), forState: UIControlState.Selected)
        buttonUniverse.addTarget(self, action: #selector(FeedsTableViewController.onUniverseButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        buttonUniverse.selected = true
        self.navigationController?.navigationBar.addSubview(buttonUniverse)
        
        buttonFriends = UIButton(type: .Custom)
        buttonFriends.frame = CGRectMake(screenSize.width - 33, 12, 25, 20)
        buttonFriends.backgroundColor = UIColor.clearColor()
        buttonFriends.setImage(UIImage(named: "Friends"), forState: UIControlState.Normal)
        buttonFriends.setImage(UIImage(named: "FriendsSelected"), forState: UIControlState.Selected)
        buttonFriends.addTarget(self, action: #selector(FeedsTableViewController.onFriendsButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationController?.navigationBar.addSubview(buttonFriends)
        
     }
    
    func onUniverseButtonPressed()  {
        if arrayFeeds.count > 0{
            self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: UITableViewScrollPosition.Top, animated: false)
        }
        buttonUniverse.selected = true
        buttonFriends.selected = false
        self.indexCount = 0
        self.fetchNearByFeeds()
    }
    
    func onFriendsButtonPressed()  {
        if arrayFeeds.count > 0{
            self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: UITableViewScrollPosition.Top, animated: false)
        }
        self.indexCount = 0
        buttonUniverse.selected = false
        buttonFriends.selected = true
        self.fetchFeedItems()

    }
    
    func fetchNearByFeeds()  {
        if indexCount == 0{
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        params.addValue("\(indexCount)", key: "index")

        ServiceManager.fetchDataFromService("apilinks/feeditemsbyrange1.php?", parameters: params) { (success, result, error) -> Void in
            self._activityIndicatorView?.stopAnimating()
            if error == nil && result!["status"] as! Bool == true
            {
                if self.indexCount == 0 { self.arrayFeeds.removeAllObjects() }
                self.arrayFeeds.addObjectsFromArray(result!["Details"] as! [AnyObject])
                self.isLoadMoreData = self.arrayFeeds.count % 15 == 0 ? true : false
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
            else if error == nil && result!["status"] as! Bool == false
            {
                if self.indexCount == 0 { SVProgressHUD.showErrorWithStatus("No Feeds found") }
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }
    }
    
    func fetchFeedItems (){
         if indexCount == 0{
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        params.addValue("\(indexCount)", key: "index")
        
        ServiceManager.fetchDataFromService("apilinks/feeditems1.php?", parameters: params) { (success, result, error) -> Void in
            self._activityIndicatorView?.stopAnimating()

            if error == nil && result!["status"] as! Bool == true
            {
                if self.indexCount == 0{  self.arrayFeeds.removeAllObjects() }
                self.arrayFeeds.addObjectsFromArray(result!["Details"] as! [AnyObject])
                self.tableView.reloadData()
                self.isLoadMoreData = self.arrayFeeds.count % 15 == 0 ? true : false
                SVProgressHUD.dismiss()
            }
            else if error == nil && result!["status"] as! Bool == false
            {
                if self.indexCount == 0{ SVProgressHUD.showErrorWithStatus("No Feeds found") }
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }

    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFeeds.count
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 75
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        (cell as! FollowersFeedsTableViewCell).configureCell(self.arrayFeeds[indexPath.row] as! NSDictionary)
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeedsTableViewController.didTappedOnUserName(_:)))
        (cell as! FollowersFeedsTableViewCell).labelUserName.userInteractionEnabled = true
        (cell as! FollowersFeedsTableViewCell).labelUserName.addGestureRecognizer(tapGesture)
        (cell as! FollowersFeedsTableViewCell).labelUserName.tag = indexPath.row
        
        let tapGesture1 : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeedsTableViewController.didTappedOnLocationName(_:)))
        (cell as! FollowersFeedsTableViewCell).labelLocationName.userInteractionEnabled = true
        (cell as! FollowersFeedsTableViewCell).labelLocationName.addGestureRecognizer(tapGesture1)
        (cell as! FollowersFeedsTableViewCell).labelLocationName.tag = indexPath.row
        return cell!
    }

    //MARK:- SETTING FOOTER LOADING VIEW
    func setLoadingFooter (){
        let frame : CGRect =  CGRectMake(0, 0, self.view.frame.size.width, 30)
        let aframe : CGRect = CGRectMake(self.view.center.x - 10, 20, 5, 5)
        let loadingView : UIView = UIView(frame: frame)
        _activityIndicatorView = UIActivityIndicatorView(frame: aframe)
        _activityIndicatorView?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        _activityIndicatorView?.startAnimating()
        loadingView.addSubview(_activityIndicatorView!)
        self.tableView.tableFooterView = loadingView
    }
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if (self.tableView.contentOffset.y<0)/* scroll from top */
        {
            
        }else if (self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height) && arrayFeeds.count > 0 && isLoadMoreData == true ){
            indexCount = indexCount + 1
            self.setLoadingFooter()
            buttonUniverse.selected == true ? fetchNearByFeeds() : fetchFeedItems()
        }
    }


    func didTappedOnUserName(gesture : UIGestureRecognizer){
        let dict = arrayFeeds[gesture.view!.tag] as! NSDictionary
        let profilevc = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
        profilevc.userId = dict["userid"] as? String
        profilevc.followStatus = "Yes"
        profilevc.userName = dict["username"] as? String
        profilevc.isFromPushNotification = false
        self.navigationController?.pushViewController(profilevc, animated: true)
    }
    
    func didTappedOnLocationName (gesture : UIGestureRecognizer) {
       let dict = arrayFeeds[gesture.view!.tag] as! NSDictionary
      let location : LocationDetails = LocationDetails(locationDict: dict)
        let locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
       locationDetailsViewController.location = location
       self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
    }
}
