//
//  FilterViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 29/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController{

    @IBOutlet weak var thumbsUpSlider: ANPopoverSlider!
    @IBOutlet weak var casualSlider: ANPopoverSlider!
    @IBOutlet weak var energySlider: ANPopoverSlider!
    @IBOutlet weak var ageSlider: ANPopoverSlider!
    var preferencesArray:NSMutableArray = NSMutableArray()
    var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavBarImage()
        setUpUI()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUpUI(){
        let backBtn: UIButton = UIButton(type:
            UIButtonType.Custom)
        backBtn.frame = CGRectMake(0, 0, 25, 25) ;
        backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
        backBtn.addTarget(self, action:#selector(FilterViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backButton
    }
    func backAction(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:- Change Appearence Of Slider
    
    //MARK:- Submit Button Action
    @IBAction func actionSubmit(sender: AnyObject)
    {
        let filteredResultsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FilteredResultViewController") as! FilteredResultViewController
        filteredResultsViewController.preferencesArray = preferencesArray
        self.navigationController?.pushViewController(filteredResultsViewController, animated: true)
    }
    
    //MARK:- AgeSliderAction
    @IBAction func actionAgeSlider(sender: UISlider){
        sender.selected = true
        sender.setThumbImage(sliderThumb, forState: .Normal)
        sender.minimumTrackTintColor = sliderColor
        
        let dict = ["row": "\(sender.tag)", "val" : String(format: "%.0f", sender.value)]
        if preferencesArray.count == 0{
            preferencesArray.addObject(dict)
        }else{
            preferencesArray.enumerateObjectsUsingBlock  ({obj, idx, stop  in
                let dict1 = obj as! NSDictionary
                if dict1["row"] as! String == "\(sender.tag)"{
                    self.preferencesArray.removeObject(dict1)
                    self.preferencesArray.addObject(dict)
                }
                else{
                    self.preferencesArray.addObject(dict)
                }
            })
        }
        
    }
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
