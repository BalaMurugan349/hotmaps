//
//  FilteredResultViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 30/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import Foundation

class FilteredResultViewController: UIViewController , UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, sortLocationDelegate, UIGestureRecognizerDelegate,LocationCreatedDelegate{

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var sortedArray: NSMutableArray!
    var preferencesArray:NSMutableArray = NSMutableArray()
    var locationDetailsArray: NSMutableArray = NSMutableArray()
    var FilteredArray: NSMutableArray = NSMutableArray()
    var nameArray: NSMutableArray!
    var searchActive : Bool!
    var locationArray: NSMutableArray = NSMutableArray()
    
    var indexCount : NSInteger!
    var isLoadMoreData : Bool?
    var indexSet:NSMutableIndexSet = NSMutableIndexSet()
    
    var resultArray : NSMutableArray!
    var filtered:[String] = []
    var tapRec: UITapGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
        /* Setup delegates */
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        indexCount = 0
        isLoadMoreData = true
        getTopLocations()
        setUpUI()
//        var sortButton : UIBarButtonItem = UIBarButtonItem(title: "Sort", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("sortAction"))
//        self.navigationItem.rightBarButtonItem = sortButton

        self.setNavBarImage()
        if #available(iOS 9.0, *) {
            UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).font =  UIFont.init(name: "Futura", size: 14)
        } else {
            checkSubview(view)
        }
       
        // Do any additional setup after loading the view.
    }
    func checkSubview(view:UIView){
        for subView in view.subviews {
            if subView is UITextField {
                let textField = subView as! UITextField
                textField.font = UIFont.init(name: "Futura", size: 14)
            } else {
                checkSubview(subView)
            }
        }
    }
    func setUpUI(){
        
        let backBtn: UIButton = UIButton(type: UIButtonType.Custom)
            backBtn.frame = CGRectMake(0, 0, 25, 25) ;
            backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
            backBtn.addTarget(self, action:#selector(FilteredResultViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
            
            let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
            self.navigationItem.leftBarButtonItem = backButton
       
        
        let mapBtn: UIButton = UIButton(type: UIButtonType.Custom)
        mapBtn.frame = CGRectMake(0, 0, 25, 25) ;
        mapBtn.setImage(UIImage(named:"mapbtn.png"), forState: UIControlState.Normal)
        mapBtn.addTarget(self, action:#selector(FilteredResultViewController.mapAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let mapButton: UIBarButtonItem = UIBarButtonItem(customView: mapBtn)
        
        let sotrBtn: UIButton = UIButton(type:UIButtonType.Custom)
        sotrBtn.frame = CGRectMake(0, 0, 20, 20) ;
        sotrBtn.setImage(UIImage(named:"sort.png"), forState: UIControlState.Normal)
        sotrBtn.addTarget(self, action:#selector(FilteredResultViewController.sortAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let sortButton: UIBarButtonItem = UIBarButtonItem(customView: sotrBtn)
        self.navigationItem.setRightBarButtonItems([mapButton,sortButton], animated: true)
    }
    func backAction(){
        self.navigationController?.popViewControllerAnimated(true)
    }

    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        searchActive = searchActive != nil ? searchActive : false
            
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(FilteredResultViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(FilteredResultViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    //MARK:- VIEW WILL DISAPPEAR
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    //MARK:- KEYBOARD SHOW
    func keyboardWillShow (note : NSNotification)
    {
        tapRec = UITapGestureRecognizer(target: self , action:#selector(FilteredResultViewController.tap(_:)))
        tapRec.cancelsTouchesInView = false
        tapRec.delegate = self
        
        self.tableView.addGestureRecognizer(tapRec)
    }
    
    //MARK:- KEYBOARD HIDE
    func keyboardWillHide (note : NSNotification)
    {
        if tapRec != nil{
        self.tableView.removeGestureRecognizer(tapRec)
        }
    }
    //MARK:- UIGesture Recognizer Delegate
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if gestureRecognizer is UITapGestureRecognizer {
          //  let location = touch.locationInView(tableView)
            return true
        }
        return false
    }
    func tap(sender: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    func mapAction(){
        let mapVC = self.storyboard?.instantiateViewControllerWithIdentifier("MapVC") as! MapVC
        mapVC.locationArray = self.locationArray
        self.navigationController?.pushViewController(mapVC, animated: true)
    }
    func sortAction()
    {
        searchBarCancelButtonClicked(searchBar)
        
        let sortTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SortTableViewController") as! SortTableViewController
        sortTableViewController.isFromTopLocationsVC = false
        sortTableViewController.locationArray = locationArray
        sortTableViewController.delegate = self
        self.navigationController?.pushViewController(sortTableViewController, animated: true)
        
    }
   func sortLocations(sortedArray: NSMutableArray, isSortingRating: Bool) {
         searchBar.text = ""
        filtered = []
        self.sortedArray = sortedArray
        locationArray.removeAllObjects()
        locationArray = sortedArray
        tableView.reloadData()
    }
    func reloadData() {
    indexCount = 0
    isLoadMoreData = true
    searchActive = false
    locationArray.removeAllObjects()
    self.tableView.reloadData()
        
    getTopLocations()
    }
    //MARK:- Get Top Locations
    
    func getTopLocations(){
        //http://52.88.60.242/HotMaps/Api.php?type=listLocation&devicetoken=12345&latitude=11.2505&longitude=75.7700&index=0
        
        var userLocation : NSDictionary
        if  NSUserDefaults.standardUserDefaults().objectForKey("userLocation") != nil {
            userLocation  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as! NSDictionary
        }
        else{
            userLocation = ["long" : "76.8811670", "lat" : "8.5603060"]
        }
        
        var token: NSString?
        if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
            token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
        }
        token = token == nil ? "12345" : token
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("listLocation", key: "type")
        params.addValue(userLocation["lat"]!, key: "latitude")
        params.addValue(userLocation["long"]!, key: "longitude")
        params.addValue(token!, key: "devicetoken")
        let userId = User.currentUser().userId == nil ? "" : User.currentUser().userId
        params.addValue(userId, key: "userId")
        params.addValue("\(indexCount)", key: "index")
        SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
             SVProgressHUD.dismiss()
            if error == nil && result!["Result"] as! String == "Success"
            {
                if(self.indexCount == 0) { self.locationDetailsArray.removeAllObjects() }
                self.locationDetailsArray.addObjectsFromArray((result as! NSDictionary).objectForKey("Details") as! [AnyObject])
               // self.indexCount = self.indexCount + 1
                self.isLoadMoreData = self.locationDetailsArray.count % 10 == 0 ? true : false
                self.filterArray()
                
              /*  if self.isLoadMoreData == true{
                    
                    self.getTopLocations()
                }
                else{
                    
                  SVProgressHUD.dismiss()
                    self.FilteredArray = self.locationDetailsArray
                    
                    let group = dispatch_group_create()
                    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                    
                    // Add a task to the group
                    dispatch_group_async(group, queue){
                        
                        self.performFiltering()
                    }
                   
                    dispatch_group_notify(group, queue){
                        
                        var descriptor = NSSortDescriptor(key: "val", ascending: true)
                        var resultArray1 = self.resultArray.sortedArrayUsingDescriptors(NSArray(object: descriptor) as [AnyObject])
                        
                        self.locationArray.removeAllObjects()
                        self.nameArray = NSMutableArray()
                        var arr = NSMutableArray(array: resultArray1)
                        arr.enumerateObjectsUsingBlock({ object, index, stop in
                            var locationDict = object as! NSDictionary
                            self.locationArray.addObject(locationDict["location"]!)
                            var location = locationDict["location"] as! LocationDetails
                            self.nameArray.addObject(location.locationName)

                            
                        })

                        /*
                        
                    //    self.locationDetailsArray.removeObjectsAtIndexes(self.indexSet)
                        self.nameArray = NSMutableArray()
                        self.locationDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
                            var locationDict = self.locationDetailsArray[index] as! NSDictionary
                            var location: LocationDetails = LocationDetails(locationDict: locationDict)
                            self.nameArray.addObject(location.locationName)
                           // self.locationArray.addObject(location)
                        })*/
                        dispatch_async(dispatch_get_main_queue()){
                            self.tableView.reloadData()

                        }
                        
                    }
                    
                }*/
            }
            else if error == nil && result!["Result"] as! String == "Failed"
            {
                SVProgressHUD.dismiss()
                self.isLoadMoreData = false
                if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Alert", message: "Loading Finished", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                    
                }))
                self.parentViewController?.presentViewController(alert, animated: true, completion:  nil)

                }
           /*     self.FilteredArray = self.locationDetailsArray
                
                let group = dispatch_group_create()
                let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                
                // Add a task to the group
                dispatch_group_async(group, queue){
                    
                    self.performFiltering()
                }
                
                dispatch_group_notify(group, queue){
                    var descriptor = NSSortDescriptor(key: "val", ascending: true)
                    var resultArray1 = self.resultArray.sortedArrayUsingDescriptors(NSArray(object: descriptor) as [AnyObject])
                    
                    self.locationArray.removeAllObjects()
                    self.nameArray = NSMutableArray()

                    var arr = NSMutableArray(array: resultArray1)
                    arr.enumerateObjectsUsingBlock({ object, index, stop in
                        var locationDict = self.resultArray[index] as! NSDictionary
                        self.locationArray.addObject(locationDict["location"]!)
                        var location = locationDict["location"] as! LocationDetails
                        self.nameArray.addObject(location.locationName)

                        

                    })
                    dispatch_async(dispatch_get_main_queue()){
                        self.tableView.reloadData()
                        
                    }
                    
                }*/

                
            }
            else{
                SVProgressHUD.dismiss()
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
            
        }
        
    }
    func filterArray(){
        self.FilteredArray = self.locationDetailsArray
        
        let group = dispatch_group_create()
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        
        // Add a task to the group
        dispatch_group_async(group, queue){
            
            self.performFiltering()
        }
        
        dispatch_group_notify(group, queue){
           
            let descriptor = NSSortDescriptor(key: "val", ascending: true)
            let resultArray1 = self.resultArray.sortedArrayUsingDescriptors(NSArray(object: descriptor) as! [NSSortDescriptor])
            
            self.locationArray.removeAllObjects()
            self.nameArray = NSMutableArray()
            let arr = NSMutableArray(array: resultArray1)
            arr.enumerateObjectsUsingBlock({ object, index, stop in
                let locationDict = object as! NSDictionary
                self.locationArray.addObject(locationDict["location"]!)
                let location = locationDict["location"] as! LocationDetails
                self.nameArray.addObject(location.locationName)
                
                
            })
            dispatch_async(dispatch_get_main_queue()){
                self.tableView.reloadData()
                
            }

    }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func loadMoreResults(sender: UIButton){
        if isLoadMoreData == true{
            indexCount = indexCount + 1
            getTopLocations()
        }
        else
        {if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: "Alert", message: "Loading Finished", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                
            }))
            self.parentViewController?.presentViewController(alert, animated: true, completion:  nil)
            }
        }
    }
    func performFiltering(){
        
         resultArray = NSMutableArray()
        locationDetailsArray.enumerateObjectsUsingBlock ({ obj, idx, stop in
            if idx == 2{
                
            }
            let locationDict = self.locationDetailsArray[idx] as! NSDictionary
            let location: LocationDetails = LocationDetails(locationDict: locationDict)
            var count: Double = 0
            for (_,item) in self.preferencesArray.enumerate()
            {
                let dict: NSDictionary = item as! NSDictionary
                let rowVal = dict.objectForKey("row") as! String
                
                switch rowVal{
                    case "0":
                       /* var filterMatch = location.age.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                        if filterMatch == false{
                          self.indexSet.addIndex(idx)
                        }*/
                        let sq = abs(location.likePercent.integerValue - dict.objectForKey("val")!.integerValue)
                        count = count + pow(Double(sq), 2)
                    
                    
                case "1":
                   /* var filterMatch = location.energy.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                    if filterMatch == false{
                        self.indexSet.addIndex(idx)
                    }*/
                    let sq = abs(location.age.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                 
                case "2":
                    
                    /*var filterMatch = location.casual.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                    if filterMatch == false{
                        self.indexSet.addIndex(idx)
                    }*/
                    let sq = abs(location.energy.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                    
                case "3":
                  /*  var filterMatch = location.gender.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                    if filterMatch == false{
                       self.indexSet.addIndex(idx)
                    }*/
                    let sq = abs(location.casual.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                case "4":
                 /*   var filterMatch = location.single.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                    if filterMatch == false{
                       self.indexSet.addIndex(idx)
                    }*/
                    let sq = abs(location.gender.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                case "5":
                   /* var filterMatch = location.wait.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                    if filterMatch == false{
                        self.indexSet.addIndex(idx)
                    }*/
                    let sq = abs(location.single.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                case "6":
                  /*  var filterMatch = location.fullness.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                    if filterMatch == false{
                        self.indexSet.addIndex(idx)
                    }*/
                    let sq = abs(location.wait.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                case "7":
                    /*var filterMatch = location.inclusive.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                    if filterMatch == false{
                       self.indexSet.addIndex(idx)
                    }*/
                    let sq = abs(location.fullness.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                case "8":
                   
                  /*  var filterMatch = location.price.integerValue <= dict.objectForKey("val")?.integerValue ? true : false
                    
                    if filterMatch == false{
                        self.indexSet.addIndex(idx)
                    }*/
                    let sq = abs(location.inclusive.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                case "9":
                    let sq = abs(location.price.integerValue - dict.objectForKey("val")!.integerValue)
                    count = count + pow(Double(sq), 2)
                default:
                    break
                }
                
               
                
            }
            
            let dict = ["location": location, "val" : count, "name": location.locationName]
       self.resultArray.addObject(dict)
        })
    }
    
   
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
       
        searchActive = searchBar.text == "" ? false : true
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
      //  searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
      //  searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if nameArray != nil{
            let dataArray = nameArray as AnyObject as! [String]
            filtered = dataArray.filter({ (text) -> Bool in
            let tmp: NSString = text
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
            
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        }

        self.tableView.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //MARK:- UITableView Data Source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive == true) {
            return filtered.count
        }
        else{
        return locationArray.count
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TopLocationsCell") as! TopLocationsTableViewCell;
        if searchActive == true {
            
           // var sortedArray = NSMutableArray()
            locationDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
                
                let locationDict = object as! NSDictionary
                let location: LocationDetails = LocationDetails(locationDict: locationDict)
                if location.locationName as String == self.filtered[indexPath.row]
                {
                    cell.trendImageView.image = location.trendUpValue.integerValue >= location.trendDownValue.integerValue ? UIImage(named: "up") : UIImage(named: "down")
                    cell.locationLabel.text = location.locationName as String
                
                    cell.infoLabel.text = location.likePercent as String
                    cell.infoLabel.textColor = location.likePercent.integerValue >= 50 ? UIColor(red: 0, green: 147/255.0, blue: 0, alpha: 1.0) : UIColor.redColor()

                    dispatch_async(dispatch_get_main_queue()){
                       
                    cell.distanceLabel.frame = CGRectMake( cell.infoLabel.intrinsicContentSize().width + CGRectGetMinX(cell.infoLabel.frame) + 2, cell.infoLabel.frame.origin.y, cell.distanceLabel.frame.size.width, cell.infoLabel.frame.size.height)
                    cell.distanceLabel.numberOfLines = 1

                    cell.distanceLabel.text = location.distance as String
                    }
                   
                }
                
                 })
            
         
        } else {
            
            let location = locationArray[indexPath.row] as! LocationDetails
            cell.trendImageView.image = location.trendUpValue.integerValue >= location.trendDownValue.integerValue ? UIImage(named: "up") : UIImage(named: "down")
            cell.locationLabel.text = location.locationName as String
            let font = UIFont.init(name: "Helvetica", size: 18.0)!
            let ht = heightForLabel(location.locationName as String, font: font, width: CGRectGetWidth(cell.locationLabel.frame))

            cell.locationLabel.frame =
                CGRectMake(CGRectGetMinX(cell.locationLabel.frame), CGRectGetMinY(cell.locationLabel.frame), CGRectGetWidth(cell.locationLabel.frame), ht)
            
            cell.infoLabel.text = location.likePercent as String
            cell.infoLabel.textColor = location.likePercent.integerValue >= 50 ? UIColor(red: 0, green: 147/255.0, blue: 0, alpha: 1.0) : UIColor.redColor()
            
            dispatch_async(dispatch_get_main_queue()){
                
                cell.distanceLabel.frame = CGRectMake( cell.infoLabel.intrinsicContentSize().width + CGRectGetMinX(cell.infoLabel.frame) + 2, cell.infoLabel.frame.origin.y, cell.distanceLabel.frame.size.width, cell.infoLabel.frame.size.height)
                cell.distanceLabel.numberOfLines = 1
                
                cell.distanceLabel.text = location.distance as String
            }
        }
        
        return cell;
    }
  /*  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return
    }*/
    //MARK:- UITableView Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var location: LocationDetails!
        if searchActive == true {
            
            locationDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
                let locationDict = object as! NSDictionary
                let loc: LocationDetails = LocationDetails(locationDict: locationDict)
                if loc.locationName as String == self.filtered[indexPath.row]
                {
                    location = loc
                }
                })
        }
        else {
                 location = locationArray[indexPath.row] as! LocationDetails
        }
        
                let locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
                locationDetailsViewController.location = location
                locationDetailsViewController.delegate = self
                self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
        
       }
    
    //MARK:- CALCULATE HEIGHT FOR LABEL
    func heightForLabel ( text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
