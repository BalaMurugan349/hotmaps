//
//  HeatMapViewController.h
//  HotMaps
//
//  Created by Srishti Innovative on 06/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeatMapViewController : UIViewController
- (NSDictionary *)heatMapData: (NSMutableArray *)locationCoordinatesArray;
@end
