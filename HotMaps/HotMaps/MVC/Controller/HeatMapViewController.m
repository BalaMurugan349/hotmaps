//
//  HeatMapViewController.m
//  HotMaps
//
//  Created by Srishti Innovative on 06/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

#import "HeatMapViewController.h"
#import <MapKit/MapKit.h>
#import "HeatMap.h"
#import "HeatMapView.h"

@interface HeatMapViewController ()

@end

@implementation HeatMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDictionary *)heatMapData: (NSMutableArray *)locationCoordinatesArray
{
    
    NSMutableDictionary *toRet = [[NSMutableDictionary alloc] initWithCapacity:[locationCoordinatesArray count]];
    
    for (NSDictionary *dict in locationCoordinatesArray) {

        MKMapPoint point = MKMapPointForCoordinate(
                                                   CLLocationCoordinate2DMake([[dict objectForKey:@"lat"] doubleValue], [[dict objectForKey:@"lon"] doubleValue]));
    
        NSValue *pointValue = [NSValue value:&point withObjCType:@encode(MKMapPoint)];
        
        NSString *likeString = [dict objectForKey:@"like"];
        long like =  likeString.longLongValue / 10 == 0 ? 1 : likeString.longLongValue / 10;
        
        [toRet setObject:[NSNumber numberWithLong:like] forKey:pointValue];
       // [toRet setObject:@"ll" forKey:@"lo"];
    }
    
    return toRet;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
