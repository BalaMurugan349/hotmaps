//
//  InviteFriendsTableViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 10/28/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class InviteFriendsTableViewController: UITableViewController {

   // var arrContacts: Array<Contact>! = Array<Contact>()
    var arrContacts: NSMutableArray!
    var arrSelectedIndex: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBarImage()
        setUpUI()
       
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
       
        //ContactsImporter.importContacts(showContacts)
        let mob = MobileContacts()
        arrContacts.addObjectsFromArray(mob.getContactDetails())
        tableView.reloadData()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         arrContacts = NSMutableArray()
        self.setNavigationBarItem()
    }
    func setUpUI(){
        let sendBtn: UIButton = UIButton(type: UIButtonType.Custom)
        sendBtn.frame = CGRectMake(0, 0, 25, 25) ;
        sendBtn.setImage(UIImage(named:"send.png"), forState: UIControlState.Normal)
        sendBtn.addTarget(self, action:#selector(InviteFriendsTableViewController.sendAction), forControlEvents: UIControlEvents.TouchUpInside)
      //  var sendButton: UIBarButtonItem = UIBarButtonItem(customView: sendBtn)
        //self.navigationItem.rightBarButtonItem = sendButton
    }
   /* func showContacts(contacts: Array<Contact>) {
        arrContacts = contacts
        tableView.reloadData()
        
    }*/
    func sendAction(){
        if arrSelectedIndex.count == 0{
            let alert = UIAlertView(title: "Warning", message: "Please select atleast one recepient", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        else{
          //  let arrContactsSelected = NSMutableArray()
            /*for(_, item) in arrSelectedIndex.enumerate(){
             //   var cntct = arrContacts[idx] as Contact
              //  var phn = cntct.phonesArray!.count > 0 ? cntct.phonesArray?.first : ""
              //  arrContactsSelected.addObject(phn)
            }*/
            let sendmsgVC = self.storyboard?.instantiateViewControllerWithIdentifier("SendMessageViewController") as! SendMessageViewController
        //    sendmsgVC.arrayContacts = arrContactsSelected
            self.navigationController?.pushViewController(sendmsgVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return arrContacts.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell : ContactTableViewCell? = tableView.dequeueReusableCellWithIdentifier("ContactTableViewCell") as? ContactTableViewCell
        
        if cell == nil {
            cell = ContactTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "ContactTableViewCell")
        }
       
        // Configure the cell...
        let contact = arrContacts[indexPath.row] as! NSDictionary
      //  cell!.labelContactName.text = contact.firstName + " " + contact.lastName
          cell!.labelContactName.text = contact.objectForKey("userName") as? String
         cell!.labelPhone.text = contact.objectForKey("mobile") as? String
        if contact.objectForKey("profile") != nil{
            cell!.imageViewProfile.image =  contact.objectForKey("profile") as? UIImage
        }
//        if contact.thumbnailImage != nil{
//        cell!.imageViewProfile.image = UIImage(data: contact.thumbnailImage!)
//        }
        else{
            
        cell!.imageViewProfile.image = UIImage(named: "placeholder")
        }
        cell!.imageViewProfile.layer.cornerRadius =  CGRectGetWidth(cell!.imageViewProfile.frame)/2
        cell!.imageViewProfile.layer.masksToBounds = true
        cell!.labelContactName.textColor = UIColor.whiteColor()
        cell!.labelContactName?.backgroundColor = UIColor.clearColor()
        cell?.imageViewTickMark.image = nil
        for(_, item) in arrSelectedIndex.enumerate(){
            let indx = item as! NSIndexPath
            if indx == indexPath{
                cell?.imageViewTickMark.image = UIImage(named: "tick")
            }
            }
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let contact = arrContacts[indexPath.row] as! NSDictionary

        if contact.objectForKey("mobile") != nil{
                 let sendmsgVC = self.storyboard?.instantiateViewControllerWithIdentifier("SendMessageViewController") as! SendMessageViewController
                    sendmsgVC.phonenumber = contact.objectForKey("mobile") as? String
                  self.navigationController?.pushViewController(sendmsgVC, animated: true)
              }
              else{
                  let alert
                 = UIAlertView(title: "Error", message: "Phone number not found", delegate: nil, cancelButtonTitle: "OK")
                   alert.show()
              }
        
        
     /*   if arrSelectedIndex.containsObject(indexPath){
            arrSelectedIndex.removeObject(indexPath)
        }
        else{
            arrSelectedIndex.addObject(indexPath)
        }
        tableView.reloadData()*/
       // var contact = arrContacts[indexPath.row]
       // if contact.phonesArray?.count > 0{
       //     var sendmsgVC = self.storyboard?.instantiateViewControllerWithIdentifier("SendMessageViewController") as! SendMessageViewController
        //    println(contact.phonesArray)
            
      //      sendmsgVC.phonenumber = contact.phone
      //      self.navigationController?.pushViewController(sendmsgVC, animated: true)
      //  }
      //  else{
      //      let alert
       //     = UIAlertView(title: "Error", message: "Phone number not found", delegate: nil, cancelButtonTitle: "OK")
     //       alert.show()
      //  }
        
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
