//
//  LeftMenuViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 29/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

enum LeftMenu: Int{
  case Map = 0
  case TopLocations
  case Feeds
  case Profile
  case Rank
  case Users
  case Settings
  case FacebookFriends
  case FeedBack
  case Help
}

protocol LeftMenuProocol: class{
    func changeViewController(menu: LeftMenu)
}
class LeftMenuViewController: UIViewController, LeftMenuProocol {
    
    @IBOutlet weak var tableView: UITableView!
    var menus = ["Browse, Search & Rate",  "Top Locations", "Activity Feed","Profile","Rank","Find User","Settings","Facebook Connect","Feedback","Help"]
    var mapViewController: UIViewController!
    var topLocationsViewController: UIViewController!
    var ratingHistoryViewController: UIViewController!
    var settingsViewController: UIViewController!
    var rankViewController: UIViewController!
    var feedbackViewController: UIViewController!
    var inviteFriendsViewController: UIViewController!
    var welcomeViewController: UIViewController!
    var profileViewController: UIViewController!
    var usersViewController: UIViewController!
    var feedViewController: UIViewController!
    var facebookFriendsViewController : UIViewController!
 
    var storyboardObj : UIStoryboard!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableView.separatorColor = UIColor.blackColor()
        
         storyboardObj = UIStoryboard(name: "Main", bundle: nil)
        
        let mapViewController = storyboardObj.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
        self.mapViewController = UINavigationController(rootViewController: mapViewController)
        
        let topLocationsViewController = storyboardObj.instantiateViewControllerWithIdentifier("TopLocationsViewController") as! TopLocationsViewController
        self.topLocationsViewController = UINavigationController(rootViewController: topLocationsViewController)
        
        let ratingHistoryViewController = storyboardObj.instantiateViewControllerWithIdentifier("RatingHistoryViewController") as! RatingHistoryViewController
        self.ratingHistoryViewController = UINavigationController(rootViewController: ratingHistoryViewController)

        
        let settingsViewController = storyboardObj.instantiateViewControllerWithIdentifier("SettingsViewController") as! SettingsViewController
        self.settingsViewController = UINavigationController(rootViewController: settingsViewController)
        
        let rankViewController = storyboardObj.instantiateViewControllerWithIdentifier("RankViewController") as! RankViewController
        self.rankViewController = UINavigationController(rootViewController: rankViewController)
        
        let feedbackViewController = storyboardObj.instantiateViewControllerWithIdentifier("FeedbackViewController") as! FeedbackViewController
        self.feedbackViewController = UINavigationController(rootViewController: feedbackViewController)
        
        let inviteFriendsViewController = storyboardObj.instantiateViewControllerWithIdentifier("InviteFriendsTableViewController") as! InviteFriendsTableViewController
        self.inviteFriendsViewController = UINavigationController(rootViewController: inviteFriendsViewController)
        
        let welcomeViewController = storyboardObj.instantiateViewControllerWithIdentifier("WelcomePopUpViewController") as! WelcomePopUpViewController
        self.welcomeViewController = UINavigationController(rootViewController: welcomeViewController)
        
        let userVC = storyboardObj.instantiateViewControllerWithIdentifier("UsersVC") as! UsersTableViewController
        userVC.isListFollowers = false
        self.usersViewController = UINavigationController(rootViewController: userVC)

        let feedVC = storyboardObj.instantiateViewControllerWithIdentifier("FeedsVC") as! FeedsTableViewController
        self.feedViewController = UINavigationController(rootViewController: feedVC)
        
        let fbVC = storyboardObj.instantiateViewControllerWithIdentifier("FacebookFriendsVC") as! FacebookFriendsViewController
        self.facebookFriendsViewController = UINavigationController(rootViewController: fbVC)

        self.tableView.registerCellClass(BaseTableViewCell.self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: BaseTableViewCell = BaseTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: BaseTableViewCell.identifier)
        cell.backgroundColor = UIColor(red: 32/255, green: 32/255, blue: 32/255, alpha: 1.0)
        cell.textLabel?.font = UIFont.init(name: "Futura", size: 18)
        cell.textLabel?.textColor = indexPath.row == 7 ? UIColor.redColor() : UIColor.whiteColor()
        cell.textLabel?.text = menus[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.item) {
            self.changeViewController(menu)
        }
    }
    
    func changeViewController(menu: LeftMenu) {
        switch menu {
        case .Map:
            self.slideMenuController()?.changeMainViewController(self.mapViewController, close: true)
        case .TopLocations:
            self.slideMenuController()?.changeMainViewController(self.topLocationsViewController, close: true)
            break
//        case .RatingHistory:
//            if User.currentUser().userId != nil{
//                self.slideMenuController()?.changeMainViewController(self.ratingHistoryViewController, close: true)}
//            else{self.slideMenuController()?.closeLeft()
//                showLoginAlert()
//            }
//            break
        case .Rank:
            if User.currentUser().userId != nil{
                self.slideMenuController()?.changeMainViewController(self.rankViewController, close: true)}
            else{
                self.slideMenuController()?.closeLeft()
                showLoginAlert()
            }
            break
//        case .InviteFriends:
//            if User.currentUser().userId != nil{
//                self.slideMenuController()?.changeMainViewController(self.inviteFriendsViewController, close: true)}
//            else{
//                self.slideMenuController()?.closeLeft()
//                showLoginAlert()
//            }
//            break
        case .FeedBack:
            if User.currentUser().userId != nil{
                self.slideMenuController()?.changeMainViewController(self.feedbackViewController, close: true)}
            else{
                self.slideMenuController()?.closeLeft()
                showLoginAlert()
            }
            break
        case .Settings:
            if User.currentUser().userId != nil{
                self.slideMenuController()?.changeMainViewController(self.settingsViewController, close: true)}
                else{
                self.slideMenuController()?.closeLeft()
                    showLoginAlert()
                }
            break
        case .Profile:
            if User.currentUser().userId != nil{
                let profilevc = storyboardObj.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
                profilevc.userId = User.currentUser().userId as String
                profilevc.followStatus = ""
                profilevc.userName = User.currentUser().username as String
                profilevc.isFromPushNotification = false
                self.profileViewController = UINavigationController(rootViewController: profilevc)
                self.slideMenuController()?.changeMainViewController(self.profileViewController, close: true)}
            else{
                self.slideMenuController()?.closeLeft()
                showLoginAlert()
            }
            break
        case .Users :
            if User.currentUser().userId != nil{
                self.slideMenuController()?.changeMainViewController(self.usersViewController, close: true)}
            else{
                self.slideMenuController()?.closeLeft()
                showLoginAlert()
            }
            break
        case .Feeds :
            if User.currentUser().userId != nil{
                self.slideMenuController()?.changeMainViewController(self.feedViewController, close: true)
                ((self.feedViewController as! UINavigationController).viewControllers[0] as! FeedsTableViewController).fetchFeedItems()
            }
            else{
                self.slideMenuController()?.closeLeft()
                showLoginAlert()
            }
            break
        case .FacebookFriends :
            if User.currentUser().userId != nil{
              self.slideMenuController()?.changeMainViewController(self.facebookFriendsViewController, close: true)
            }
            else{
                self.slideMenuController()?.closeLeft()
                showLoginAlert()
            }

           
            break
        case .Help:
            self.slideMenuController()?.changeMainViewController(self.welcomeViewController, close: true)
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showLoginAlert(){
        if #available(iOS 8.0, *) {
        let alertController = UIAlertController(title: "Sorry", message: "You must login to use this feature", preferredStyle: .Alert)
        
//        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
//            
//        }
//        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Login", style: .Default) { (action) in
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.didLogout(true)
        }
        alertController.addAction(OKAction)
            
        let signup = UIAlertAction(title: "Sign-up", style: .Default) { (action) in
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.showSignupPage(true)
            }
            alertController.addAction(signup)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
