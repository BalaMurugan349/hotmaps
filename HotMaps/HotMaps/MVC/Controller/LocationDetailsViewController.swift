//
//  LocationDetailsViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 03/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import Darwin
import Foundation

class LocationDetailsViewController: UITableViewController, LocationCreatedDelegate, UIAlertViewDelegate,UIGestureRecognizerDelegate {

   // @IBOutlet weak var tableViewFilters: UITableView!
    @IBOutlet weak var trendImageView: UIImageView!
    @IBOutlet weak var locationNameLabel: UILabel!
   // @IBOutlet weak var lastReviewLabel: UILabel!
    @IBOutlet weak var likePercentLabel: UILabel!
    @IBOutlet weak var ratingButton: UIButton!
   // @IBOutlet weak var rateCountlabel: UILabel!
    var deleteButton: UIBarButtonItem!
    var delegate: LocationCreatedDelegate!
    @IBOutlet weak var labelLocationName1 : UILabel!
    @IBOutlet weak var imageViewLocation : UIImageView!
    var location: LocationDetails!
    @IBOutlet weak var labelTextRating : UILabel!
    var arrayHistory : NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var viewLine1 : UIView!
    @IBOutlet weak var viewLine2 : UIView!
    var scrollviewImage  : UIScrollView = UIScrollView()
    var currentIndex : NSInteger = 0
    var buttonLeftArrow : UIButton!
    var buttonRightArrow : UIButton!
    var arrayImages : [String]!
    
     var filterPreferencesArray:NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavBarImage()        
        filterPreferencesArray   = [
            ["left": "Younger", "right" : "Older"],["left": "Mellow", "right" : "Wild"],
            ["left": "Casual", "right" : "Formal"],["left": "Male", "right" : "Female"],
            ["left": "Single", "right" : "Taken"],["left": "No Wait", "right" : "Long Wait"],
            ["left": "Empty", "right" : "Packed"],["left": "Inclusive", "right" : "Exclusive"],
            ["left": "$", "right" : "$$$$"]]
        
        setValues()
        
        ///TAP GESTURE
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LocationDetailsViewController.didTapImage))
        tapGesture.numberOfTapsRequired = 1
        imageViewLocation.addGestureRecognizer(tapGesture)
        imageViewLocation.userInteractionEnabled = true
        
        if #available(iOS 8.0, *) {
            CustomPhotoAlbum.sharedInstance
        } else {
            // Fallback on earlier versions
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        setUpUI()
        fetchRatingHistory()

    }
    func setUpUI(){
        let backBtn: UIButton = UIButton(type: UIButtonType.Custom) 
        backBtn.frame = CGRectMake(0, 0, 25, 25) ;
        backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
        backBtn.addTarget(self, action:#selector(LocationDetailsViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backButton
      
        let delBtn: UIButton = UIButton(type: UIButtonType.Custom)
        delBtn.frame = CGRectMake(0, 0, 25, 25) ;
        delBtn.setImage(UIImage(named:"delete.png"), forState: UIControlState.Normal)
        delBtn.addTarget(self, action:#selector(LocationDetailsViewController.actionDelete(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        deleteButton = UIBarButtonItem(customView: delBtn)
        if User.currentUser().userId != nil{
            
            if User.currentUser().userId == location.locationCreatorId{
                self.navigationItem.rightBarButtonItem = deleteButton
            }
            else{
            }
        }
        
        imageViewLocation.layer.borderWidth = 1.0
        imageViewLocation.layer.borderColor = UIColor.whiteColor().CGColor

    }
    func backAction(){
       // self.delegate.reloadData!()
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func actionDelete(sender: AnyObject) {
        
        let alert = UIAlertView(title: "Warning", message: "Are you sure to delete \(location.locationName)", delegate: self, cancelButtonTitle:"Cancel")
        alert.addButtonWithTitle("Delete")
        alert.show()
        
      

    }
    //MARK: UIAlertViewDelegate
    
    func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        switch buttonIndex{
        case 1: deleteLocation()
        case 0: break
        default: break
        }
    }
    func deleteLocation(){
        SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("deleteLocation", key: "type")
        params.addValue(location.id, key: "loaction_id")
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            SVProgressHUD.dismiss()
            
            if error == nil && result!["Result"] as! String == "Success"
            {
                 if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Success", message: "Location Deleted", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                    self.navigationController?.popViewControllerAnimated(true)
                    self.delegate.reloadData!()
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            else if error == nil && result!["Result"] as! String == "Failed"
            {
                 if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Error", message: "Failed To Delete Location", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                    
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            else{
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }
    }
    //MARK:- LocationCreatedDelegate
    func locationCreated() {
        let mapViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
        let mapViewNavController = UINavigationController(rootViewController: mapViewController)
        self.slideMenuController()?.changeMainViewController(mapViewNavController, close: true)
    }
    //MARK:- Set Values of Location
    func setValues()
    {
        trendImageView.image = location.trendUpValue.integerValue >= location.trendDownValue.integerValue ? UIImage(named: "up") : UIImage(named: "down")
        let arr : NSMutableArray = NSMutableArray(array:  location.locationFullName.componentsSeparatedByString(","))
        if(arr.count > 0){
        locationNameLabel.text = arr[0] as? String
            arr.removeObjectAtIndex(0)
        labelLocationName1.text = arr.componentsJoinedByString(",").stringByReplacingOccurrencesOfString("_", withString: " ").stringByReplacingOccurrencesOfString(",", withString: ", ")
        }
        likePercentLabel.text = location.likePercent as String
        likePercentLabel.textColor = location.likePercent.integerValue >= 50 ? UIColor(red: 0, green: 147/255.0, blue: 0, alpha: 1.0) : UIColor.redColor()
        
      /*  if location.totalRates.integerValue <= 1{
            lastReviewLabel.text = "Be the first to rate!"
            rateCountlabel.hidden = true
        }
        else{
            rateCountlabel.hidden = false*/
           // rateCountlabel.text = "Rate Count: \(location.totalRates)"
        let string = "Last Rating Submitted: \(location.time)" as NSString
        let attributedString = NSMutableAttributedString(string: string as String)
        
        let firstAttributes = [NSFontAttributeName: UIFont.init(name: "Futura", size: 15)!, NSBackgroundColorAttributeName: UIColor.clearColor()]
        let secondAttributes = [NSFontAttributeName: UIFont.init(name: "Futura-MediumItalic", size: 15)!, NSBackgroundColorAttributeName: UIColor.clearColor()]

        attributedString.addAttributes(firstAttributes, range: string.rangeOfString("Last Rating Submitted: "))
        attributedString.addAttributes(secondAttributes, range: string.rangeOfString("\(location.time)"))
        
       // lastReviewLabel.attributedText = attributedString
      //  }
        arrayImages  = location.locationImageName.componentsSeparatedByString(",")
        let imageName = arrayImages.count == 0 ? "" : arrayImages.last!
        viewLine1.hidden = arrayImages.count < 2
        viewLine2.hidden = arrayImages.count < 2
        self.imageViewLocation.sd_setImageWithURL(NSURL(string:_imagePathUser + imageName ), placeholderImage: UIImage(named: "UserPlaceholder"))
        self.labelTextRating.text = location.locationTextRating

        
    }
    
    @IBAction func actionGiveRating(sender: UIButton){
        if User.currentUser().userId != nil{
            if location.distance.floatValue > 0.378{
                  UIAlertView(title: "Sorry", message: "You must be at that location to give your rating", delegate: nil, cancelButtonTitle: "OK").show()
            }
            else
            {
                let createLocationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CreateLocationViewController") as! CreateLocationViewController
                createLocationViewController.delegate = self
                createLocationViewController.isForRatingLocation = true
                createLocationViewController.locationId = location.id
                createLocationViewController.locationName = location.locationName
                createLocationViewController.latitude = String(format:"%f", location.latitude)
                createLocationViewController.longitude = String(format:"%f",location.longitude)
                self.navigationController?.pushViewController(createLocationViewController, animated: true)
            }
        }
        else{
            if #available(iOS 8.0, *) {
                let alertController = UIAlertController(title: "Sorry", message: "You must login to use this feature", preferredStyle: .Alert)
                
                //                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                //
                //                }
                //                alertController.addAction(cancelAction)
                
                let OKAction = UIAlertAction(title: "Login", style: .Default) { (action) in
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.didLogout(true)
                }
                alertController.addAction(OKAction)
                let signup = UIAlertAction(title: "Sign-up", style: .Default) { (action) in
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.showSignupPage(true)
                }
                alertController.addAction(signup)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    //MARK:- UITableView DataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterPreferencesArray.count + 1 + arrayHistory.count
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row < filterPreferencesArray.count ? indexPath.row == 0 ? 80 : 50 : 80
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(indexPath.row < filterPreferencesArray.count){
        var cell : FilterPreferencesTableViewCell? = tableView.dequeueReusableCellWithIdentifier("FilterPreferencesCell") as? FilterPreferencesTableViewCell
        
        if cell == nil {
            cell = FilterPreferencesTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "FilterPreferencesCell")
        }
        
         cell?.sliderPreference.popupView.alpha = 0.0
        let filterDict = filterPreferencesArray.objectAtIndex(indexPath.row) as! NSDictionary
        cell!.leftLabel.text = filterDict.objectForKey("left") as? String
        cell!.rightLabel.text = filterDict.objectForKey("right") as? String

        //cell!.initWithDetails(filterDict)
        
        cell!.sliderPreference.userInteractionEnabled = false
        
        cell!.sliderPreference.minimumValue = indexPath.row == 0 ? 18 : 0
        cell!.sliderPreference.maximumValue =  indexPath.row == 0 ? 65 : 100
        cell!.ageRangeLabel1.hidden = indexPath.row == 0 ? false : true
        cell!.ageRangeLabel2.hidden = indexPath.row == 0 ? false : true
        cell!.ageValueLabel.hidden = indexPath.row == 0 ? false : true

        cell?.sliderPreference.minimumTrackTintColor = sliderUnselectedColor
      //  cell?.sliderPreference.thumbTintColor = sliderUnselectedColor
            
        switch indexPath.row{
        
        case 0:
            cell!.sliderPreference.value = location.age.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location.age == "" ? sliderUnselectedColor : sliderColor
            if location.age != ""{
                  dispatch_async(dispatch_get_main_queue()){
            cell?.sliderPreference.popupView.alpha = 1.0
            let zeThumbRect = cell?.sliderPreference.thumbRect
                cell?.sliderPreference.popupView.frame = CGRectMake(CGRectGetMinX(zeThumbRect!) + 1, CGRectGetMinY(zeThumbRect!) - CGRectGetHeight(zeThumbRect!) - 10, CGRectGetWidth(zeThumbRect!), CGRectGetHeight(zeThumbRect!))
                
        //    var popupRect = CGRectOffset(zeThumbRect!, 0, -floor(zeThumbRect!.size.height * 1.5))
       //     cell?.sliderPreference.popupView.frame = CGRectInset(popupRect,3, 15)
            cell?.sliderPreference.popupView.value = self.location.age.floatValue
                }
            }
            
             cell?.sliderPreference.setThumbImage(location.age == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
         //   cell?.sliderPreference.thumbTintColor = location.age == "" ? sliderUnselectedColor : sliderColor
        case 1:
            cell!.sliderPreference.value = location.energy.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location.energy == "" ? sliderUnselectedColor : sliderColor
             cell?.sliderPreference.setThumbImage(location.energy == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
         //   cell?.sliderPreference.thumbTintColor = location.energy == "" ? sliderUnselectedColor : sliderColor
        case 2:
            cell!.sliderPreference.value = location.casual.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location.casual == "" ? sliderUnselectedColor : sliderColor
             cell?.sliderPreference.setThumbImage(location.casual == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
       //     cell?.sliderPreference.thumbTintColor = location.casual == "" ? sliderUnselectedColor : sliderColor
        case 3:
            cell!.sliderPreference.value = location.gender.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location.gender == "" ? sliderUnselectedColor : sliderColor
             cell?.sliderPreference.setThumbImage(location.gender == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
        //    cell?.sliderPreference.thumbTintColor = location.gender == "" ? sliderUnselectedColor : sliderColor
        case 4:
            cell!.sliderPreference.value = location.single.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location == "" ? sliderUnselectedColor : sliderColor
             cell?.sliderPreference.setThumbImage(location.single == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
         //   cell?.sliderPreference.thumbTintColor = location.single == "" ? sliderUnselectedColor : sliderColor
        case 5:
            cell!.sliderPreference.value = location.wait.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location.wait == "" ? sliderUnselectedColor : sliderColor
             cell?.sliderPreference.setThumbImage(location.wait == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
     //       cell?.sliderPreference.thumbTintColor = location.wait == "" ? sliderUnselectedColor : sliderColor
        case 6:
            cell!.sliderPreference.value = location.fullness.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location.fullness == "" ? sliderUnselectedColor : sliderColor
             cell?.sliderPreference.setThumbImage(location.fullness == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
      //      cell?.sliderPreference.thumbTintColor = location.fullness == "" ? sliderUnselectedColor : sliderColor
        case 7:
            cell!.sliderPreference.value = location.inclusive.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location.inclusive == "" ? sliderUnselectedColor : sliderColor
             cell?.sliderPreference.setThumbImage(location.inclusive == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
       //     cell?.sliderPreference.thumbTintColor = location.inclusive == "" ? sliderUnselectedColor : sliderColor
        case 8:
            cell!.sliderPreference.value = location.price.floatValue
            cell?.sliderPreference.minimumTrackTintColor = location.price == "" ? sliderUnselectedColor : sliderColor
             cell?.sliderPreference.setThumbImage(location.price == "" ? sliderUnselectedThunb : sliderThumb, forState: .Normal)
        //    cell?.sliderPreference.thumbTintColor = location.price == "" ? sliderUnselectedColor : sliderColor
        default:
            break
        }
       
            return cell!
        }else if(indexPath.row == 9){
            let cell1 : UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("sceneCell")
            return cell1!
        }else{
            let cell2 : RatingHistoryTableViewCell? = tableView.dequeueReusableCellWithIdentifier("historyCell") as? RatingHistoryTableViewCell
            let dict : NSDictionary = arrayHistory[indexPath.row - 10] as! NSDictionary
            cell2?.labelLikeCount.text = dict["likecount"] as? String
            cell2?.locationNameLabel.text = dict["location_description"] as? String
                //== "" ? "No description found" : dict["location_description"] as? String
            let username : String = dict["username"] as! String
            cell2?.buttonUserName.setTitle("-" + username, forState: UIControlState.Normal)
            cell2?.buttonUserName.tag = indexPath.row - 10
            cell2?.buttonUserName.addTarget(self, action: #selector(LocationDetailsViewController.didTappedonUserName(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            return cell2!
        }
        
    }
    
  @IBAction  func mapItAction(){
        let destCoord = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        let destPlacemark = MKPlacemark(coordinate: destCoord, addressDictionary: nil)
        let destMapItem = MKMapItem(placemark: destPlacemark)
        let routeVC = self.storyboard?.instantiateViewControllerWithIdentifier("RouteViewController") as! RouteViewController
        routeVC.destination = destMapItem
    routeVC.location = self.location
        self.navigationController?.pushViewController(routeVC, animated: true)
    }

    func fetchRatingHistory (){
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId == nil ? "" : User.currentUser().userId , key: "userid")
        params.addValue(location.id, key: "locationid")
        
        ServiceManager.fetchDataFromService("apilinks/LocationHistory.php?", parameters: params) { (success, result, error) -> Void in
            
            if error == nil && result!["status"] as! Bool == true
            {
                self.arrayHistory.removeAllObjects()
                self.arrayHistory.addObjectsFromArray(result!["location_history"] as! [AnyObject])
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
            else if error == nil && result!["status"] as! Bool == false
            {
                SVProgressHUD.dismiss()
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }

    }

    func didTappedonUserName(sender : UIButton) {
        let dict : NSDictionary = arrayHistory[sender.tag] as! NSDictionary
        let profilevc = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
        profilevc.userId = dict["userid"] as! String
        profilevc.followStatus = dict["follower_status"] as! String
        profilevc.userName = dict["username"] as! String
        profilevc.isFromPushNotification = false
        self.navigationController?.pushViewController(profilevc, animated: true)
    }
    
    //MARK:- TAP LOCATION IMAGE
    func didTapImage (){
       // let arrayImages : [String] = location.locationImageName.componentsSeparatedByString(",")

        if(imageViewLocation.image == UIImage(named: "UserPlaceholder") && arrayImages.count == 0 ){
        return
        }
        
        let contentView : UIView = UIView(frame: UIScreen.mainScreen().bounds)
        let navView : UIView = self.navigationController!.view
        contentView.backgroundColor = UIColor.blackColor()
        //UIColor.clearColor().colorWithAlphaComponent(0.75)
        scrollviewImage = UIScrollView(frame: contentView.bounds)
        scrollviewImage.pagingEnabled = true
        scrollviewImage.showsVerticalScrollIndicator = false
        scrollviewImage.showsHorizontalScrollIndicator = false
        scrollviewImage.backgroundColor = UIColor.clearColor()
        scrollviewImage.delegate = self
        currentIndex = 0
        contentView.addSubview(scrollviewImage)
        
        buttonLeftArrow = UIButton(type: .Custom)
        buttonLeftArrow.frame = CGRectMake(-10, screenSize.height/2 - 20, 40, 40)
        buttonLeftArrow.backgroundColor = UIColor.clearColor()
        buttonLeftArrow.setImage(UIImage(named: "LeftArrow"), forState: UIControlState.Normal)
        buttonLeftArrow.addTarget(self, action: #selector(LocationDetailsViewController.onLeftArrowPressed), forControlEvents: UIControlEvents.TouchUpInside)
        buttonLeftArrow.hidden = true
       contentView.addSubview(buttonLeftArrow)

        buttonRightArrow = UIButton(type: .Custom)
        buttonRightArrow.frame = CGRectMake(screenSize.width - 30, screenSize.height/2 - 20, 40, 40)
        buttonRightArrow.backgroundColor = UIColor.clearColor()
        buttonRightArrow.setImage(UIImage(named: "RightArrow"), forState: UIControlState.Normal)
        buttonRightArrow.addTarget(self, action: #selector(LocationDetailsViewController.onRightArrowPressed), forControlEvents: UIControlEvents.TouchUpInside)
        buttonRightArrow.hidden = arrayImages.count == 1
        contentView.addSubview(buttonRightArrow)

        ((arrayImages as NSArray).reverseObjectEnumerator().allObjects as NSArray).enumerateObjectsUsingBlock { (str, idx, stop) in
            let imageviewModal : UIImageView = UIImageView(frame: CGRectMake(screenSize.width * CGFloat(idx), 0, contentView.frame.width, contentView.frame.height))
            imageviewModal.contentMode = UIViewContentMode.ScaleAspectFit
            imageviewModal.sd_setImageWithURL(NSURL(string:_imagePathUser + (str as! String)), placeholderImage: UIImage(named: "UserPlaceholder"))
            self.scrollviewImage.addSubview(imageviewModal)
           // self.view.bringSubviewToFront(imageviewModal)

        }
        
        let buttonSave = UIButton(type: .Custom)
        buttonSave.frame = CGRectMake(screenSize.width - 60, 15, 50, 40)
        buttonSave.backgroundColor = UIColor.clearColor()
        buttonSave.setTitle("Save", forState: UIControlState.Normal)
        buttonSave.addTarget(self, action: #selector(LocationDetailsViewController.onSaveButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        contentView.addSubview(buttonSave)

        let floatValue = CGFloat(arrayImages.count)
         scrollviewImage.contentSize = CGSizeMake(floatValue * screenSize.width, 0)
            UIView.transitionWithView(self.view, duration: 0.50, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
                navView.addSubview(contentView)
                }, completion: nil)
            self.tableView.scrollEnabled = false
            let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(LocationDetailsViewController.setUpTapGesture(_:)))
            tapGesture.numberOfTouchesRequired = 1
            tapGesture.numberOfTapsRequired = 1
            contentView.addGestureRecognizer(tapGesture)
            tapGesture.delegate = self
//        }
    }
    
    func onSaveButtonPressed() {
        let imageName = ((arrayImages as NSArray).reverseObjectEnumerator().allObjects as NSArray).objectAtIndex(currentIndex) as! String
        SDWebImageDownloader.sharedDownloader().downloadImageWithURL(NSURL(string:_imagePathUser + imageName ), options: SDWebImageDownloaderOptions.HandleCookies, progress: nil) { (image, data, error, completed) in
            if #available(iOS 8.0, *) {
                CustomPhotoAlbum.sharedInstance.saveImage(image)
            } else {
                UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
                // Fallback on earlier versions
            }
            print("done")
            
        }
        
    }
    func onLeftArrowPressed()  {
        scrollviewImage.setContentOffset(CGPointMake(CGFloat(currentIndex - 1) * screenSize.width, 0), animated: true)
    }
    
    func onRightArrowPressed() {
        scrollviewImage.setContentOffset(CGPointMake(CGFloat(currentIndex + 1) * screenSize.width, 0), animated: true)

    }

    //MARK:- SCROLL VIEW DELEGATE
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView == scrollviewImage{
            currentIndex = (NSInteger(scrollviewImage.contentOffset.x) / NSInteger(scrollviewImage.frame.size.width)) as NSInteger
            buttonLeftArrow.hidden = currentIndex == 0
            buttonRightArrow.hidden = currentIndex == arrayImages.count - 1
        }
        
    }

    //MARK:- TAP GESTURE ACTION
    func setUpTapGesture(gestureRecognizer : UITapGestureRecognizer){
        UIView.transitionWithView(self.view, duration: 0.5, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
            gestureRecognizer.view!.removeFromSuperview()
            }, completion: nil)
        self.tableView.scrollEnabled = true
    }

}
