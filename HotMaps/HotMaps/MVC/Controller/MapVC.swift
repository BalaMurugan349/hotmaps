//
//  MapVC.swift
//  HotMaps
//
//  Created by Srishti Innovative on 28/09/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController,MKMapViewDelegate, CLLocationManagerDelegate,
AnnotationViewDelegate,LocationCreatedDelegate {
    @IBOutlet weak var mapView: MKMapView!
    var locationArray: NSMutableArray!
    let locationManager = CLLocationManager()
    var selectedIndex: Int!

    var startupRegion : MKCoordinateRegion!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBarImage()
        addAnnotationsToMapView()
        // Do any additional setup after loading the view.
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.mapView.showsUserLocation = true
        self.mapView.delegate = self
        setUpUI()
       
        
    }
    func setUpUI(){
        let backBtn: UIButton = UIButton(type: UIButtonType.Custom)
        backBtn.frame = CGRectMake(0, 0, 25, 25) ;
        backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
        backBtn.addTarget(self, action:#selector(MapVC.backAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backButton
    }
    func backAction(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:- CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
    }
   

  /*  func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        
        var currentLocation = newLocation
        if currentLocation != nil{
            var locValue:CLLocationCoordinate2D = currentLocation.coordinate
            var lat = NSNumber(double: locValue.latitude)
            var lon = NSNumber(double: locValue.longitude)
            var userLocation: NSDictionary = ["lat":lat,"long":lon]
            
            NSUserDefaults.standardUserDefaults().setObject(userLocation, forKey: "userLocation")
            NSUserDefaults.standardUserDefaults().synchronize()
            locationManager.stopUpdatingLocation()
            
            //set user visible region
            self.startupRegion = MKCoordinateRegion()
            self.startupRegion.center = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
         //   self.startupRegion.span = MKCoordinateSpanMake(0.0005,0.0005)
            
            let viewRegion = MKCoordinateRegionMakeWithDistance(self.startupRegion.center, 152, 152)
            self.mapView.setRegion(viewRegion, animated: false)
            
           // self.mapView.setRegion(self.startupRegion, animated: true)
            
        }
    }*/
    func mapView(mapView: MKMapView, didUpdateUserLocation userLoc: MKUserLocation) {
        NSUserDefaults.standardUserDefaults().setDouble(userLoc.coordinate.latitude, forKey: "lastLatitude")
        NSUserDefaults.standardUserDefaults().setDouble(userLoc.coordinate.longitude, forKey: "lastLongitude")
        let userLocation: NSDictionary = ["lat":NSNumber(double: userLoc.coordinate.latitude),"long":NSNumber(double: userLoc.coordinate.longitude)]
        NSUserDefaults.standardUserDefaults().setObject(userLocation, forKey: "userLocation")
        NSUserDefaults.standardUserDefaults().synchronize()
        //set user visible region
        self.startupRegion = MKCoordinateRegion()
        self.startupRegion.center = CLLocationCoordinate2DMake(userLoc.coordinate.latitude, userLoc.coordinate.longitude)
        let viewRegion = MKCoordinateRegionMakeWithDistance(self.startupRegion.center, 152, 152)
        self.mapView.setRegion(viewRegion, animated: false)
    }

    //MARK:- Current User Location Button Action
    @IBAction func setUserRegion()
    {
        //set user visible region
        startupRegion = MKCoordinateRegion()
        startupRegion.center = CLLocationCoordinate2DMake(mapView.userLocation.coordinate.latitude, mapView.userLocation.coordinate.longitude)
      //  startupRegion.span = MKCoordinateSpanMake(0.005,0.005)
        let viewRegion = MKCoordinateRegionMakeWithDistance(self.startupRegion.center, 152, 152)
        self.mapView.setRegion(viewRegion, animated: false)
      //  mapView.setRegion(startupRegion, animated: true)
    }
    //MARK:- Adding Annotation to Map View
    func addAnnotationsToMapView(){
        self.locationArray.enumerateObjectsUsingBlock({ object, index, stop in
            let location = object as! LocationDetails
            let annotation: Annotation = Annotation(callOutMessage: location)
            self.mapView.addAnnotation(annotation)
        })
    }
    
    //MARK:- AnnotationViewDelegate
    func annotationClicked(sender: UIButton) {
        
    //    var tag = String(sender.tag)
       /* self.locationArray.enumerateObjectsUsingBlock({ object, index, stop in
            var location = object as! LocationDetails
            if location.id == tag{
                self.selectedIndex = index
                var locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
                locationDetailsViewController.location = location
                locationDetailsViewController.delegate = self
                if sender.userInteractionEnabled == true
                {
                    sender.userInteractionEnabled = false
                    self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
                }
            }
        })*/
    }
    //MARK:- LocationCreatedDelegate
    func reloadData() {
      //  self.locationArray.removeObjectAtIndex(selectedIndex)
      //  mapView.removeAnnotations(self.mapView.annotations)
      //  addAnnotationsToMapView()
    }
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier("AnnotationView")
        if annotation.isKindOfClass(Annotation){
            
            if pinView == nil{
                let arrMessageView = (NSBundle.mainBundle().loadNibNamed("AnnotationMessageView", owner: self, options: nil)! as NSArray)
                let msgView = arrMessageView[0] as! AnnotationMessageView
                (msgView as AnnotationMessageView).delegate = self
                (msgView as AnnotationMessageView).btn.userInteractionEnabled = true
                let annot = annotation as! Annotation
                
                msgView.setMessage(annot.callOutMessage)
                
                let imageViewPin = UIImageView(frame: CGRectMake(0, 0, 116 * self.view.frame.width/320.0, 30 * self.view.frame.width/320.0))
                imageViewPin.contentMode = UIViewContentMode.ScaleAspectFit
                imageViewPin.image = UIImage(named: "mapannotation")
                pinView = DXAnnotationView(annotation: annotation, reuseIdentifier: "MapPinIdentifier", pinView: imageViewPin, calloutView: msgView, settings: DXAnnotationSettings.defaultSettings())
                (pinView as! DXAnnotationView).showCalloutView()
                pinView!.layer.anchorPoint = CGPointMake (0.5, 1.0)
            }
            else{
                pinView!.annotation = annotation
            }
            
        }
        
        return pinView
    }
    
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        if view.isKindOfClass(DXAnnotationView){
            view.layer.zPosition = -1
        }
        
        
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        if view.isKindOfClass(DXAnnotationView){
            (view as! DXAnnotationView).showCalloutView()
            view.layer.zPosition = 0
        }
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
