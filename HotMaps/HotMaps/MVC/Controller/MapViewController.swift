//
//  MapViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 29/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, UIGestureRecognizerDelegate, AnnotationViewDelegate, bmDropDownDelegate, LocationCreatedDelegate,UISearchBarDelegate {
    
    //Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tablePlaces: UITableView!
    
    var searchResultPlaces: NSArray!
    var searchQuery: SPGooglePlacesAutocompleteQuery!
    
    var locationManager : CLLocationManager!
    var locationDetailsArray: NSMutableArray = NSMutableArray()
    var locationCoordinatesArray: NSMutableArray = NSMutableArray()
    var locArr: NSMutableArray = NSMutableArray()
    var location: CLLocation!
    
    var isLoadMoreData : Bool?
    var hasOverlay: Bool!
    var indexCount : NSInteger!
    //  var hasAnnotations: Bool! = false
    var showCallout: Bool! = false
    //  var isLoadingFinished: Bool!
    var startupRegion : MKCoordinateRegion!
    var updationCount: Int!
    var lastSelectedIndexPath: NSIndexPath!
    var rateDuration: NSString!
    var timerGetLocations: NSTimer!
    
    var showLeftMenu : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        ///OLD KEY (THUSARA ACCOUNT)
        ////AIzaSyDfzPH8vWSZNKhtnAjRJCz04Jxv2Mac4gw
        
        //IOS KEY
        //AIzaSyBEHgu0muiwquFjTNfW9pAjIvnRP07tk0U
        
        //BROWSER KEY
        //AIzaSyBbaKmVxNWyiuXK9NnlN82oCW8LeBBLHGw
        
        //SERVER KEY
        //AIzaSyAV-2QcYUoR3pkBdhBlZuquy5mPF94_d1c
        searchQuery = SPGooglePlacesAutocompleteQuery(apiKey:"AIzaSyBEHgu0muiwquFjTNfW9pAjIvnRP07tk0U")
        
        setUpUI()
        self.setInititalValues()
        
        rateDuration = ""
        
        //Initializing Location Manager
        locationManager = locationManager != nil ?  locationManager : CLLocationManager()
        locationManager.delegate = self
        let state = CLLocationManager.authorizationStatus()
        if state == .NotDetermined && iOS8
        {
            if #available(iOS 8.0, *) {
                locationManager.requestWhenInUseAuthorization()
                locationManager.requestAlwaysAuthorization()
            } else {
                // Fallback on earlier versions
            }
        }
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        self.mapView.showsUserLocation = true
        self.mapView.delegate = self
        if  NSUserDefaults.standardUserDefaults().objectForKey("PopUpShown") == nil{
            showWelcomePopUp()
        }else{
            if showLeftMenu == true{
                self.performSelector(#selector(MapViewController.onCloseButtonPressed), withObject: nil, afterDelay: 1.0)
                //slideMenuController()?.toggleLeft()
                showLeftMenu = false
            }

        }
        
        if #available(iOS 9.0, *) {
            UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).font =  UIFont.init(name: "Futura", size: 14)
        } else {
            checkSubview(view)
        }
        
         mapView.mapType = MKMapType.Hybrid
        if  NSUserDefaults.standardUserDefaults().objectForKey("userLocation") != nil  {
            resetItems()
            getRatedLocations()
            startTimer()
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        setUserLocation()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MapViewController.onCloseButtonPressed), name: "KGModalClosed", object: nil)


    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

    }
    //MARK:- Check SubView
    func checkSubview(view:UIView){
        for subView in view.subviews {
            if subView is UITextField {
                let textField = subView as! UITextField
                textField.font! = UIFont.init(name: "Futura", size: 14)!
            } else {
                checkSubview(subView)
            }
        }
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        mapView.showsPointsOfInterest = false
        hasOverlay = false
        updationCount = 0;
        self.setNavigationBarItem()
        self.mapView.showsUserLocation = true
        self.mapView.delegate = self

       // mapView.showsPointsOfInterest = false
       // mapView.mapType = MKMapType.Hybrid
        
//        if  NSUserDefaults.standardUserDefaults().objectForKey("userLocation") != nil && hasOverlay == false {
//            resetItems()
//            getRatedLocations()
//            startTimer()
//        }
        
        
    }
    /*  func mapView(mapView: MKMapView, didAddAnnotationViews views: [MKAnnotationView]) {
    let ulv = mapView.viewForAnnotation(mapView.userLocation)
    if showCallout == true{
    ulv?.hidden = true
    }
    else{
    ulv?.hidden = false
    }
    }*/
    //MARK:- Show Welcome Pop Up
    func showWelcomePopUp(){
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "PopUpShown")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        let welcomeView: UIView = UIView(frame: CGRectMake(0,0,screenSize.width - 40, 400))
        welcomeView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        let labelWelcome = UILabel(frame: CGRectMake(0,10,200, 30))
        labelWelcome.textAlignment = NSTextAlignment.Center
        labelWelcome.center.x = welcomeView.center.x
        labelWelcome.font = UIFont.init(name: "Futura-CondensedMedium", size: 30)
        labelWelcome.textColor = UIColor.whiteColor()
        labelWelcome.text = "Welcome"
        welcomeView.addSubview(labelWelcome)
        
        let imagePinch : UIImageView =  UIImageView(frame: CGRectMake(12,CGRectGetMaxY(labelWelcome.frame) + 30,67,46))
        imagePinch.image = UIImage(named: "pinch")
        imagePinch.contentMode = UIViewContentMode.ScaleAspectFill
        welcomeView.addSubview(imagePinch)
        let labelPinch = UILabel(frame: CGRectMake(75,0 + 30,200, 21))
        labelPinch.center.y = imagePinch.center.y
        labelPinch.textAlignment = NSTextAlignment.Left
        labelPinch.font = UIFont.init(name: "Futura-CondensedMedium", size: 15)
        labelPinch.textColor = UIColor.whiteColor()
        labelPinch.text = "Zoom in to explore what's popular"
        welcomeView.addSubview(labelPinch)
        
        let imageFilter : UIImageView =  UIImageView(frame: CGRectMake(30,CGRectGetMaxY(labelPinch.frame) + 30,27,26))
        imageFilter.image = UIImage(named: "filter")
        imageFilter.contentMode = UIViewContentMode.ScaleAspectFit
        welcomeView.addSubview(imageFilter)
        let labelFilter = UILabel(frame: CGRectMake(75,0 + 30,200, 21))
        labelFilter.center.y = imageFilter.center.y
        labelFilter.textAlignment = NSTextAlignment.Left
        labelFilter.font = UIFont.init(name: "Futura-CondensedMedium", size: 15)
        labelFilter.textColor = UIColor.whiteColor()
        labelFilter.text = "Filter based on your preferences"
        welcomeView.addSubview(labelFilter)
        
        
        let imageTime : UIImageView =  UIImageView(frame: CGRectMake(30,CGRectGetMaxY(labelFilter.frame) + 30,30,29))
        imageTime.image = UIImage(named: "clock")
        imageTime.contentMode = UIViewContentMode.ScaleAspectFit
        welcomeView.addSubview(imageTime)
        let labelTime = UILabel(frame: CGRectMake(75,0 + 30,200, 40))
        labelTime.center.y = imageTime.center.y
        labelTime.textAlignment = NSTextAlignment.Left
        labelTime.font = UIFont.init(name: "Futura-CondensedMedium", size: 15)
        labelTime.textColor = UIColor.whiteColor()
        labelTime.text = "See what's popular this month or right now"
        labelTime.numberOfLines = 2
        welcomeView.addSubview(labelTime)
        
        
        let imageSort : UIImageView =  UIImageView(frame: CGRectMake(25,CGRectGetMaxY(labelTime.frame) + 30,43,23))
        imageSort.image = UIImage(named: "sort")
        imageSort.contentMode = UIViewContentMode.ScaleAspectFit
        welcomeView.addSubview(imageSort)
        let labelSort = UILabel(frame: CGRectMake(75,0 + 30,200, 21))
        labelSort.center.y = imageSort.center.y
        labelSort.textAlignment = NSTextAlignment.Left
        labelSort.font = UIFont.init(name: "Futura-CondensedMedium", size: 15)
        labelSort.textColor = UIColor.whiteColor()
        labelSort.text = "Sort Results"
        welcomeView.addSubview(labelSort)
        
        let labelEarn = UILabel(frame: CGRectMake(0,CGRectGetMaxY(labelSort.frame) + 40,200, 30))
        labelEarn.center.x = welcomeView.center.x
        labelEarn.textAlignment = NSTextAlignment.Center
        labelEarn.font = UIFont.init(name: "Futura-CondensedMedium", size: 17)
        labelEarn.textColor = UIColor.whiteColor()
        labelEarn.text = "Earn points by rating Locations"
        welcomeView.addSubview(labelEarn)
        
        let labelInfo = UILabel(frame: CGRectMake(0,CGRectGetMaxY(labelEarn.frame) + 30,CGRectGetWidth(welcomeView.frame), 21))
        labelInfo.center.x = welcomeView.center.x
        labelInfo.textAlignment = NSTextAlignment.Center
        labelInfo.font = UIFont.init(name: "Futura-CondensedMedium", size: 17)
        labelInfo.textColor = UIColor.whiteColor()
        welcomeView.addSubview(labelInfo)
        
        let strInfo = NSMutableAttributedString(string: "more points ")
        let likeAttachment = NSTextAttachment()
        likeAttachment.image = UIImage(named: "arrow")
        let attachmentString = NSAttributedString(attachment: likeAttachment)
        strInfo.appendAttributedString(attachmentString)
        
        let attrString = NSAttributedString(string: " more ")
        strInfo.appendAttributedString(attrString)
        
        let attrStringSway = NSMutableAttributedString(string:"Sway ")
        attrStringSway.addAttribute(NSForegroundColorAttributeName, value: UIColor.greenColor(), range: NSRange(location: 0, length: attrStringSway.length ))
        strInfo.appendAttributedString(attrStringSway)
        
        strInfo.appendAttributedString(attachmentString)
        
        let attrString1 = NSAttributedString(string: " more influence")
        strInfo.appendAttributedString(attrString1)
        labelInfo.attributedText = strInfo
        
        KGModal.sharedInstance().showWithContentView(welcomeView, andAnimated: true)
    }
    
    func onCloseButtonPressed (){
        slideMenuController()?.toggleLeft()
    }
    
    func startTimer(){
        timerGetLocations = NSTimer(timeInterval: 3.0, target: self, selector:#selector(MapViewController.loadLocations), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timerGetLocations, forMode: NSRunLoopCommonModes)
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        timerGetLocations.invalidate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- setUpUI
    func setUpUI(){
        
        self.setNavBarImage()
        
        //UIBarButton Items
        let filterBtn: UIButton = UIButton(type: UIButtonType.Custom)
        filterBtn.frame = CGRectMake(0, 0, 25, 25) ;
        filterBtn.setImage(UIImage(named:"filterBtn.png"), forState: UIControlState.Normal)
        filterBtn.addTarget(self, action:#selector(MapViewController.filterAction), forControlEvents: UIControlEvents.TouchUpInside)
        let filterButton: UIBarButtonItem = UIBarButtonItem(customView: filterBtn)
        
        let rateBtn: UIButton = UIButton(type:UIButtonType.Custom)
        rateBtn.frame = CGRectMake(0, 0, 25, 25) ;
        rateBtn.setImage(UIImage(named:"clock.png"), forState: UIControlState.Normal)
        rateBtn.addTarget(self, action:#selector(MapViewController.rateAction), forControlEvents: UIControlEvents.TouchUpInside)
        let rateButton: UIBarButtonItem = UIBarButtonItem(customView: rateBtn)
        
        self.navigationItem.setRightBarButtonItems([filterButton,rateButton], animated: true)
    }
    //MARK:- UIButtonAction Globe
    @IBAction func actionGlobe(sender: UIButton){
        sender.userInteractionEnabled = false
        if mapView.mapType == MKMapType.Standard{
            let currentZoomScale:MKZoomScale = mapView.bounds.size.width / CGFloat(mapView.visibleMapRect.size.width)
            let strZoom: NSString = "\(currentZoomScale)"
            if strZoom.floatValue >   0.0156249992260516{
                if mapView.userLocation.location != nil {
                    self.startupRegion = MKCoordinateRegion()
                    self.startupRegion.center = CLLocationCoordinate2DMake(mapView.userLocation.coordinate.latitude, mapView.userLocation.coordinate.longitude)
                    self.startupRegion.span = MKCoordinateSpanMake(0.07,0.07)
                    self.mapView.setRegion(self.startupRegion, animated: true)
                    mapView.removeAnnotations(mapView.annotations)
                    
                    mapView.mapType = MKMapType.Hybrid
                }
            }
            else{
                mapView.mapType = MKMapType.Hybrid
                showorhideAnnotations()
            }
            sender.userInteractionEnabled = true
        }
            
        else if mapView.mapType == MKMapType.Hybrid{
            
            mapView.mapType = MKMapType.Standard
            if mapView.annotations.count <= 1{
                addAnnotationsToMapView()
            }
            //showorhideAnnotations()
            sender.userInteractionEnabled = true
        }
        
        
        
    }
    //MARK:- resetItems
    func resetItems(){
        showCallout = false
        tablePlaces.hidden = true
        locationDetailsArray.removeAllObjects()
        locationCoordinatesArray.removeAllObjects()
        //    hasAnnotations = false
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)
        
        //   isLoadingFinished = false
        indexCount = 0
        isLoadMoreData = true
        hasOverlay = false
        
    }
    
    //MARK:- Current User Location Button Action
    @IBAction func setUserRegion(sender: UIButton)
    {
        sender.userInteractionEnabled = false
        mapView.showsUserLocation = true
        setUserLocation()
        showorhideAnnotations()
        for annotation in mapView.annotations{
            if annotation.isKindOfClass(Annotation){
                showCallout = true
                mapView.selectAnnotation(annotation, animated: true)
            }
        }
        sender.userInteractionEnabled = true
        
    }
    func setUserLocation(){
        if mapView.userLocation.location != nil {
            //set user visible region
            self.startupRegion = MKCoordinateRegion()
            self.startupRegion.center = CLLocationCoordinate2DMake(mapView.userLocation.coordinate.latitude, mapView.userLocation.coordinate.longitude)
            self.startupRegion.span = MKCoordinateSpanMake(0.05,0.05)
            self.mapView.setRegion(self.startupRegion, animated: true)
        }
        if  hasOverlay == false  {
            //resetItems()
//            if timerGetLocations != nil{
//                timerGetLocations.invalidate()
//            }
//            getRatedLocations()
//            startTimer()
            
        }
    }
    //MARK:- SETINITIALVALUES
    func setInititalValues()
    {
        if  NSUserDefaults.standardUserDefaults().objectForKey("userLocation") == nil {
            let userLocation: NSDictionary = ["lat":40.726533,"long":-74.002203]
            NSUserDefaults.standardUserDefaults().setObject(userLocation, forKey: "userLocation")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        let latitude : Double? = NSUserDefaults.standardUserDefaults().doubleForKey("lastLatitude")
        let longitude : Double? = NSUserDefaults.standardUserDefaults().doubleForKey("lastLongitude")
        
        //set user visible region
        startupRegion = MKCoordinateRegion()
        if latitude != 0.0 && longitude != 0.0 {
            startupRegion.center = CLLocationCoordinate2DMake(latitude!, longitude!)
        } else {
            startupRegion.center = CLLocationCoordinate2DMake(mapView.userLocation.coordinate.latitude, mapView.userLocation.coordinate.longitude)
        }
        self.startupRegion.span = MKCoordinateSpanMake(0.05,0.05)
        self.mapView.setRegion(self.startupRegion, animated: true)
    }
    
    //MARK:- PARSE
    //MARK:- Getting Locations
    func getRatedLocations(){
        //http://52.88.60.242/HotMaps/Api.php?type=listLocation&devicetoken=12345&latitude=11.2505&longitude=75.7700&index=0
        
      //  locArr = NSMutableArray()
        hasOverlay = true
        
        if indexCount == 0{
            dispatch_async(dispatch_get_main_queue()){
                SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)}
        }
        var userLocation : NSDictionary
        if  NSUserDefaults.standardUserDefaults().objectForKey("userLocation") != nil {
            userLocation  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as! NSDictionary
        }
        else{
            userLocation = ["long" : "-75.159192", "lat" : "39.961731"]
        }
        var token: NSString?
        if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
            token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
        }
        token = token == nil ? "12345" : token
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("listLocation", key: "type")
        let userId = User.currentUser().userId == nil ? "" : User.currentUser().userId
        params.addValue(userId, key: "userId")
        params.addValue(userLocation["lat"]!, key: "latitude")
        params.addValue(userLocation["long"]!, key: "longitude")
        params.addValue(token!, key: "devicetoken")
        params.addValue("\(indexCount)", key: "index")
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            
            dispatch_async(dispatch_get_main_queue()){
                SVProgressHUD.dismiss()}
            if error == nil && result!["Result"] as! String == "Success"
            {
                if(self.indexCount == 0) {self.locationCoordinatesArray.removeAllObjects()
                    self.locationDetailsArray.removeAllObjects()
                    self.locArr.removeAllObjects()
                }
                self.locArr.addObjectsFromArray((result as! NSDictionary).objectForKey("Details") as! [AnyObject])
                self.locationDetailsArray.addObjectsFromArray((result as! NSDictionary).objectForKey("Details") as! [AnyObject])
                
                self.isLoadMoreData = self.locationDetailsArray.count % 10 == 0 ? true : false
                
                if self.isLoadMoreData == false{
                    self.timerGetLocations.invalidate()
                }
                self.plotLocations()
                if self.mapView.mapType == MKMapType.Standard{
                    self.addAnnotationsToMapView()
                }
            }
            else  if error == nil && result!["Result"] as! String == "Failed"
            {
                self.timerGetLocations.invalidate()
                self.isLoadMoreData = false
                self.plotLocations()
                if self.mapView.mapType == MKMapType.Standard{
                    self.addAnnotationsToMapView()
                }
            }
            else{
                self.timerGetLocations.invalidate()
                dispatch_async(dispatch_get_main_queue()){
                    SVProgressHUD.showErrorWithStatus("Please check your internet connection")
                }
            }
            
            
        }
    }
    //MARK:- bmDropDownDelegate
    func selectedValueFromBMDropDowm(value : NSString, idx: NSIndexPath) {
        lastSelectedIndexPath = idx
        switch lastSelectedIndexPath.row{
        case 0:
            rateDuration = "0"
        case 1:
            rateDuration = "1"
        case 2:
            rateDuration = "2"
        case 3:
            rateDuration = "3"
        case 4:
            rateDuration = ""
        default:
            break
        }
//        resetItems()
//       getRatedLocations()
//        showCallout = false
//        tablePlaces.hidden = true
//        locationDetailsArray.removeAllObjects()
//        locationCoordinatesArray.removeAllObjects()
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)
//        indexCount = 0
//        isLoadMoreData = true
//        hasOverlay = false
        plotLocations()
        if self.mapView.mapType == MKMapType.Standard{
            self.addAnnotationsToMapView()
        }
        if timerGetLocations != nil{
            timerGetLocations.invalidate()
        }
        
       // startTimer()
        //  getRatedLocations()
    }
    
    //MARK:- Plot Locations
    func plotLocations(){
        let arr = NSMutableArray()
        
        /* print(locationDetailsArray.count)
        print("****")
        print(locationCoordinatesArray.count)*/
        
        // let arrLoc = NSMutableArray()
        /* if self.searchActive == true{
        
        self.locationDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
        var dict = object as! NSDictionary
        var location: LocationDetails = LocationDetails(locationDict: dict)
        for (idx,item) in enumerate(self.filtered){
        
        if dict["locationname"] as! String == item
        {
        var annotation: Annotation = Annotation(callOutMessage: location)
        self.mapView.addAnnotation(annotation)
        var dict1 = ["lat" : dict["latitude"]!, "lon": dict["longitude"]!]
        self.locationCoordinatesArray.addObject(dict1)
        
        var location: LocationDetails = LocationDetails(locationDict: dict)
        var dict2 = ["lat" : dict["latitude"]!, "lon": dict["longitude"]!, "like": location.likePercent.integerValue]
        arr.addObject(dict2)
        print(arr)
        
        
        var lat = Double(location.latitude)
        var long = Double(location.longitude)
        var loc = CLLocation(latitude: lat , longitude:  long)
        arrLoc.addObject(loc)
        }
        }
        })
        centerMap(arrLoc)
        var objHeatMapViewController = HeatMapViewController()
        var heatMap: HeatMap = HeatMap(data: objHeatMapViewController.heatMapData(arr))
        mapView.addOverlay(heatMap)
        
        }
        else
        {*/
        
        self.locArr.enumerateObjectsUsingBlock({ object, index, stop in
            let dict = object as! NSDictionary
            let dict1 = ["lat" : dict["latitude"]!, "lon": dict["longitude"]!]
            self.locationCoordinatesArray.addObject(dict1)
            
            let location: LocationDetails = LocationDetails(locationDict: dict)
            let dict2 = ["lat" : dict["latitude"]!, "lon": dict["longitude"]!, "like": location.likePercent.integerValue]
            if self.rateDuration == ""{
                arr.addObject(dict2)
            }
            else  if self.rateDuration == "0"{
                if location.isRatedToday == true{
                    arr.addObject(dict2)
                }
            }
            else  if self.rateDuration == "1"{
                if location.isRatedThisWeek == true{
                    arr.addObject(dict2)
                }
            }
            else  if self.rateDuration == "2"{
                
                if location.isRatedThisMonth == true{
                    arr.addObject(dict2)
                }
            }
            else  if self.rateDuration == "3"{
                
                if location.isRatedThisYear == true{
                    arr.addObject(dict2)
                }
            }
        })
        mapView.removeOverlays(mapView.overlays)

        let objHeatMapViewController = HeatMapViewController()
        let heatMap: HeatMap = HeatMap(data: objHeatMapViewController.heatMapData(arr))
        mapView.addOverlay(heatMap)
        
    }
    
    
    func showorhideAnnotations(){
        
        if mapView.mapType == .Hybrid {
            mapView.removeAnnotations(mapView.annotations)
        }
        else{
            if mapView.annotations.count <= 1{
                addAnnotationsToMapView()
            }
        }
        let currentZoomScale:MKZoomScale = mapView.bounds.size.width / CGFloat(mapView.visibleMapRect.size.width)
        let strZoom: NSString = "\(currentZoomScale)"
        
        if strZoom.floatValue >   0.0156249992260516{
            //More Zoomed in, switch to Standard Map Type and Show Titles
            if self.mapView.mapType != MKMapType.Standard{
                self.mapView.mapType = MKMapType.Standard
                if mapView.annotations.count <= 1{
                    addAnnotationsToMapView()
                }
            }
            /*  for annotation in mapView.annotations{
            if annotation.isKindOfClass(Annotation){
            showCallout = false
            mapView.selectAnnotation(annotation, animated: true)
            }
            }*/
        }
        else{
            // self.mapView.showsUserLocation = false
            /* for annotation in mapView.annotations{
            if annotation.isKindOfClass(Annotation){
            showCallout = true
            mapView.selectAnnotation(annotation, animated: true)
            }
            }*/
        }
    }
    
    func centerMap(arrLoc: NSMutableArray){
        var region: MKCoordinateRegion! = MKCoordinateRegion()
        
        var maxLat: CLLocationDegrees = -90
        var maxLon: CLLocationDegrees = -180
        var minLat: CLLocationDegrees = 90
        var minLon: CLLocationDegrees = 180
        for idx in (0..<arrLoc.count)
        {
            let currentLocation: CLLocation = arrLoc.objectAtIndex(idx) as! CLLocation
            if(currentLocation.coordinate.latitude > maxLat)
            {maxLat = currentLocation.coordinate.latitude;}
            if(currentLocation.coordinate.latitude < minLat)
            {minLat = currentLocation.coordinate.latitude;}
            if(currentLocation.coordinate.longitude > maxLon)
            {maxLon = currentLocation.coordinate.longitude;}
            if(currentLocation.coordinate.longitude < minLon)
            {minLon = currentLocation.coordinate.longitude;}
        }
        region.center.latitude     = (maxLat + minLat) / 2;
        region.center.longitude    = (maxLon + minLon) / 2;
        region.span.latitudeDelta  = maxLat - minLat;
        region.span.longitudeDelta = maxLon - minLon;
        
        mapView.setRegion(region, animated: true)
    }
    
    
    //MARK:- MKMapViewDelegate
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        return HeatMapRenderer(overlay: overlay)
    }
    
    
    func mapView(mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        showorhideAnnotations()
        if searchBar.isFirstResponder(){
            searchBar.resignFirstResponder()
        }
    }
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier("AnnotationView")
        // This is false if its a user pin
        
        if annotation.isKindOfClass(Annotation){
            if pinView == nil{
                let arrMessageView = (NSBundle.mainBundle().loadNibNamed("AnnotationMessageView", owner: self, options: nil)! as NSArray)
                let msgView = arrMessageView[0] as! AnnotationMessageView
                (msgView as AnnotationMessageView).delegate = self
                (msgView as AnnotationMessageView).btn.userInteractionEnabled = true
                let annot = annotation as! Annotation
                
                msgView.setMessage(annot.callOutMessage)
                
                let imageViewPin = UIImageView(frame: CGRectMake(0, 0, 116 * self.view.frame.width/320.0, 30 * self.view.frame.width/320.0))
                imageViewPin.contentMode = UIViewContentMode.ScaleAspectFit
                imageViewPin.image = UIImage(named: "mapannotation")
                pinView = DXAnnotationView(annotation: annotation, reuseIdentifier: "MapPinIdentifier", pinView: imageViewPin, calloutView: msgView, settings: DXAnnotationSettings.defaultSettings())
                //   (pinView as! DXAnnotationView).showCalloutView()
                pinView!.layer.anchorPoint = CGPointMake (0.5, 1.0)
                //mapView.deselectAnnotation(annotation, animated: true)
            }
            else{
                pinView!.annotation = annotation
            }
            
            
        }
        else{
            
            if annotation.isKindOfClass(MKUserLocation) == true &&  annotation.isKindOfClass(Annotation) == false{
                let userPin = "UserLocationView"
                
                if pinView == nil{
                    pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: userPin)
                    pinView!.image = UIImage(named: "blueMarker")
                }
                else{
                    pinView!.annotation = annotation
                }
                
                
                /*  if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(userPin)
                {
                return dequeuedView
                } else
                {
                let mkAnnotationView:MKAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: userPin)
                mkAnnotationView.image = UIImage(named: "blueMarker")
                /*   let offset:CGPoint = CGPoint(x: 0, y: -mkAnnotationView.image!.size.height / 2)
                mkAnnotationView.centerOffset = offset*/
                
                return mkAnnotationView
                }*/
                
            }
        }
        
        return pinView
    }
    
    
    
    
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        
        if view.isKindOfClass(DXAnnotationView){
            (view as! DXAnnotationView).hideCalloutView()
            //  view.layer.zPosition = -1
        }
        
        
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        
        // if showCallout == false{
        if view.isKindOfClass(DXAnnotationView){
            (view as! DXAnnotationView).showCalloutView()
        }
        /*    }
        else{
        if view.isKindOfClass(DXAnnotationView){
        (view as! DXAnnotationView).hideCalloutView()
        }
        }*/
    }
    
    //MARK:- AnnotationViewDelegate
    func annotationClicked(sender: UIButton) {
//         if User.currentUser().userId == nil{
//            showLoginAlert()
//         }else{
        let tag = String(sender.tag)
        self.locationDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
            let location: LocationDetails = LocationDetails(locationDict: self.locationDetailsArray[index] as! NSDictionary)
            if location.id == tag{
                let locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
                locationDetailsViewController.location = location
                locationDetailsViewController.delegate = self
//                if sender.userInteractionEnabled == true
//                {
//                    sender.userInteractionEnabled = false
                    self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
//                }
            }
        })
//        }
    }
    
    func showLoginAlert(){
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Sorry", message: "You must login to use this feature", preferredStyle: .Alert)
            
//            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
//                
//            }
//            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Login", style: .Default) { (action) in
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.didLogout(true)
            }
            alertController.addAction(OKAction)
            let signup = UIAlertAction(title: "Sign-up", style: .Default) { (action) in
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.showSignupPage(true)
            }
            alertController.addAction(signup)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }

    //MARK:- LocationCreatedDelegate
    func reloadData() {
    }
    func locationCreated() {
    }
    
    //MARK:- Bar Button Action - Filter
    func filterAction()
    {
        let filterViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FilterViewController") as! FilterViewController
        self.navigationController?.pushViewController(filterViewController, animated: true)
        
    }
    func rateAction(){
        // isRatingToday = true
        // resetItems()
        //  plotLocations()
        self.view.endEditing(true)
        let idx = NSIndexPath(forItem: 4, inSection: 0)
        let dropDownView : BMDropDown = BMDropDown()
        dropDownView.lastSelectedIndexPath = lastSelectedIndexPath == nil ? idx : lastSelectedIndexPath
        dropDownView.arrayToLoad = ["Rated Today","Rated This Week","Rated This Month","Rated This Year","All"]
        dropDownView.delegate = self
        dropDownView.showDropDown(dropDownFrame:  CGRectMake(self.view.frame.size.width - 200, 60, 200, 0), heightOfDropDown: 220.0)
    }
    //MARK:- Adding Annotation to Map View
    func addAnnotationsToMapView(){
        
        locationDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
            let location: LocationDetails = LocationDetails(locationDict: self.locationDetailsArray[index] as! NSDictionary)
            let annotation: Annotation = Annotation(callOutMessage: location)
            if !self.mapView.containsAnnotation(annotation) {
                
                if self.rateDuration == ""{
                    self.mapView.addAnnotation(annotation)            }
                else  if self.rateDuration == "0"{
                    if location.isRatedToday == true{
                        self.mapView.addAnnotation(annotation)
                    }
                }
                else  if self.rateDuration == "1"{
                    if location.isRatedThisWeek == true{
                        //    let annotation: Annotation = Annotation(callOutMessage: location)
                        self.mapView.addAnnotation(annotation)
                    }
                }  else  if self.rateDuration == "2"{
                    if location.isRatedThisMonth == true{
                        // let annotation: Annotation = Annotation(callOutMessage: location)
                        self.mapView.addAnnotation(annotation)
                    }
                }
                else  if self.rateDuration == "3"{
                    if location.isRatedThisYear == true{
                        self.mapView.addAnnotation(annotation)
                    }
                }
            }
            
        })
    }
    
    func  loadLocations(){
        if isLoadMoreData == true{
            indexCount = indexCount + 1
            getRatedLocations()
        }
        
    }
    
    
    //MARK:- UISearchBar Delegate
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
        // searchActive = searchBar.text == "" ? false : true
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        //  searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        // searchActive = false;
        tablePlaces.hidden = true
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        //   plotLoc(searchBar.text)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" { searchBar.resignFirstResponder() }
        print("search Text \(searchText)")
        tablePlaces.hidden = false
        let latitude : Double! = NSUserDefaults.standardUserDefaults().doubleForKey("lastLatitude")
        let longitude : Double! = NSUserDefaults.standardUserDefaults().doubleForKey("lastLongitude")
        searchQuery.location = CLLocation(latitude: latitude, longitude: longitude).coordinate
        searchQuery.input = searchText
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.show()}
        searchQuery.fetchPlaces { (places, error) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                SVProgressHUD.dismiss()
            }
            if error != nil{
            }
            else
            {
                self.searchResultPlaces = places
                
                self.tablePlaces.reloadData()
            }
        }
    }
    //MARK:- UITableView Data Source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchResultPlaces == nil || searchResultPlaces.count == 0{
            tablePlaces.hidden = true
        }
        else{
            tablePlaces.hidden = false
        }
        return searchResultPlaces != nil ?searchResultPlaces.count : 0;
        
    }
    
    func placeAtIndexPath(indexPath: NSIndexPath) -> SPGooglePlacesAutocompletePlace{
        return searchResultPlaces[indexPath.row] as! SPGooglePlacesAutocompletePlace
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "SPGooglePlacesAutocompleteCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        //  cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        cell!.textLabel!.text = placeAtIndexPath(indexPath).name as String
        cell!.textLabel!.textColor = textColor
        cell!.textLabel!.font = UIFont.init(name: "Futura-Medium", size: 14)
        return cell!
    }
    //MARK:- UITableView Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.view.endEditing(true)
        let place: SPGooglePlacesAutocompletePlace = placeAtIndexPath(indexPath)
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        place.resolveToPlacemark { (placemark, addressString, error ) -> Void in
            
            if (error != nil){
                dispatch_async(dispatch_get_main_queue()){
                    SVProgressHUD.dismiss()
                }
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Error", message: "Could not map selected place", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                        
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            else if placemark != nil{
                
                let selectedLocation = CLLocation(latitude:  placemark.location!.coordinate.latitude , longitude: placemark.location!.coordinate.longitude)
                let userLocationDict  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as! NSDictionary
                let userLocation = CLLocation(latitude: userLocationDict.objectForKey("lat")as! CLLocationDegrees, longitude: userLocationDict.objectForKey("long")as! CLLocationDegrees)
                var dist = userLocation.distanceFromLocation(selectedLocation) * 0.000621371
                dist = Double(round(100*dist)/100)
                
                let city = placemark.locality == nil ? "" : placemark.locality
                let countryName: String! = placemark.addressDictionary!["CountryCode"] == nil ? "" : placemark.addressDictionary!["CountryCode"] as! String
                
                let strArray = addressString.componentsSeparatedByString(",") as NSArray
                var locName: String?
                if strArray.count <= 2 {
                    locName = strArray.componentsJoinedByString(",")
                }
                else{
                    let arr = NSMutableArray()
                    for i in 0..<2{
                        arr.addObject(strArray[i])
                    }
                    locName = arr.componentsJoinedByString(",")
                    
                }
                locName = locName! + "_" + city! + "," + countryName!
                locName = locName!.stringByReplacingOccurrencesOfString("'", withString: "")
                let params = NSMutableData.postData() as! NSMutableData
                params.addValue("LocationDetails", key: "type")
                params.addValue(locName!, key: "locationname")
                ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
                    dispatch_async(dispatch_get_main_queue()){
                        SVProgressHUD.dismiss()
                    }
                    if error == nil && result!["Result"] as! String == "Success"
                    {
                        
                        let dict: NSDictionary? = (result as! NSDictionary).objectForKey("Details") as? NSDictionary
                        let location = LocationDetails(locationDict: dict!)
                        /* var arrLoc = NSMutableArray()
                        var loc = CLLocation(latitude: location.latitude , longitude:  location.longitude)
                        self.tablePlaces.hidden = true
                        arrLoc.addObject(loc)
                        self.centerMap(arrLoc)*/
                        
                        self.searchBar.resignFirstResponder()
                        let locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
                        locationDetailsViewController.location = location
                        locationDetailsViewController.delegate = self
                        self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
                    }
                    else if error == nil && result!["Result"] as! String == "Failed"{
                        
                        if User.currentUser().userId != nil{
                            if dist > 0.378{
                                UIAlertView(title: "Sorry", message: "You must be at that location to give your rating", delegate: nil, cancelButtonTitle: "OK").show()
                            }
                                
                            else{
                                let createLocationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CreateLocationViewController") as! CreateLocationViewController
                                createLocationViewController.delegate = self
                                createLocationViewController.isForRatingLocation = false
                                createLocationViewController.locationId = ""
                                createLocationViewController.locationName = locName
                                createLocationViewController.latitude = String(format: "%f", placemark.location!.coordinate.latitude)
                                createLocationViewController.longitude = String(format: "%f", placemark.location!.coordinate.longitude)
                                
                                self.navigationController?.pushViewController(createLocationViewController, animated: true)
                            }
                        }
                        else{
                            if #available(iOS 8.0, *) {
                                let alertController = UIAlertController(title: "Sorry", message: "You must login to use this feature", preferredStyle: .Alert)
                                
//                                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
//                                    
//                                }
//                                alertController.addAction(cancelAction)
                                
                                let OKAction = UIAlertAction(title: "Login", style: .Default) { (action) in
                                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                    appDelegate.didLogout(true)
                                }
                                alertController.addAction(OKAction)
                                
                                let signup = UIAlertAction(title: "Sign-up", style: .Default) { (action) in
                                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                    appDelegate.showSignupPage(true)
                                }
                                alertController.addAction(signup)
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue()){
                            SVProgressHUD.showErrorWithStatus("Please check your internet connection")
                        }
                    }
                }
            }
        }
    }
    
    
}

extension MapViewController: CLLocationManagerDelegate, MKMapViewDelegate
{
    //MARK:- CLLocationManagerDelegate
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation1: MKUserLocation) {
        
        //    if updationCount == 0{
        
        var loc: CLLocation
        if userLocation1.location != nil{
            loc = userLocation1.location!
        }
        else{
            loc = location
        }
        //       let loc: CLLocation = location == nil ? userLocation1.location : location
        
        
        NSUserDefaults.standardUserDefaults().setDouble(loc.coordinate.latitude, forKey: "lastLatitude")
        NSUserDefaults.standardUserDefaults().setDouble(loc.coordinate.longitude, forKey: "lastLongitude")
        let userLocation: NSDictionary = ["lat":NSNumber(double: loc.coordinate.latitude),"long":NSNumber(double: loc.coordinate.longitude)]
        NSUserDefaults.standardUserDefaults().setObject(userLocation, forKey: "userLocation")
        NSUserDefaults.standardUserDefaults().synchronize()
        //  setUserLocation()
        //   updationCount = updationCount + 1
        //  }
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        
        location = newLocation
        if location != nil{
            NSUserDefaults.standardUserDefaults().setDouble(newLocation.coordinate.latitude, forKey: "lastLatitude")
            NSUserDefaults.standardUserDefaults().setDouble(newLocation.coordinate.longitude, forKey: "lastLongitude")
            
            let userLocation: NSDictionary = ["lat":NSNumber(double: newLocation.coordinate.latitude),"long":NSNumber(double: newLocation.coordinate.longitude)]
            NSUserDefaults.standardUserDefaults().setObject(userLocation, forKey: "userLocation")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            locationManager.stopUpdatingLocation()
            self.mapView(mapView, didUpdateUserLocation: mapView.userLocation)
            updateUserLocationWithJson("\(newLocation.coordinate.latitude)", long: "\(newLocation.coordinate.longitude)")
            
            //set user visible region
            self.startupRegion = MKCoordinateRegion()
            self.startupRegion.center = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            self.startupRegion.span = MKCoordinateSpanMake(0.05,0.05)
            self.mapView.setRegion(self.startupRegion, animated: true)
            
        }
        if  hasOverlay == false  {
            resetItems()
            if timerGetLocations != nil{
                timerGetLocations.invalidate()
            }
            getRatedLocations()
            startTimer()
        }
        
    }
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
    }
    /*  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
    locationManager.stopUpdatingLocation()
    }
    */
    
    func updateUserLocationWithJson(lat : String , long : String)  {
        if User.currentUser().userId == nil{ return }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        params.addValue(lat, key: "latitude")
        params.addValue(long, key: "longitude")
        
        ServiceManager.fetchDataFromService("apilinks/change_loc.php?", parameters: params) { (success, result, error) -> Void in
            print(result)
        }

    }
    
}
