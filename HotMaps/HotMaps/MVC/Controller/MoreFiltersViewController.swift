//
//  MoreFiltersViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 08/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
protocol FilterPreferenceDelegate{
    func getFilterValues(filterArray: NSMutableArray)
    func actionSubmit(filterArray: NSMutableArray)
}

class MoreFiltersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var delegate: FilterPreferenceDelegate?
  
    var isFromCreateViewController: Bool?
    
    var filterPreferencesArray:NSMutableArray = NSMutableArray()
    var editedCellsArray:NSMutableArray = NSMutableArray()
    
    var point: CGPoint!
    
    var age, energy, casual : NSString!
    
    @IBOutlet weak var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.setNavBarImage()
        
        filterPreferencesArray   = [
            ["left": "Younger", "right" : "Older"],["left": "Mellow", "right" : "Wild"],
            ["left": "Casual", "right" : "Formal"],["left": "Male", "right" : "Female"],
            ["left": "Single", "right" : "Taken"],["left": "No Wait", "right" : "Long Wait"],
            ["left": "Empty", "right" : "Packed"],["left": "Inclusive", "right" : "Exclusive"],
            ["left": "$", "right" : "$$$$"]]
        
        addBackButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
           }
    
    //MARK:- Add Left Bar Button Back
    func addBackButton(){
        let backBtn: UIButton = UIButton(type:UIButtonType.Custom)
        backBtn.frame = CGRectMake(0, 0, 25, 25) ;
        backBtn.setImage(UIImage(named:"back"), forState: UIControlState.Normal)
        backBtn.addTarget(self, action:#selector(MoreFiltersViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backButton
    }
        func backAction()
    {
        if isFromCreateViewController == true {
            delegate!.getFilterValues(editedCellsArray)
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:- Submit Button Action
    @IBAction func actionSubmit(sender: AnyObject)
    {
        
        if isFromCreateViewController == true {
            delegate!.actionSubmit(editedCellsArray)
        }
        else{
        let filteredResultsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FilteredResultViewController") as! FilteredResultViewController
        filteredResultsViewController.preferencesArray = editedCellsArray
            
        self.navigationController?.pushViewController(filteredResultsViewController, animated: true)
        }
    }
    //MARK:- UITableView DataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterPreferencesArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell : FilterPreferencesTableViewCell? = tableView.dequeueReusableCellWithIdentifier("FilterPreferencesCell", forIndexPath: indexPath) as? FilterPreferencesTableViewCell
//        
//        if cell == nil {
//            cell = FilterPreferencesTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "FilterPreferencesCell")
//        }
       
        
        let filterDict = filterPreferencesArray.objectAtIndex(indexPath.row) as! NSDictionary
        cell!.leftLabel.text = filterDict.objectForKey("left") as? String
        cell!.rightLabel.text = filterDict.objectForKey("right") as? String
      
       // cell!.initWithDetails(filterDict)
        cell!.sliderPreference.tag = indexPath.row + 1
        
        
        cell!.sliderPreference.minimumValue = indexPath.row == 0 ? 18 : 0
        cell!.sliderPreference.maximumValue =  indexPath.row == 0 ? 65 : 100
        cell!.sliderPreference.value =  indexPath.row == 0 ? 41.5 : 50

        cell?.sliderPreference.minimumTrackTintColor = sliderUnselectedColor
        cell?.sliderPreference.setThumbImage(sliderUnselectedThunb, forState: .Normal)
        cell!.sliderPreference.addTarget(self, action:#selector(MoreFiltersViewController.actionSlider(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        cell!.ageRangeLabel1.hidden = indexPath.row == 0 ? false : true
        cell!.ageRangeLabel2.hidden = indexPath.row == 0 ? false : true
        cell!.ageValueLabel.hidden = indexPath.row == 0 ? false : true
        cell!.ageValueLabel.text = ""
       
        let rowVal = "\(cell!.sliderPreference.tag)"
        
        
        if editedCellsArray.count > 0{
        editedCellsArray.enumerateObjectsUsingBlock  ({obj, idx, stop  in
            let dict = obj as! NSDictionary
            
            if dict["row"] as! String == rowVal{
                let val: NSString =  (dict["val"] as? NSString)!
                cell?.sliderPreference.value = val.floatValue
                cell?.sliderPreference.minimumTrackTintColor = sliderColor
                cell?.sliderPreference.setThumbImage(sliderThumb, forState: .Normal)
            }
            
        })
        }
        return cell!
    }
    
    func actionSlider(sender: UISlider){
        
  
        let dict = ["row": "\(sender.tag)", "val" : String(format: "%.0f", sender.value)]
        
     
        if editedCellsArray.count == 0{
            editedCellsArray.addObject(dict)
        }else{
            editedCellsArray.enumerateObjectsUsingBlock  ({obj, idx, stop  in
                let dict1 = obj as! NSDictionary
                
                if dict1["row"] as! String == "\(sender.tag)"{
                    self.editedCellsArray.removeObject(dict1)
                    self.editedCellsArray.addObject(dict)
                }
                else{
                    self.editedCellsArray.addObject(dict)
                }
            })
        }
         tableView.reloadData()

     //   if !(sender.value  <  51 && sender.value  > 49) || (sender.value  > 41) {
       
        /*
        var buttonPosition: CGPoint = sender.convertPoint(CGPointZero, toView: self.tableView)
        
        var indexPath: NSIndexPath = tableView.indexPathForRowAtPoint(buttonPosition)!
        var filterCell: FilterPreferencesTableViewCell = tableView.cellForRowAtIndexPath(indexPath) as! FilterPreferencesTableViewCell
        
        var dict = ["row": "\(indexPath.row + 1)", "val" : String(format: "%.0f", sender.value)]
        if editedCellsArray.count == 0{
            editedCellsArray.addObject(dict)
        }else{
            editedCellsArray.enumerateObjectsUsingBlock  ({obj, idx, stop  in
                var dict1 = obj as! NSDictionary
                
                if dict1["row"] as! String == "\(indexPath.row)"{
                    self.editedCellsArray.removeObject(dict1)
                    self.editedCellsArray.addObject(dict)
                }
                else{
                    self.editedCellsArray.addObject(dict)
                }
            })
        }
        tableView.reloadData()
     //   }*/
    }
  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
