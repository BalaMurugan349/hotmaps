//
//  ProfileViewController.swift
//  HotMaps
//
//  Created by Bala on 01/04/2016.
//  Copyright © 2016 Srishti Innovative. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    var arrayRatingHistory : NSMutableArray = NSMutableArray()
    @IBOutlet weak var tableviewHistory : UITableView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var buttonProfilePic : UIButton!
    @IBOutlet weak var labelPoints : UILabel!
    @IBOutlet weak var labelFollowers : UILabel!
    @IBOutlet weak var buttonFollow : UIButton!
    @IBOutlet weak var labelFollowing : UILabel!
    @IBOutlet weak var labelRecentLocation : UILabel!

    var ratingsArray: NSMutableArray = NSMutableArray()
    var rankDetailsArray: NSMutableArray = NSMutableArray()
     var locationDistance: NSString = ""
    var progressView: ArcProgressView!
    var lastSelectedIndexPath: NSIndexPath!
    var imagePicker : UIImagePickerController!
    var userId : String!
    var followStatus : String!
    var userName : String!
    var isFromPushNotification : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.setNavBarImage()
        // Do any additional setup after loading the view.
        self.labelPoints.text = "0 \n pts"
        self.labelFollowers.text = "0 \n followers"
        self.labelFollowing.text = "0 \n following"


        self.fetchProfileDetails()
        progressView = ArcProgressView(diameter: 120.0, arcWidth: 10.0, arcRadian: 260)
        buttonProfilePic.superview?.addSubview(progressView)
        progressView.changeProgress(0, isAnimated: false)
        
        labelUsername.text = userName
        
//        let distButton: UIButton = UIButton(type: UIButtonType.Custom)
//        distButton.frame = CGRectMake(0, 0, 20, 20) ;
//        distButton.setImage(UIImage(named:"Location.png"), forState: UIControlState.Normal)
//        distButton.addTarget(self, action: #selector(ProfileViewController.distanceAction), forControlEvents: UIControlEvents.TouchUpInside)
//        let distanceButton: UIBarButtonItem = UIBarButtonItem(customView: distButton)
//        
//        self.navigationItem.rightBarButtonItem = distanceButton
        
        buttonProfilePic.layer.cornerRadius = 50
        buttonProfilePic.clipsToBounds = true
        buttonProfilePic.superview?.bringSubviewToFront(buttonProfilePic)
        
        buttonFollow.hidden = userId == User.currentUser().userId
        buttonFollow.setImage(followStatus == "Yes" ? UIImage(named: "Following") : UIImage(named: "Follow"), forState: UIControlState.Normal)
        
        let tapGesture = UITapGestureRecognizer(target: self, action:#selector(ProfileViewController.didTappedonFollowersCount))
        labelFollowers.addGestureRecognizer(tapGesture)
        labelFollowers.userInteractionEnabled = true
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action:#selector(ProfileViewController.didTappedOnFollowingsCount))
        labelFollowing.addGestureRecognizer(tapGesture1)
        labelFollowing.userInteractionEnabled = true
        
        if userId != User.currentUser().userId {
            labelRecentLocation.hidden = true
            buttonFollow.hidden = true
            self.tableviewHistory.hidden = true
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if followStatus == "" || isFromPushNotification == true{
            self.setNavigationBarItem()
        }else{
            let backBtn: UIButton = UIButton(type: UIButtonType.Custom)
            backBtn.frame = CGRectMake(0, 0, 25, 25) ;
            backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
            backBtn.addTarget(self, action:#selector(ProfileViewController.onBackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
            
            let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
            self.navigationItem.leftBarButtonItem = backButton
        }
        lastSelectedIndexPath = NSIndexPath(forItem: 3, inSection: 0)
    }

    override func viewDidLayoutSubviews() {
        progressView.center = CGPointMake(screenSize.width/2, 140)
    }
    
    func didTappedonFollowersCount() {
        let userVC = self.storyboard!.instantiateViewControllerWithIdentifier("UsersVC") as! UsersTableViewController
        userVC.isListFollowers = true
        userVC.followerId = userId
        userVC.linkType = "apilinks/FollowersList.php?"
        self.navigationController?.pushViewController(userVC, animated: true)

    }
    func didTappedOnFollowingsCount() {
        let userVC = self.storyboard!.instantiateViewControllerWithIdentifier("UsersVC") as! UsersTableViewController
        userVC.isListFollowers = true
        userVC.followerId = userId
        userVC.linkType = "apilinks/FollowingList.php?"
        self.navigationController?.pushViewController(userVC, animated: true)

    }
    func  onBackButtonPressed()  {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func onFollowButtonPressed (sender : UIButton){
        
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(userId, key: "userid")
        params.addValue(User.currentUser().userId, key: "otheruserid")

        ServiceManager.fetchDataFromService(followStatus == "Yes" ? "apilinks/unfollow.php?" : "apilinks/follow.php?", parameters: params) { (success, result, error) -> Void in
            
            if error == nil && result!["status"] as! Bool == true
            {
                self.buttonFollow.setImage(self.followStatus == "Yes" ? UIImage(named: "Follow") : UIImage(named: "Following") , forState: UIControlState.Normal)
                self.followStatus = self.followStatus == "Yes" ? "No" : "Yes"
                SVProgressHUD.dismiss()
            }
            else if error == nil && result!["status"] as! Bool == false
            {
                SVProgressHUD.showErrorWithStatus("Please try again")
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }

    }
    
    func fetchProfileDetails(){
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(userId, key: "userid")
        ServiceManager.fetchDataFromService("apilinks/ViewProfile.php?", parameters: params) { (success, result, error) -> Void in
            let result : NSDictionary = result as! NSDictionary
            if(result["status"] as! Bool == true){
                self.arrayRatingHistory.removeAllObjects()
                if result["users"] is NSArray { self.arrayRatingHistory.addObjectsFromArray(result["users"] as! [AnyObject]) }
                self.tableviewHistory.reloadData()
                let userdict : NSDictionary = (result["user"] as! NSArray).objectAtIndex(0) as! NSDictionary
                let points : String = userdict.objectForKey("points") as! String
                self.labelPoints.text = "\(points) \n pts"
                let followers : String = userdict.objectForKey("follower_count") as! String
                self.labelFollowers.text = "\(followers) \n followers"
                let followings : String = userdict.objectForKey("following_count") as! String
                self.labelFollowing.text = "\(followings) \n following"
                let imageName : String = userdict.objectForKey("profileimage") as! String
                self.buttonProfilePic.sd_setImageWithURL(NSURL(string: _imagePathUser + imageName), forState: UIControlState.Normal, placeholderImage: UIImage(named: "UserPlaceholder"))
                let privacy : String = userdict.objectForKey("privacy") as! String
                if(privacy == "public"){
                    self.labelRecentLocation.hidden = false
                    self.buttonFollow.hidden = self.userId == User.currentUser().userId
                        //&& (self.followStatus == "MyProfile" || self.followStatus == "")
                    self.tableviewHistory.hidden = false
                }
                SVProgressHUD.dismiss()

            }else{
                 SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
            self.getPoint()
        }

    }
    
    @IBAction func onProfilePicButtonPressed (sender : UIButton){
        if(userId != User.currentUser().userId) { return }

        UIActionSheet(title: "Choose", delegate: self, cancelButtonTitle:nil , destructiveButtonTitle:nil, otherButtonTitles: "Camera", "PhotoAlbum","Cancel").showInView((self.view)!)

    }
    
    func updateProfilePic(image : UIImage) {
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        //hotmapsapp.com/HotMaps/apilinks/EditUser.php?userid=18&(with = > 'profileimg')

        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        params.addValue(UIImageJPEGRepresentation(image, 0.7)!, key: "profileimg", filename: User.makeFileName())
        
        ServiceManager.fetchDataFromService("apilinks/EditUser.php?", parameters: params) { (success, result, error) -> Void in
            
            if error == nil && result!["status"] as! Bool == true
            {
                SVProgressHUD.dismiss()
            }
            else if error == nil && result!["status"] as! Bool == false
            {
                SVProgressHUD.showErrorWithStatus("Please try again")
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }

    }
    //////////OLD CALCULATION
    //MARK:- Get Points
    func getPoint(){
        
        rankDetailsArray = NSMutableArray()
        ratingsArray = NSMutableArray()
        
        //http://52.88.60.242/HotMaps/Api.php?type=allratedusers
        
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("allratedusers", key: "type")
        
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            
            SVProgressHUD.dismiss()
            if error == nil && result!["rseult"] as! String == "success"
            {
                if result!["Location"] as? Int == 0{
                    self.infoLabel.text = ""
                }
                else{
                    self.ratingsArray.addObjectsFromArray((result as! NSDictionary).objectForKey("Rates") as! [AnyObject])
                    
                    let totalLocations = result!["Location"] as? Int
                    self.calculatePoints(self.ratingsArray, totalLocations: totalLocations!)
                }
            }
            else if error == nil && result!["rseult"] as! String == "Failed"
            {
               // self.points = 0
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }
    }

    //MARK:- CALCULATE POINTS
    func calculatePoints(ratingsArray: NSMutableArray, totalLocations: Int ){
        let userLocationDict  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as? NSDictionary
        let userLocation = CLLocation(latitude: userLocationDict!.objectForKey("lat")as! CLLocationDegrees, longitude: userLocationDict!.objectForKey("long")as! CLLocationDegrees)
        
        
        var rank: Float = 0
        
        for (_,item) in self.ratingsArray.enumerate()
        {
            let dict = item as! NSDictionary
            
            let arrRates = dict["rates"] as? NSArray
            //if arrRates == nil { return }
            let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            let sortedArray = arrRates!.sortedArrayUsingDescriptors(NSArray(object: sortDescriptor) as! [NSSortDescriptor])
            let arr = NSMutableArray(array: sortedArray)
            let myArr = self.arrayFilteredForUniqueValuesOfKeyPath("location_name",arr: arr)
            
            var idx  = 0
            var time: NSString = ""
            for (index,item) in arrRates!.enumerate(){
                
                
                let dict = item as! NSDictionary
                let timeString = (dict["date"] as! String)
                if index == 0 {
                    idx = 0
                    time = timeString
                }
                else{
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                    let date1 = formatter.dateFromString(time as String)
                    let date2 = formatter.dateFromString(timeString)
                    let result  = date1!.compare(date2!)
                    if result == NSComparisonResult.OrderedAscending{
                        idx = index
                        time = timeString
                    }
                }
            }
            
            let loc: NSDictionary = (arrRates?.objectAtIndex(idx) as? NSDictionary)!
            
            let locationName = loc["location_name"] as! String
            let strArray = locationName.componentsSeparatedByString("_") as NSArray
            
            let locName: String = strArray.count  > 1 ? (strArray[1] as? String)! : ""
            
            
            let  latitude = (loc["latitude"] as? NSString)!.doubleValue
            let longitude = (loc["longitude"] as! NSString).doubleValue
            let location = CLLocation(latitude: latitude , longitude: longitude)
            
            let dist = userLocation.distanceFromLocation(location) * 0.000621371
            
            // let pt = "\(myArr.count * 150)"
            // let distance = "\(dist)"
            if dict["userid"] as? String == userId{
                rank = Float((myArr.count * 100) / totalLocations)
            }
            let dictRankDetails: Dictionary<String, String> = ["userId": dict["userid"] as! String, "username": dict["username"] as! String, "points": dict["points"] as! String,  "lastRated": locName, "distance" : "\(dist)" ]
            
            rankDetailsArray.addObject(dictRankDetails as AnyObject)
            
        }
        
        
        if locationDistance == ""{
            let
            descriptor =  NSSortDescriptor(key: "points.integerValue" as String, ascending: false)
            let sorted = self.rankDetailsArray.sortedArrayUsingDescriptors(NSArray(object: descriptor) as! [NSSortDescriptor])
            rankDetailsArray.removeAllObjects()
            rankDetailsArray = NSMutableArray(array: sorted)
        }
        else{
            let arr = NSMutableArray()
            self.rankDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
                let locationDict = object as! NSDictionary
                
                if (locationDict["distance"] as! NSString).floatValue <= self.locationDistance.floatValue{
                    arr.addObject(locationDict)
                }
            })
            rankDetailsArray.removeAllObjects()
            rankDetailsArray = NSMutableArray(array: arr)
        }
        dispatch_async(dispatch_get_main_queue()){
            self.calculatePercentile()
    }
    
}
    
    func calculatePercentile(){
        var percentile: Float = 0
        let totalScore = rankDetailsArray.count
        for (idx, item) in rankDetailsArray.enumerate(){
            let dict = item as! NSDictionary
            if dict["userId"] as? String == userId{
                percentile = (( Float(totalScore) - ( Float(idx) + 1)) * 100 ) / Float(totalScore)
            }
        }
        
        let myString = NSMutableAttributedString(string: "Has ")
        let range = NSRange(location: 0, length: myString.length)
        myString.addAttribute(NSForegroundColorAttributeName, value: sliderUnselectedColor, range: range)
        
        let fontMore = UIFont.boldSystemFontOfSize(16)
        let moreString = NSMutableAttributedString(string: "more ")
        moreString.addAttribute(NSFontAttributeName, value: fontMore, range: NSRange(location: 0, length: moreString.length))
        myString.appendAttributedString(moreString)

        
        let font = UIFont.italicSystemFontOfSize(14)
        let percentString = NSMutableAttributedString(string: "Sway ")
        percentString.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: percentString.length))
        myString.appendAttributedString(percentString)
        
        let myStringThan = NSMutableAttributedString(string: "than ")
        let rangeThan = NSRange(location: 0, length: myStringThan.length)
        myStringThan.addAttribute(NSForegroundColorAttributeName, value: sliderUnselectedColor, range: rangeThan)
        myString.appendAttributedString(myStringThan)


        
        let percent =  String(format: "%.0f", percentile)
        let attrString = NSMutableAttributedString(string: percent + "%")
        
        attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.greenColor(), range: NSRange(location: 0, length: attrString.length ))
        myString.appendAttributedString(attrString)
        
        let str = NSMutableAttributedString(string: " of HotMappers")
       // str.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: str.length))
        myString.appendAttributedString(str)
        
        infoLabel.attributedText = myString
        progressView.changeProgress(CGFloat(percentile/100), isAnimated: false)
        
    }
    func arrayFilteredForUniqueValuesOfKeyPath(keyPath: String, arr: NSMutableArray) -> [AnyObject] {
        let valueSeen: NSMutableSet = NSMutableSet()
        return arr.filteredArrayUsingPredicate(NSPredicate { (evaluatedObject, bindings) -> Bool in
            let value: AnyObject = evaluatedObject!.valueForKeyPath(keyPath)!
            if !valueSeen.containsObject(value) {
                valueSeen.addObject(value)
                return true
            }
            else {
                return false
            }
            })
    }

    //MARK:- UIBar Button Actions
    //MARK:- distance Action
    func distanceAction()
    {
        let dropDownView : BMDropDown = BMDropDown()
        dropDownView.lastSelectedIndexPath = lastSelectedIndexPath
        dropDownView.arrayToLoad = ["10 Miles","100 Miles","1000 Miles","World"]
        dropDownView.delegate = self
        dropDownView.showDropDown(dropDownFrame:  CGRectMake(screenSize.width - self.view.frame.size.width / 3.125 - 10, 60, self.view.frame.size.width / 3.125 + 10, 0), heightOfDropDown: 190.0)
    }
    
   
}

extension ProfileViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.arrayRatingHistory.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        let dict : NSDictionary = self.arrayRatingHistory[indexPath.row] as! NSDictionary
        let labelName : UILabel = cell?.viewWithTag(1) as! UILabel
        let loc = dict["locationname"] as? String == nil ? "" : dict["locationname"] as? String
        if loc != ""{
            let strArray = loc!.componentsSeparatedByString("_") as NSArray
             labelName.text = strArray[0] as? String
        }
        else{
             labelName.text = ""
        }

        let labelCount : UILabel = cell?.viewWithTag(2) as! UILabel
        labelCount.text = dict["likecount"] as? String

        
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let dict = arrayRatingHistory[indexPath.row] as! NSDictionary
        let location : LocationDetails = LocationDetails(locationDict: dict)
        let locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
        locationDetailsViewController.location = location
        self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
    }
}

extension ProfileViewController : bmDropDownDelegate{
    //MARK:- bmDropDownDelegate
    func selectedValueFromBMDropDowm(value : NSString, idx: NSIndexPath) {
        
        lastSelectedIndexPath = idx
        switch lastSelectedIndexPath.row{
        case 0:
            locationDistance = "10"
        case 1:
            locationDistance = "100"
        case 2:
            locationDistance = "1000"
        case 3:
            locationDistance = ""
            
        default:
            break
        }
        getPoint()
    }

}

extension ProfileViewController : UIActionSheetDelegate
{
    //MARK:- ACTIONSHEET DELEGATE
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        imagePicker = imagePicker == nil ?  UIImagePickerController() : imagePicker
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        switch (buttonIndex)
        {
        case 0 :
            let hasCamera : Bool = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
            imagePicker.sourceType = hasCamera ? UIImagePickerControllerSourceType.Camera : UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(imagePicker, animated: true, completion: nil)
            break
        case 1 :
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(imagePicker, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
}

extension ProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //MARK:- IMAGEPICKERCONTROLLER DELEGATE
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.dismissViewControllerAnimated(true) {
            self.openEditor(image)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func openEditor(image : UIImage) {
        
        let controller : PECropViewController = PECropViewController()
        controller.delegate = self;
        controller.image = image;
        
        let width = image.size.width;
        let height = image.size.height;
        let length = min(width, height)
        controller.imageCropRect = CGRectMake((width - length) / 2, (height - length) / 2,length, length);
        let nav : UINavigationController = UINavigationController(rootViewController: controller)
        self.presentViewController(nav, animated: true, completion: nil)
    }
    
}

extension ProfileViewController : PECropViewControllerDelegate{
    func cropViewController(controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        buttonProfilePic.setImage(croppedImage, forState: UIControlState.Normal)
        self.updateProfilePic(croppedImage)
    }
    func cropViewControllerDidCancel(controller: PECropViewController!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
