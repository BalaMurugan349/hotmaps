//
//  RankViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 04/08/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class RankViewController: UIViewController, bmDropDownDelegate {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var tableRank: UITableView!
    @IBOutlet weak var infoLabel: UILabel!
    
    var progressView: ArcProgressView!
    var lastSelectedIndexPath: NSIndexPath!
    var points: Int!
    var ratingsArray: NSMutableArray!
    var locationDistance: NSString!
    var rankDetailsArray: NSMutableArray = NSMutableArray()
    var totalScore: Float!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameLabel.adjustsFontSizeToFitWidth = true
        self.setNavBarImage()
        setUpUI()
        
        points = 0
        usernameLabel.text = User.currentUser().username as String
      
        progressView = ArcProgressView(diameter: 120.0, arcWidth: 10.0, arcRadian: 260)
        self.view.addSubview(progressView)
        progressView.changeProgress(0, isAnimated: false)
        
        let tapgesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onCurrentUserNameClicked))
        tapgesture.numberOfTapsRequired = 1
        usernameLabel.addGestureRecognizer(tapgesture)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onCurrentUserNameClicked()  {
        let profilevc = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
        profilevc.userId = User.currentUser().userId as String
        profilevc.followStatus = "MyProfile"
        profilevc.userName = User.currentUser().username as String
        profilevc.isFromPushNotification = false
        self.navigationController?.pushViewController(profilevc, animated: true)

    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarItem()
        getPoint()
        lastSelectedIndexPath = NSIndexPath(forItem: 3, inSection: 0)
        ratingsArray = NSMutableArray()
        rankDetailsArray = NSMutableArray()
        tableRank.reloadData()
        locationDistance = ""
    }
    
    override func viewDidLayoutSubviews() {
        progressView.center = CGPointMake(screenSize.width/2, 140)
    }
    
    //MARK:- SetUpUI
    func setUpUI(){
        let distButton: UIButton = UIButton(type: UIButtonType.Custom)
        distButton.frame = CGRectMake(0, 0, 20, 20) ;
        distButton.setImage(UIImage(named:"Location.png"), forState: UIControlState.Normal)
        distButton.addTarget(self, action:#selector(RankViewController.distanceAction), forControlEvents: UIControlEvents.TouchUpInside)
        let distanceButton: UIBarButtonItem = UIBarButtonItem(customView: distButton)
        
        self.navigationItem.rightBarButtonItem = distanceButton
    }
    
    //MARK:- Get Points
    func getPoint(){
        
        rankDetailsArray = NSMutableArray()
        ratingsArray = NSMutableArray()
        
        //http://52.88.60.242/HotMaps/Api.php?type=allratedusers
        
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
       
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("allratedusers", key: "type")
        params.addValue(User.currentUser().userId, key: "userid")
        
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            
            SVProgressHUD.dismiss()
            if error == nil && result!["rseult"] as! String == "success"
            {
                if result!["Location"] as? Int == 0{
                    self.infoLabel.text = ""
                }
                else{
                self.ratingsArray.addObjectsFromArray((result as! NSDictionary).objectForKey("Rates") as! [AnyObject])
                
                let totalLocations = result!["Location"] as? Int
                self.calculatePoints(self.ratingsArray, totalLocations: totalLocations!)
                }
            }
            else if error == nil && result!["rseult"] as! String == "Failed"
            {
                self.points = 0
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }
    }
    
    //MARK:- CALCULATE POINTS
    func calculatePoints(ratingsArray: NSMutableArray, totalLocations: Int ){
        let userLocationDict  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as? NSDictionary
        let userLocation = CLLocation(latitude: userLocationDict!.objectForKey("lat")as! CLLocationDegrees, longitude: userLocationDict!.objectForKey("long")as! CLLocationDegrees)
        
        
        var rank: Float = 0
        
        for (_,item) in self.ratingsArray.enumerate()
        {
            let dict = item as! NSDictionary
            
            let arrRates = dict["rates"] as? NSArray

            let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            let sortedArray = arrRates!.sortedArrayUsingDescriptors(NSArray(object: sortDescriptor) as! [NSSortDescriptor])
            let arr = NSMutableArray(array: sortedArray)
            let myArr = self.arrayFilteredForUniqueValuesOfKeyPath("location_name",arr: arr)
            
            var idx  = 0
            var time: NSString = ""
            for (index,item) in arrRates!.enumerate(){
                
                
                let dict = item as! NSDictionary
                let timeString = (dict["date"] as! String)
                if index == 0 {
                    idx = 0
                    time = timeString
                }
                else{
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                    let date1 = formatter.dateFromString(time as String)
                    let date2 = formatter.dateFromString(timeString)
                    let result  = date1!.compare(date2!)
                    if result == NSComparisonResult.OrderedAscending{
                        idx = index
                        time = timeString
                    }
                }
                }
            
            let loc: NSDictionary = (arrRates?.objectAtIndex(idx) as? NSDictionary)!
            
            let locationName = loc["location_name"] as! String
            let strArray = locationName.componentsSeparatedByString("_") as NSArray
            
            let locName: String = strArray.count  > 1 ? (strArray[1] as? String)! : ""
            
            
            let  latitude = (loc["latitude"] as? NSString)!.doubleValue
            let longitude = (loc["longitude"] as! NSString).doubleValue
            let location = CLLocation(latitude: latitude , longitude: longitude)
            
            let dist = userLocation.distanceFromLocation(location) * 0.000621371
            
           // let pt = "\(myArr.count * 150)"
           // let distance = "\(dist)"
            if dict["userid"] as? String == User.currentUser().userId{
                 rank = Float((myArr.count * 100) / totalLocations)
            }
            let dictRankDetails: Dictionary<String, String> = ["userId": dict["userid"] as! String, "username": dict["username"] as! String, "points": dict["points"] as! String,  "lastRated": locName, "distance" : "\(dist)" ,"FollowStatus" : dict["follow_status"] as! String ]
           
            rankDetailsArray.addObject(dictRankDetails as AnyObject)
            
        }
       
        
        if locationDistance == ""{
        let
            descriptor =  NSSortDescriptor(key: "points.integerValue" as String, ascending: false)
        let sorted = self.rankDetailsArray.sortedArrayUsingDescriptors(NSArray(object: descriptor) as! [NSSortDescriptor])
        rankDetailsArray.removeAllObjects()
        rankDetailsArray = NSMutableArray(array: sorted)
        }
        else{
            let arr = NSMutableArray()
            self.rankDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
                let locationDict = object as! NSDictionary
                
                if (locationDict["distance"] as! NSString).floatValue <= self.locationDistance.floatValue{
                    arr.addObject(locationDict)
                }
                })
            rankDetailsArray.removeAllObjects()
            rankDetailsArray = NSMutableArray(array: arr)
        }
        dispatch_async(dispatch_get_main_queue()){
            self.calculatePercentile()
            self.tableRank.reloadData()}
    }
    func calculatePercentile(){
        var percentile: Float = 0
        let totalScore = rankDetailsArray.count
        for (idx, item) in rankDetailsArray.enumerate(){
            let dict = item as! NSDictionary
            if dict["userId"] as? String == User.currentUser().userId{
                percentile = (( Float(totalScore) - ( Float(idx) + 1)) * 100 ) / Float(totalScore)
            }
        }
        
        let myString = NSMutableAttributedString(string: "You have more ")
        let range = NSRange(location: 0, length: myString.length)
        myString.addAttribute(NSForegroundColorAttributeName, value: sliderUnselectedColor, range: range)
        
        let font = UIFont.italicSystemFontOfSize(14)
        let percentString = NSMutableAttributedString(string: "Sway than ")
        percentString.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: percentString.length))
        myString.appendAttributedString(percentString)
        
        let percent =  String(format: "%.0f", percentile)
        let attrString = NSMutableAttributedString(string: percent + "%")
        
        attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.greenColor(), range: NSRange(location: 0, length: attrString.length ))
        myString.appendAttributedString(attrString)
        
        let str = NSMutableAttributedString(string: " of HotMappers")
        str.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: str.length))
        myString.appendAttributedString(str)
        
        infoLabel.attributedText = myString
        progressView.changeProgress(CGFloat(percentile/100), isAnimated: false)
        
    }
    func arrayFilteredForUniqueValuesOfKeyPath(keyPath: String, arr: NSMutableArray) -> [AnyObject] {
        let valueSeen: NSMutableSet = NSMutableSet()
        return arr.filteredArrayUsingPredicate(NSPredicate { (evaluatedObject, bindings) -> Bool in
            let value: AnyObject = evaluatedObject!.valueForKeyPath(keyPath)!
            if !valueSeen.containsObject(value) {
                valueSeen.addObject(value)
                return true
            }
            else {
                return false
            }
            })
        
        
    }

    //MARK:- UIBar Button Actions
    //MARK:- distance Action
    func distanceAction()
    {
        let dropDownView : BMDropDown = BMDropDown()
        dropDownView.lastSelectedIndexPath = lastSelectedIndexPath
        dropDownView.arrayToLoad = ["10 Miles","100 Miles","1000 Miles","World"]
        dropDownView.delegate = self
        dropDownView.showDropDown(dropDownFrame:  CGRectMake(screenSize.width - self.view.frame.size.width / 3.125 - 10, 60, self.view.frame.size.width / 3.125 + 10, 0), heightOfDropDown: 190.0)
    }
    //MARK:- bmDropDownDelegate
    func selectedValueFromBMDropDowm(value : NSString, idx: NSIndexPath) {

        lastSelectedIndexPath = idx
        switch lastSelectedIndexPath.row{
        case 0:
            locationDistance = "10"
        case 1:
            locationDistance = "100"
        case 2:
            locationDistance = "1000"
        case 3:
            locationDistance = ""
      
        default:
           break
        }
        getPoint()
    }

    //MARK:- UITableView DataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rankDetailsArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell : RankTableViewCell? = tableView.dequeueReusableCellWithIdentifier("RankTableViewCell") as? RankTableViewCell
        
        if cell == nil {
            cell = RankTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "FilterPreferencesCell")
        }
        let dict = rankDetailsArray[indexPath.row] as! NSDictionary
        cell?.labelUsername.text = dict["username"] as? String
        cell?.labelUsername.tag = indexPath.row
        // var arrRates = dict["rates"] as? NSArray
        // var rateCount = arrRates!.count
        //  var loc: NSDictionary = (arrRates?.firstObject as? NSDictionary)!
        cell?.labelPoints.text = dict["points"] as? String
        cell?.labellastRatedLocation.text = dict["lastRated"] as? String
        let tapgesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onUserNameClicked))
        tapgesture.numberOfTapsRequired = 1
        cell?.labelUsername.addGestureRecognizer(tapgesture)

        
//        if dict["userId"] as? String == User.currentUser().userId{
//            cell?.labelUsername.textColor = sliderUnselectedColor
//            cell?.labelPoints.textColor = sliderUnselectedColor
//            pointsLabel.text = dict["points"] as? String
//        }
//        else
//    {
//        cell?.labelUsername.textColor = sliderUnselectedColor
//        cell?.labelPoints.textColor = sliderUnselectedColor
//        cell?.labellastRatedLocation.textColor = sliderUnselectedColor
//    }
        return cell!
    }
    
    func onUserNameClicked (gesture : UIGestureRecognizer)  {
        let dict : NSDictionary = rankDetailsArray[gesture.view!.tag] as! NSDictionary
        print(dict)
        let profilevc = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
        profilevc.userId = dict["userId"] as! String
        profilevc.followStatus =  dict["FollowStatus"] as! String
        profilevc.userName = dict["username"] as! String
        profilevc.isFromPushNotification = false
        self.navigationController?.pushViewController(profilevc, animated: true)
    }
   }
