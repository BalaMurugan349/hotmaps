//
//  RatingHistoryViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 27/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class RatingHistoryViewController: UIViewController, LocationCreatedDelegate {

    var ratedLocationsArray: NSMutableArray = NSMutableArray()
    
    var indexSet:NSMutableIndexSet!
    
    @IBOutlet var tableViewRatedHistory: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       self.setNavBarImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        indexSet = NSMutableIndexSet()
        self.setNavigationBarItem()
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        ratedLocationsArray = NSMutableArray()
        if User.currentUser().userId != nil{
        getRatingHistory()
        }
    }
    

    func getRatingHistory(){
        
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        //http://52.88.60.242/HotMaps/Api.php?type=userRateLocation&userid=3
        
        
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("userRateLocation", key: "type")
        params.addValue(User.currentUser().userId, key: "userid")
        
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            
            SVProgressHUD.dismiss()
            
            if error == nil && result!["Result"] as! String == "Success"
            {
                self.ratedLocationsArray.addObjectsFromArray((result as! NSDictionary).objectForKey("Details") as! [AnyObject])
                
            }
            else if error == nil && result!["Result"] as! String == "Failed"
            {
            }
            else{
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
            
            for (idx,item) in self.ratedLocationsArray.enumerate(){
            let location = item as! NSDictionary
           
                if location["location_name"] as? String == nil{
                    self.indexSet.addIndex(idx)
                }
            }
            self.ratedLocationsArray.removeObjectsAtIndexes(self.indexSet)
            
            let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            let sortedArray = self.ratedLocationsArray.sortedArrayUsingDescriptors(NSArray(object: sortDescriptor) as! [NSSortDescriptor])
            
            let arr = NSMutableArray(array: sortedArray)

            let myArr = self.arrayFilteredForUniqueValuesOfKeyPath("location_name",arr: arr)
            
            self.ratedLocationsArray = NSMutableArray(array: myArr)
            self.tableViewRatedHistory.reloadData()
            
            
        }
    }

    func arrayFilteredForUniqueValuesOfKeyPath(keyPath: String, arr: NSMutableArray) -> [AnyObject] {
        let valueSeen: NSMutableSet = NSMutableSet()
        return arr.filteredArrayUsingPredicate(NSPredicate { (evaluatedObject, bindings) -> Bool in
            let value: AnyObject = evaluatedObject!.valueForKeyPath(keyPath)!
            if !valueSeen.containsObject(value) {
                valueSeen.addObject(value)
                return true
            }
            else {
                return false
            }
            })
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratedLocationsArray.count == 0 ? 1 : ratedLocationsArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RatingHistoryCell", forIndexPath: indexPath) as! RatingHistoryTableViewCell
        if ratedLocationsArray.count > 0{
            
        let location = ratedLocationsArray[indexPath.row] as! NSDictionary
            let likeStatus = location["likecount"] as? String
            let loc = location["location_name"] as? String == nil ? "" : location["location_name"] as? String
        if loc != ""{
            let strArray = loc!.componentsSeparatedByString("_") as NSArray
            cell.locationNameLabel.text = strArray[0] as? String
            }
        else{
            cell.locationNameLabel.text = ""
            }
//        cell.thumbsUpStatusImageView.image = likeStatus?.integerValue >= 50 ? UIImage(named: "thumbup") :  UIImage(named: "thumbdown")
        cell.labelLikeCount.text = likeStatus
          cell.textLabel?.text = nil
        }
        else {
            cell.textLabel?.text = "No Rating History Available"
             cell.textLabel?.textColor = sliderUnselectedColor
            cell.textLabel?.textAlignment = NSTextAlignment.Center
            cell.labelLikeCount.text = ""
            cell.locationNameLabel.text = nil
        }
        return cell
    }
    
    //MARK:- UITableView Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let location = ratedLocationsArray[indexPath.row] as! NSDictionary
        let locId = location["location_id"] as? String
        
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("LocationDetails", key: "type")
        params.addValue(locId!, key: "loaction_id")
        
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            
            SVProgressHUD.dismiss()
            
            if error == nil && result!["Result"] as! String == "Success"
            {
                let dict = (result as! NSDictionary).objectForKey("Details") as! NSDictionary
                
                let location = LocationDetails(locationDict: dict)
                let locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
                locationDetailsViewController.location = location
                locationDetailsViewController.delegate = self
                self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
            }
            else if error == nil && result!["Result"] as! String == "Failed"
            {
                SVProgressHUD.showErrorWithStatus("Invalid Location")
            }
            else{
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }

        }
       
    }
  //MARK:- LocationCreatedDelegate
    func reloadData() {
        ratedLocationsArray.removeAllObjects()
        self.tableViewRatedHistory.reloadData()
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
