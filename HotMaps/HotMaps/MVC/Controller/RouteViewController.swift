//
//  RouteViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 10/31/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import MapKit

class RouteViewController: UIViewController, MKMapViewDelegate, AnnotationViewDelegate {

    @IBOutlet var routeMap: MKMapView!
    var location: LocationDetails!
    var destination: MKMapItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        routeMap.showsUserLocation = true
        routeMap.delegate = self
        self.getDirections()
        addBackButton()
       self.setNavBarImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addBackButton()
    {
        let backBtn: UIButton = UIButton(type: UIButtonType.Custom)
    backBtn.frame = CGRectMake(0, 0, 25, 25) ;
    backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
    backBtn.addTarget(self, action:#selector(RouteViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
    
    let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
    self.navigationItem.leftBarButtonItem = backButton
    }
    func backAction()
    {
        SVProgressHUD.dismiss()
        self.navigationController?.popViewControllerAnimated(true)
    }
    func getDirections() {
           SVProgressHUD.show()
        let annotation: Annotation = Annotation(callOutMessage: location)
        self.routeMap.addAnnotation(annotation)
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem.mapItemForCurrentLocation()
        request.destination = destination
        request.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: request)
        
        directions.calculateDirectionsWithCompletionHandler { (response, error) -> Void in
            SVProgressHUD.dismiss()
            if error != nil {
                let alert = UIAlertView(title: "Error", message: "Could not map selected Location", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            } else {
                self.showRoute(response!)
            }

        }
       }
    func showRoute(response: MKDirectionsResponse) {
        
        for route in response.routes {
            
            routeMap.addOverlay(route.polyline,
                level: MKOverlayLevel.AboveRoads)
            
           /* for step in route.steps {
                println(step.instructions)
            }*/
        }
      //  let userLocation = routeMap.userLocation
     //   var dist: NSString = location.distance.componentsSeparatedByString(" ")[0] as NSString
     //   var span: CLLocationDistance = 1609.344 * (dist.doubleValue + 20)
        if  NSUserDefaults.standardUserDefaults().objectForKey("userLocation") != nil {
          let  userLocation  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as! NSDictionary
            let lat = Double(userLocation["lat"]! as! NSNumber)
            let long = Double(userLocation["long"]! as! NSNumber)
            let loc = CLLocation(latitude: lat , longitude:  long)
            let locSelected = CLLocation(latitude: location.latitude , longitude:  location.longitude)
            
            let arrLoc = NSMutableArray()
            arrLoc.addObject(loc)
            arrLoc.addObject(locSelected)
            centerMap(arrLoc)
            
          /*  let newDistance = loc.distanceFromLocation(locSelected)
            
            let region = MKCoordinateRegionMakeWithDistance(loc.coordinate, newDistance, 2 * newDistance)
            let adjustRegion = self.routeMap.regionThatFits(region)
            self.routeMap.setRegion(adjustRegion, animated:true)
            */
            
            
          /*  let region =
            MKCoordinateRegionMakeWithDistance(
                loc.coordinate, span, span)
            routeMap.setRegion(region, animated: true)*/

        }
        
       
           }
    func centerMap(arrLoc: NSMutableArray){
        var region: MKCoordinateRegion! = MKCoordinateRegion()
        
        var maxLat: CLLocationDegrees = -90
        var maxLon: CLLocationDegrees = -180
        var minLat: CLLocationDegrees = 90
        var minLon: CLLocationDegrees = 180
        for idx in (0..<arrLoc.count){
            let currentLocation: CLLocation = arrLoc.objectAtIndex(idx) as! CLLocation
            if(currentLocation.coordinate.latitude > maxLat)
            {maxLat = currentLocation.coordinate.latitude;}
            if(currentLocation.coordinate.latitude < minLat)
            {minLat = currentLocation.coordinate.latitude;}
            if(currentLocation.coordinate.longitude > maxLon)
            {maxLon = currentLocation.coordinate.longitude;}
            if(currentLocation.coordinate.longitude < minLon)
            {minLon = currentLocation.coordinate.longitude;}

        }
        region.center.latitude     = (maxLat + minLat) / 2;
        region.center.longitude    = (maxLon + minLon) / 2;
        region.span.latitudeDelta  = maxLat - minLat;
        region.span.longitudeDelta = maxLon - minLon;
        
        routeMap.setRegion(region, animated: true)
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay
        overlay: MKOverlay) -> MKOverlayRenderer {
            let renderer = MKPolylineRenderer(overlay: overlay)
            
            renderer.strokeColor = UIColor.blueColor()
            renderer.lineWidth = 5.0
            return renderer
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier("AnnotationView")
        if annotation.isKindOfClass(Annotation){
            
            if pinView == nil{
                let arrMessageView = (NSBundle.mainBundle().loadNibNamed("AnnotationMessageView", owner: self, options: nil)! as NSArray)
                let msgView = arrMessageView[0] as! AnnotationMessageView
                (msgView as AnnotationMessageView).delegate = self
                (msgView as AnnotationMessageView).btn.userInteractionEnabled = true
                let annot = annotation as! Annotation
                
                msgView.setMessage(annot.callOutMessage)
                
                let imageViewPin = UIImageView(frame: CGRectMake(0, 0, 116 * self.view.frame.width/320.0, 30 * self.view.frame.width/320.0))
                imageViewPin.contentMode = UIViewContentMode.ScaleAspectFit
                imageViewPin.image = UIImage(named: "mapannotation")
                pinView = DXAnnotationView(annotation: annotation, reuseIdentifier: "MapPinIdentifier", pinView: imageViewPin, calloutView: msgView, settings: DXAnnotationSettings.defaultSettings())
                (pinView as! DXAnnotationView).showCalloutView()
                pinView!.layer.anchorPoint = CGPointMake (0.5, 1.0)
            }
            else{
                pinView!.annotation = annotation
            }
            
        }
        
        return pinView
    }
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        if view.isKindOfClass(DXAnnotationView){
            view.layer.zPosition = -1
        }
        
        
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        if view.isKindOfClass(DXAnnotationView){
            (view as! DXAnnotationView).showCalloutView()
            view.layer.zPosition = 0
        }
    }
    
    //MARK:- AnnotationViewDelegate
    func annotationClicked(sender: UIButton) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
