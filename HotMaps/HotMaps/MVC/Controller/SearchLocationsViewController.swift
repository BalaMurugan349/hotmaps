//
//  SearchLocationsViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 02/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import MapKit

class SearchLocationsViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, MKMapViewDelegate, LocationCreatedDelegate {

    var searchResultPlaces: NSArray!
    var searchQuery: SPGooglePlacesAutocompleteQuery!
    var selectedPlaceAnnotation: MKPointAnnotation!
    var shouldBeginEditing: Bool!
    var searchActive : Bool = false
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavBarImage()
        // Do any additional setup after loading the view.
        searchQuery = SPGooglePlacesAutocompleteQuery(apiKey:"AIzaSyDfzPH8vWSZNKhtnAjRJCz04Jxv2Mac4gw")
        shouldBeginEditing = true
        
        /* Setup delegates */
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        searchBar.placeholder = "Search for Location"
        if #available(iOS 9.0, *) {
            UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).font =  UIFont.init(name: "Futura", size: 14)
        } else {
            checkSubview(view)
        }
       
    }
    func checkSubview(view:UIView){
        for subView in view.subviews {
            if subView is UITextField {
                let textField = subView as! UITextField
                textField.font = UIFont.init(name: "Futura", size: 14)
            } else {
                checkSubview(subView)
            }
        }
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- LocationCreatedDelegate
    func locationCreated() {
        searchBar.text = ""
        searchResultPlaces = nil
        tableView.reloadData()
       
        let mapViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
        let mapViewNavController = UINavigationController(rootViewController: mapViewController)
        self.slideMenuController()?.changeMainViewController(mapViewNavController, close: true)
    }
    //MARK:- UISearch Bar
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        self.view.endEditing(true)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchQuery.location = CLLocation(latitude: 8.5603060, longitude: 76.8811670).coordinate
        searchQuery.input = searchText
        SVProgressHUD.show()
        searchQuery.fetchPlaces { (places, error) -> Void in
            if error != nil{
               // print("Error \(error)")
            }
            else
            {
                self.searchResultPlaces = places
                SVProgressHUD.dismiss()
                self.tableView.reloadData()
            }
        }
        
    }
    //MARK:- UITableView Data Source
 
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultPlaces != nil ?searchResultPlaces.count : 0;
    }
    
    func placeAtIndexPath(indexPath: NSIndexPath) -> SPGooglePlacesAutocompletePlace{
        return searchResultPlaces[indexPath.row] as! SPGooglePlacesAutocompletePlace
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "SPGooglePlacesAutocompleteCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        
      //  cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
         cell!.textLabel!.text = placeAtIndexPath(indexPath).name as String
         cell!.textLabel!.textColor = textColor
         cell!.textLabel!.font = UIFont.init(name: "Futura-Medium", size: 14)
        return cell!;
    }
    
    //MARK:- UITableView Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let place: SPGooglePlacesAutocompletePlace = placeAtIndexPath(indexPath)
        SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        place.resolveToPlacemark { (placemark, addressString, error ) -> Void in
            
            if (error != nil){
                SVProgressHUD.dismiss()
                if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Error", message: "Could not map selected place", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                    
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            else if placemark != nil{
               if User.currentUser().userId != nil{
                
                let selectedLocation = CLLocation(latitude:  placemark.location!.coordinate.latitude , longitude: placemark.location!.coordinate.longitude)
                let userLocationDict  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as! NSDictionary
                let userLocation = CLLocation(latitude: userLocationDict.objectForKey("lat")as! CLLocationDegrees, longitude: userLocationDict.objectForKey("long")as! CLLocationDegrees)
                var dist = userLocation.distanceFromLocation(selectedLocation) * 0.000621371
                 dist = Double(round(100*dist)/100)
             
                let city = placemark.locality == nil ? "" : placemark.locality
                let countryName: String! = placemark.addressDictionary!["CountryCode"] == nil ? "" : placemark.addressDictionary!["CountryCode"] as! String
                
                let strArray = addressString.componentsSeparatedByString(",") as NSArray
                var locName: String?
                if strArray.count <= 2 {
                    locName = strArray.componentsJoinedByString(",")
                }
                else{
                    let arr = NSMutableArray()
                    for i in 0..<2{
                        arr.addObject(strArray[i])
                    }
                    locName = arr.componentsJoinedByString(",")
                    
                }
                locName = locName! + "_" + city! + "," + countryName
                let params = NSMutableData.postData() as! NSMutableData
                params.addValue("LocationDetails", key: "type")
                params.addValue(locName!, key: "locationname")
                ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
                    SVProgressHUD.dismiss()
                        if error == nil && result!["Result"] as! String == "Success"
                        {
                            let dict: NSDictionary? = (result as! NSDictionary).objectForKey("Details") as? NSDictionary
                            let location = LocationDetails(locationDict: dict!)
                            let locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
                            locationDetailsViewController.location = location
                            locationDetailsViewController.delegate = self
                            self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
                        }
                        else if error == nil && result!["Result"] as! String == "Failed"{
                
                if dist > 0.378{
                 UIAlertView(title: "Sorry", message: "You must be at that location to give your rating", delegate: nil, cancelButtonTitle: "OK").show()
                }

                else{

                let createLocationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CreateLocationViewController") as! CreateLocationViewController
                createLocationViewController.delegate = self
                createLocationViewController.isForRatingLocation = false
                createLocationViewController.locationId = ""
                    
                createLocationViewController.locationName = locName
                createLocationViewController.latitude = String(format: "%f", placemark.location!.coordinate.latitude)
                createLocationViewController.longitude = String(format: "%f", placemark.location!.coordinate.longitude)

                self.navigationController?.pushViewController(createLocationViewController, animated: true)
                }
                    }
                        else{
                            SVProgressHUD.showErrorWithStatus("Please check your internet connection")
                    }
                }
            }
               else{
                 SVProgressHUD.dismiss()
                if #available(iOS 8.0, *) {
                let alertController = UIAlertController(title: "Sorry", message: "You must login to use this feature", preferredStyle: .Alert)
                
//                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
//                    
//                }
//                alertController.addAction(cancelAction)
                
                let OKAction = UIAlertAction(title: "Login", style: .Default) { (action) in
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.didLogout(true)
                }
                alertController.addAction(OKAction)
                    let signup = UIAlertAction(title: "Sign-up", style: .Default) { (action) in
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.showSignupPage(true)
                    }
                    alertController.addAction(signup)
                self.presentViewController(alertController, animated: true, completion: nil)
                }}
            }
        }
      
     
    }
    //MARK: - LocationCreatedDelegate
    func reloadData() {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
