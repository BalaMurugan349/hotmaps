//
//  SendMessageViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 10/28/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import MessageUI

class SendMessageViewController: UIViewController, MFMessageComposeViewControllerDelegate {

    var arrayContacts: [String] = []
    var phonenumber: String!
    var messageComposeVC: MFMessageComposeViewController!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
         self.setNavBarImage()
        // Do any additional setup after loading the view.
        arrayContacts = [phonenumber]
        sendMessage()
        addBackButton()
    }
    func addBackButton(){
        let backBtn: UIButton = UIButton(type: UIButtonType.Custom)
        backBtn.frame = CGRectMake(0, 0, 25, 25) ;
        backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
        backBtn.addTarget(self, action:#selector(SendMessageViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backButton
    }
    func backAction()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendMessage(){
        let messageComposeVC = MFMessageComposeViewController()
            messageComposeVC.messageComposeDelegate = self
            messageComposeVC.recipients = arrayContacts
            messageComposeVC.body = "Hey friend - Try this app - Hotmaps - Your Social Radar!"
        
        // Make sure the device can send text messages
            if (MFMessageComposeViewController.canSendText()) {
                
                presentViewController(messageComposeVC, animated: true, completion: nil)
            } else {
                // Let the user know if his/her device isn't able to send text messages
               /* let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
                errorAlert.show()*/
            }
        
       
    }
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        
        switch (result) {
        case MessageComposeResult.Cancelled:
          break
        case MessageComposeResult.Failed:
            let errorAlert = UIAlertView(title: "Warning", message: "Failed to send SMS", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        case MessageComposeResult.Sent:
           break
        default:
            break;
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
