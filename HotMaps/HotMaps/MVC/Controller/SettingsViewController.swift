//
//  SettingsViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 29/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var segmmentPrivacy : UISegmentedControl!

   // var sort = ["👍 Percentage","Fullness","Energy","Age"]
    
    @IBOutlet var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavBarImage()
        emailTextField.text = User.currentUser().email as String
        usernameTextField.text = User.currentUser().username as String
        passwordTextField.text = User.currentUser().password as String
        
        let logoutButton: UIButton = UIButton(type: UIButtonType.Custom)
        logoutButton.frame = CGRectMake(0, 0, 25, 25) ;
        logoutButton.setImage(UIImage(named:"logout.png"), forState: UIControlState.Normal)
        logoutButton.addTarget(self, action:#selector(SettingsViewController.logoutAction), forControlEvents: UIControlEvents.TouchUpInside)
      
        let logOutBarButton: UIBarButtonItem = UIBarButtonItem(customView: logoutButton)
        self.navigationItem.rightBarButtonItem = logOutBarButton
        
        // Do any additional setup after loading the view.
        
    }

    func logoutAction()
    {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Warning", message: "Do you want to logout?", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Logout", style: .Default) { (action) in
            self.logoutWithJson()
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func logoutWithJson() {
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        
        ServiceManager.fetchDataFromService("apilinks/logout.php?", parameters: params) { (success, result, error) -> Void in
            
            if error == nil && result!["message"] as! String == "Logout Successfully"
            {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.didLogout(true)
                SVProgressHUD.dismiss()
            }
            else if error == nil &&  result!["message"] as! String != "Logout Successfully"
            {
                SVProgressHUD.showErrorWithStatus("Please try again")
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }


    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        viewPrivacyStatus()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewPrivacyStatus (){
        //52.88.60.242/HotMaps/apilinks/view_privacy.php?userid=70
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        
        ServiceManager.fetchDataFromService("apilinks/view_privacy.php?", parameters: params) { (success, result, error) -> Void in
            
            if error == nil
            {
                let userDict : NSDictionary = (result!["users"] as! NSArray).objectAtIndex(0) as! NSDictionary
                self.segmmentPrivacy.selectedSegmentIndex = userDict["privacy"] as! String == "public" ? 0 : 1
                SVProgressHUD.dismiss()
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }
        

    }
    @IBAction func onPrivacyStatusChanged (sender : UISegmentedControl){
        //52.88.60.242/HotMaps/apilinks/setprivacy.php?userid=70&privacy=private
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        params.addValue(sender.selectedSegmentIndex == 0 ? "public" : "private", key: "privacy")
        
        ServiceManager.fetchDataFromService("apilinks/setprivacy.php?", parameters: params) { (success, result, error) -> Void in
            
            if error == nil && result!["status"] as! String == "success"
            {
                SVProgressHUD.dismiss()
            }
            else if error == nil && result!["status"] as! String == "failed"
            {
                SVProgressHUD.showErrorWithStatus("Please try again")
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }

    }
    
  /*  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sort.count
    }
 /*   func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return sort[row]
    }*/
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        
        var myView = UIView(frame: CGRectMake(0, 0, pickerView.bounds.width - 30, 60))
        
        var myImageView = UIImageView(frame: CGRectMake(30, 20, 20, 20))
        
        var rowString: String
        switch row {
        case 0:
            rowString = "Percentage"
            myImageView.image = UIImage(named:"like")
        case 1:
            rowString = "Fullness"
            myImageView.image = nil
        case 2:
            rowString = "Energy"
            myImageView.image = nil
        case 3:
            rowString = "Age"
            myImageView.image = nil
        
        
        default:
            rowString = "Error: too many rows"
            myImageView.image = nil
        }
        let myLabel = UILabel(frame: CGRectMake(60, 0, pickerView.bounds.width - 90, 60 ))
        //myLabel.font = UIFont(name:some font, size: 18)
        myLabel.text = rowString
        
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        
        return myView
    }
      */  
   /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
*/
    

}
