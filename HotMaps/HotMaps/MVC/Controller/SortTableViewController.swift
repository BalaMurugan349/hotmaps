//
//  SortTableViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 13/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
 protocol sortLocationDelegate{
    func sortLocations(sortedArray: NSMutableArray, isSortingRating: Bool)
}

class SortTableViewController: UITableViewController {
    var delegate: sortLocationDelegate?
    
    var isFromTopLocationsVC: Bool!
    var sortArray: NSMutableArray = NSMutableArray()
    var locationArray: NSMutableArray!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBarImage()
        sortArray = isFromTopLocationsVC == true ? ["Top Today", "Top This Week", "Top This Month", "Top This Year", "Percentage"] :
            ["Percentage", "Distance", "Fullness", "Energy", "Age", "Last Rating"]
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        setUpUI()
        
    }
    func setUpUI(){
        let backBtn: UIButton = UIButton(type:UIButtonType.Custom)
        backBtn.frame = CGRectMake(0, 0, 25, 25) ;
        backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
        backBtn.addTarget(self, action:#selector(SortTableViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backButton
        
        let sortBtn: UIButton = UIButton(type:UIButtonType.Custom)
        sortBtn.frame = CGRectMake(0, 0, 45, 25) ;
        sortBtn.titleLabel?.font = UIFont.init(name: "Futura-Medium", size: 13)
        sortBtn.setTitle("Sort by", forState: .Normal)
        sortBtn.titleLabel?.textAlignment = NSTextAlignment.Right
        let sortButton: UIBarButtonItem = UIBarButtonItem(customView: sortBtn)
        self.navigationItem.rightBarButtonItem = sortButton

    }
    func backAction(){
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sortArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : SortTableViewCell? = tableView.dequeueReusableCellWithIdentifier("SortTableViewCell") as? SortTableViewCell
        
        if cell == nil {
            cell = SortTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "SortTableViewCell")
        }
        
        cell?.likeImageView.image = indexPath.row == 0 ? isFromTopLocationsVC == false ?  UIImage(named: "thumbup") : nil : nil
        cell?.likeImageView.image = indexPath.row == 4 ? isFromTopLocationsVC == true ?  UIImage(named: "thumbup") : nil : nil
        cell?.sortLabel.text = sortArray[indexPath.row] as? String
        
        return cell!
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let sortCell: SortTableViewCell = tableView.cellForRowAtIndexPath(indexPath) as! SortTableViewCell
        sortCell.tickImageView.image = UIImage(named: "tickBlack")
        
        let isSortingRating: Bool = false
        var percentDescriptor : NSSortDescriptor = NSSortDescriptor()
      //  var predicate: NSPredicate!
        var sortDescriptors : NSArray!
        var sortedArray: [AnyObject]!
        
        if isFromTopLocationsVC == true{
            let descriptor = NSSortDescriptor(key: "likePercent.integerValue" as String, ascending: false)
            sortDescriptors = NSArray(object: descriptor)
            let arr = locationArray.sortedArrayUsingDescriptors(sortDescriptors as! [NSSortDescriptor])
            locationArray = NSMutableArray(array: arr)
            sortedArray = NSMutableArray() as [AnyObject]
            locationArray.enumerateObjectsUsingBlock({ object, index, stop in
                let loc = object as! LocationDetails
                if indexPath.row == 0{
                    if loc.likePercent.integerValue >= 50 && loc.isRatedToday == true{
                       sortedArray.append(loc)
                    }
                }
               else if indexPath.row == 1{
                    if loc.likePercent.integerValue >= 50 && loc.isRatedThisWeek == true{
                        sortedArray.append(loc)
                    }
                }
                if indexPath.row == 2{
                    if loc.likePercent.integerValue >= 50 && loc.isRatedThisMonth == true{
                        sortedArray.append(loc)
                    }
                }
                if indexPath.row == 3{
                    if loc.likePercent.integerValue >= 50 && loc.isRatedThisYear == true{
                        sortedArray.append(loc)
                    }
                }
                if indexPath.row == 4{
                    if loc.likePercent.integerValue >= 50 {
                        sortedArray.append(loc)
                    }
                }
                
            })
           /* switch(indexPath.row){
               
            case 0:
                predicate = NSPredicate(format: "durationInSec.integerValue <= %d AND likePercent.integerValue >= %d", argumentArray: [1, 50])
                sortedArray = locationArray.filteredArrayUsingPredicate(predicate)
                
            case 1:
                predicate = NSPredicate(format: "durationInSec.integerValue <= %d AND likePercent.integerValue >= %d", argumentArray: [7, 50])
                sortedArray = locationArray.filteredArrayUsingPredicate(predicate)
            case 2:
                predicate = NSPredicate(format: "durationInSec.integerValue <= %d AND likePercent.integerValue >= %d", argumentArray: [31, 50])
                sortedArray = locationArray.filteredArrayUsingPredicate(predicate)
            case 3:
                predicate = NSPredicate(format: "durationInSec.integerValue <= %d AND likePercent.integerValue >= %d", argumentArray: [366, 50])
                sortedArray = locationArray.filteredArrayUsingPredicate(predicate)
            case 4:
                predicate = NSPredicate(format: "likePercent.integerValue >= %d", argumentArray: [50])
                sortedArray = locationArray.filteredArrayUsingPredicate(predicate)
                isSortingRating = true
            default:
                break
                

            }*/
        }
        else{
        switch(indexPath.row){
        case 0:
            percentDescriptor = NSSortDescriptor(key: "likePercent.integerValue" as String, ascending: false)
            sortDescriptors = NSArray(object: percentDescriptor)
            sortedArray = locationArray.sortedArrayUsingDescriptors(sortDescriptors as! [NSSortDescriptor])
        case 1:
            percentDescriptor = NSSortDescriptor(key: "distance.floatValue" as String, ascending: true)
            sortDescriptors = NSArray(object: percentDescriptor)
            sortedArray = locationArray.sortedArrayUsingDescriptors(sortDescriptors as! [NSSortDescriptor])
        case 2:
            percentDescriptor = NSSortDescriptor(key: "fullness.integerValue" as String, ascending: true)
            sortDescriptors = NSArray(object: percentDescriptor)
            sortedArray = locationArray.sortedArrayUsingDescriptors(sortDescriptors as! [NSSortDescriptor])
        case 3:
            percentDescriptor = NSSortDescriptor(key: "energy.integerValue" as String, ascending: false)
            sortDescriptors = NSArray(object: percentDescriptor)
            sortedArray = locationArray.sortedArrayUsingDescriptors(sortDescriptors as! [NSSortDescriptor])
        case 4:
            percentDescriptor = NSSortDescriptor(key: "age.integerValue" as String, ascending: true)
            sortDescriptors = NSArray(object: percentDescriptor)
            sortedArray = locationArray.sortedArrayUsingDescriptors(sortDescriptors as! [NSSortDescriptor])
        case 5:
            percentDescriptor = NSSortDescriptor(key: "durationInSec.integerValue" as String, ascending: true)
            sortDescriptors = NSArray(object: percentDescriptor)
            sortedArray = locationArray.sortedArrayUsingDescriptors(sortDescriptors as! [NSSortDescriptor])
            
               default:
            break
        }
        }
        delegate?.sortLocations(NSMutableArray(array: sortedArray), isSortingRating: isSortingRating)
        
        self.navigationController?.popViewControllerAnimated(true)
      //  sortedArray = [myArray sortedArrayUsingDescriptors:sortDescriptors];

    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
