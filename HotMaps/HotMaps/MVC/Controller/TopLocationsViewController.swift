//
//  TopLocationsViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 29/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class TopLocationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, bmDropDownDelegate, sortLocationDelegate, LocationCreatedDelegate {
 
    var lastSelectedIndexPath: NSIndexPath!
    var lastSelectedIndexPath_Sort: NSIndexPath!
    var locationDistance: Float!
    var locationDetailsArray: NSMutableArray = NSMutableArray()
    var locationArray: NSMutableArray!
    var sortArray: NSMutableArray!
    var refreshControl:UIRefreshControl!
    @IBOutlet var tableViewObj : UITableView!
   
    var _activityIndicatorView : UIActivityIndicatorView?
    var indexCount : NSInteger!
    var isLoadMoreData : Bool?
    
    var isReloadData: Bool!
    var isDropDownSort: Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        isReloadData = true
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action:#selector(TopLocationsViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.tableViewObj.addSubview(refreshControl)
        
        locationArray = NSMutableArray()
        sortArray = NSMutableArray()
        self.setNavBarImage()
        // Do any additional setup after loading the view.
        setUpUI()
        
        locationDistance = 0
       
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        
        if isReloadData == true{
        indexCount = 0
        isLoadMoreData = true
        getTopLocations()
        }
       /* else{
            isReloadData = true
        }*/
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- SetUpUI
    func setUpUI(){
        let distButton: UIButton = UIButton(type: .Custom)
        distButton.frame = CGRectMake(0, 0, 20, 20) ;
        distButton.setImage(UIImage(named:"Location.png"), forState: UIControlState.Normal)
        distButton.addTarget(self, action: #selector(TopLocationsViewController.distanceAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let distanceButton: UIBarButtonItem = UIBarButtonItem(customView: distButton)
        
        let sotrBtn: UIButton = UIButton(type: .Custom)
        sotrBtn.frame = CGRectMake(0, 0, 20, 20) ;
        sotrBtn.setImage(UIImage(named:"sort.png"), forState: UIControlState.Normal)
        sotrBtn.addTarget(self, action: #selector(TopLocationsViewController.sortAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let sortButton: UIBarButtonItem = UIBarButtonItem(customView: sotrBtn)

        self.navigationItem.setRightBarButtonItems([distanceButton,sortButton], animated: true)

    }
    
    //MARK:- UIBar Button Actions
    //MARK:- distance Action
    func distanceAction()
    {
        isDropDownSort = false
        let idx = NSIndexPath(forItem: 4, inSection: 0)
        let dropDownView : BMDropDown = BMDropDown()
        dropDownView.lastSelectedIndexPath = lastSelectedIndexPath == nil ? idx : lastSelectedIndexPath
        dropDownView.arrayToLoad = ["2 Miles","10 Miles","25 Miles","50 Miles","All"]
        dropDownView.delegate = self
        dropDownView.showDropDown(dropDownFrame:  CGRectMake(screenSize.width - self.view.frame.size.width / 3.125 - 10, 60, self.view.frame.size.width / 3.125 + 10, 0), heightOfDropDown: 190.0)
    }
    //MARK:- sort Action
    func sortAction(){
        isDropDownSort = true
        let idx = NSIndexPath(forItem: 4, inSection: 0)
        let dropDownView : BMDropDown = BMDropDown()
        dropDownView.lastSelectedIndexPath = lastSelectedIndexPath_Sort == nil ? idx : lastSelectedIndexPath_Sort
        dropDownView.arrayToLoad = ["Top Today", "Top This Week", "Top This Month", "Top This Year", "Percentage"]
        dropDownView.delegate = self
        dropDownView.showDropDown(dropDownFrame:  CGRectMake(screenSize.width - 200, 60, 200, 0), heightOfDropDown: 190.0)
        
       /* isLoadMoreData = false
        var sortTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SortTableViewController") as! SortTableViewController
        sortTableViewController.locationArray = locationArray
        sortTableViewController.isFromTopLocationsVC = true
        sortTableViewController.delegate = self
        self.navigationController?.pushViewController(sortTableViewController, animated: true)
*/
    }
    
    //MARK:- PullDownToRefresh
    func refresh(sender:AnyObject)
    {
      //  locationDistance = 20000
        locationDetailsArray.removeAllObjects()
        locationArray.removeAllObjects()
        sortArray.removeAllObjects()
        indexCount = 0
        isLoadMoreData = true
        getTopLocations()
         let idx = NSIndexPath(forItem: 4, inSection: 0)
        lastSelectedIndexPath = idx
    }
    
    //MARK:- sortLocationDelegate
   func sortLocations(sortedArray: NSMutableArray, isSortingRating: Bool){
        isReloadData = false
       // locationArray.removeAllObjects()
        sortArray.removeAllObjects()
       // locationArray = sortedArray
        sortArray = sortedArray
    if isSortingRating{
        locationArray = sortedArray
        
        locationDistance = 5
        self.sortLocationWithDistance(locationArray)
        
    }
        self.tableViewObj.reloadData()
    }
   //MARK:- LocationCreatedDelegate
    func reloadData(){
        locationDistance = 0
        locationArray.removeAllObjects()
        sortArray.removeAllObjects()
        locationDetailsArray.removeAllObjects()
         self.tableViewObj.reloadData()
        indexCount = 0
        isLoadMoreData = true
        getTopLocations()
    }
    
    //MARK:- bmDropDownDelegate
    func selectedValueFromBMDropDowm(value : NSString, idx: NSIndexPath) {
        if isDropDownSort == false{
        isReloadData = false
        lastSelectedIndexPath = idx
        switch lastSelectedIndexPath.row{
        case 0:
            locationDistance = 2
        case 1:
            locationDistance = 10
        case 2:
            locationDistance = 25
        case 3:
            locationDistance = 50
        case 4:
            locationDistance = 0
               default:
        break
        }
     //   getTopLocations()
          self.sortLocationWithDistance(locationArray)
        }
        else{
            isReloadData = false
            lastSelectedIndexPath_Sort = idx
            
            let descriptor = NSSortDescriptor(key: "likePercent.integerValue" as String, ascending: false)
            let sortDescriptors = NSArray(object: descriptor)
            let arr = locationArray.sortedArrayUsingDescriptors(sortDescriptors as! [NSSortDescriptor])
            locationArray = NSMutableArray(array: arr)
           var sortedArray = NSMutableArray() as [AnyObject]
            locationArray.enumerateObjectsUsingBlock({ object, index, stop in
                let loc = object as! LocationDetails
                if self.lastSelectedIndexPath_Sort.row == 0{
                    if loc.likePercent.integerValue >= 50 && loc.isRatedToday == true{
                        sortedArray.append(loc)
                    }
                }
                else if self.lastSelectedIndexPath_Sort.row == 1{
                    if loc.likePercent.integerValue >= 50 && loc.isRatedThisWeek == true{
                        sortedArray.append(loc)
                    }
                }
                if self.lastSelectedIndexPath_Sort.row == 2{
                    if loc.likePercent.integerValue >= 50 && loc.isRatedThisMonth == true{
                        sortedArray.append(loc)
                    }
                }
                if self.lastSelectedIndexPath_Sort.row == 3{
                    if loc.likePercent.integerValue >= 50 && loc.isRatedThisYear == true{
                        sortedArray.append(loc)
                    }
                }
                if self.lastSelectedIndexPath_Sort.row == 4{
                    if loc.likePercent.integerValue >= 50 {
                        sortedArray.append(loc)
                    }
                }
                
            })
            sortLocations(NSMutableArray(array: sortedArray), isSortingRating: false)
        }
    }
    //MARK:- Get Top Locations
    
    func getTopLocations(){
        //http://52.88.60.242/HotMaps/Api.php?type=listLocation&devicetoken=12345&latitude=11.2505&longitude=75.7700&index=0
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        var userLocation : NSDictionary
        if  NSUserDefaults.standardUserDefaults().objectForKey("userLocation") != nil {
            userLocation  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as! NSDictionary
        }
        else{
            userLocation = ["long" : "76.8811670", "lat" : "8.5603060"]
        }
        
        var token: NSString?
        if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
            token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
        }
        token = token == nil ? "12345" : token
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("listLocation", key: "type")
        let userId = User.currentUser().userId == nil ? "" : User.currentUser().userId
        params.addValue(userId, key: "userId")
        params.addValue(userLocation["lat"]!, key: "latitude")
        params.addValue(userLocation["long"]!, key: "longitude")
        params.addValue(token!, key: "devicetoken")
        params.addValue("\(indexCount)", key: "index")
        
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            self._activityIndicatorView?.stopAnimating()
            self.refreshControl.endRefreshing()
            SVProgressHUD.dismiss()
            if error == nil && result!["Result"] as! String == "Success"
            {
                if(self.indexCount == 0) { self.locationDetailsArray.removeAllObjects() }
                
                self.locationDetailsArray.addObjectsFromArray((result as! NSDictionary).objectForKey("Details") as! [AnyObject])
                self.isLoadMoreData = self.locationDetailsArray.count % 10 == 0 ? true : false
                
                self.sortLocationBasedOnDistance(self.locationDetailsArray)
                
            }
            else if error == nil && result!["Result"] as! String == "Failed"
            {
                self.isLoadMoreData = false
                if self.indexCount == 0{
                    if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Error", message: "No Locations Created", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                    
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                }
                }
            }
            else{
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
            
        }
        
    }
    func sortLocationBasedOnDistance(locationDetailsArray: NSMutableArray){
          
       /* locationArray = NSMutableArray()
        
        self.locationDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
            var locationDict = self.locationDetailsArray[index] as! NSDictionary
            var location: LocationDetails = LocationDetails(locationDict: locationDict)
       
            if location.distance.floatValue <= self.locationDistance{
                self.locationArray.addObject(location)
            }
            
        })*/
        
        sortArray = NSMutableArray()
        locationArray = NSMutableArray()
        self.locationDetailsArray.enumerateObjectsUsingBlock({ object, index, stop in
            let locationDict = self.locationDetailsArray[index] as! NSDictionary
            let location: LocationDetails = LocationDetails(locationDict: locationDict)
             self.locationArray.addObject(location)
              if self.locationDistance != 0{
            if location.distance.floatValue <= self.locationDistance && (location.likePercent.integerValue > 50){
                self.sortArray.addObject(location)
            }
            }
              else{
                self.sortArray.addObject(location)
               // self.locationArray.addObject(location)
            }
        })
        let descriptor = NSSortDescriptor(key: "likePercent.integerValue" as String, ascending: false)
        let arr = sortArray.sortedArrayUsingDescriptors(NSArray(object: descriptor) as! [NSSortDescriptor])
        sortArray = NSMutableArray(array: arr)

        self.tableViewObj.reloadData()
    }
    
    func sortLocationWithDistance(locationDetailsArray: NSMutableArray){
        sortArray = NSMutableArray()
        self.locationArray.enumerateObjectsUsingBlock({ object, index, stop in
            let location: LocationDetails = object as! LocationDetails
            if self.locationDistance != 0{
            if (location.distance.floatValue <= self.locationDistance) && (location.likePercent.integerValue > 50){
                self.sortArray.addObject(location)
            }
            }
            else{
                self.sortArray.addObject(location)
            }
        })
        let descriptor = NSSortDescriptor(key: "likePercent.integerValue" as String, ascending: false)
        let arr = sortArray.sortedArrayUsingDescriptors(NSArray(object: descriptor) as! [NSSortDescriptor])
        sortArray = NSMutableArray(array: arr)
      
        
        self.tableViewObj.reloadData()
    }
    //MARK:- UITableView DataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("TopLocationsCell", forIndexPath: indexPath) as! TopLocationsTableViewCell
        if sortArray.count  > 0 && sortArray.count  > indexPath.row{
        let location = sortArray[indexPath.row] as! LocationDetails
        
        cell.trendImageView.image = location.trendUpValue.integerValue >= location.trendDownValue.integerValue ? UIImage(named: "up") : UIImage(named: "down")
        cell.locationLabel.text = location.locationName as String

            cell.infoLabel.text = location.likePercent as String
            cell.infoLabel.textColor = location.likePercent.integerValue >= 50 ? UIColor(red: 0, green: 147/255.0, blue: 0, alpha: 1.0) : UIColor.redColor()
            
            dispatch_async(dispatch_get_main_queue()){
              
            cell.distanceLabel.frame = CGRectMake( cell.infoLabel.frame.size.width + CGRectGetMinX(cell.infoLabel.frame) + 2, cell.infoLabel.frame.origin.y, cell.distanceLabel.frame.size.width, cell.infoLabel.frame.size.height)
            cell.distanceLabel.text = location.distance as String
            }
        }
        
        else if locationArray.count == 0{
            //cell.textLabel?.text = "No Top Locations Available"
            if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: "Error", message: "No Top Locations Available", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { action in
                
            }))
            self.presentViewController(alert, animated: true, completion: nil)
            }
        }

        return cell
    }
    
    //MARK:- UITableView Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! TopLocationsTableViewCell
        var idx = 0
        for(index,item) in locationArray.enumerate(){
            let loc = item as! LocationDetails
            if loc.locationName == cell.locationLabel.text{
                idx = index
                break
            }
            
        }
        let location = locationArray[idx] as! LocationDetails
        let locationDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LocationDetailsViewController") as! LocationDetailsViewController
        locationDetailsViewController.location = location
        locationDetailsViewController.delegate = self
        self.navigationController?.pushViewController(locationDetailsViewController, animated: true)
    }
    //MARK:- SETTING FOOTER LOADING VIEW
    func setLoadingFooter (){
        let frame : CGRect =  CGRectMake(0, 0, self.view.frame.size.width, 30)
        let aframe : CGRect = CGRectMake(self.view.center.x - 10, 20, 5, 5)
        let loadingView : UIView = UIView(frame: frame)
        _activityIndicatorView = UIActivityIndicatorView(frame: aframe)
        _activityIndicatorView?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        _activityIndicatorView?.startAnimating()
        loadingView.addSubview(_activityIndicatorView!)
        self.tableViewObj.tableFooterView = loadingView
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if (self.tableViewObj.contentOffset.y<0)/* scroll from top */
        {
            
        }else if (self.tableViewObj.contentOffset.y >= (self.tableViewObj.contentSize.height - self.tableViewObj.bounds.size.height) && locationDetailsArray.count > 0 && isLoadMoreData == true ){
            indexCount = indexCount + 1
            self.setLoadingFooter()
            self.getTopLocations()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
