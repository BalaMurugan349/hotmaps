//
//  UsersTableViewController.swift
//  HotMaps
//
//  Created by Bala on 04/04/2016.
//  Copyright © 2016 Srishti Innovative. All rights reserved.
//

import UIKit

class UsersTableViewController: UIViewController {
    
    var arrayUsers : NSMutableArray = NSMutableArray()
    var arraySearch : NSMutableArray = NSMutableArray()
    var isListFollowers : Bool!
    var followerId : String!
    var linkType : String!
    var isSearching : Bool = false
    @IBOutlet weak var tableViewUsers : UITableView!
    @IBOutlet weak var searchBarObj : UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBarImage()


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        searchBarObj.text = ""
        isListFollowers == true ? fetchFollowers() : fetchAllUsers()
        if isListFollowers == false {
            self.setNavigationBarItem()
        }else{
            let backBtn: UIButton = UIButton(type: UIButtonType.Custom)
            backBtn.frame = CGRectMake(0, 0, 25, 25) ;
            backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
            backBtn.addTarget(self, action:#selector(UsersTableViewController.onBackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
            
            let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
            self.navigationItem.leftBarButtonItem = backButton
        }

    }
    
    func  onBackButtonPressed()  {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func fetchFollowers()  {
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(followerId, key: "userid")
        
        ServiceManager.fetchDataFromService(linkType, parameters: params) { (success, result, error) -> Void in
            
            if error == nil && result!["status"] as! Bool == true
            {
                self.arrayUsers.removeAllObjects()
                self.linkType == "apilinks/FollowersList.php?" ? self.arrayUsers.addObjectsFromArray(result!["followers"] as! [AnyObject]) : self.arrayUsers.addObjectsFromArray(result!["following"] as! [AnyObject])
                self.tableViewUsers.reloadData()
                SVProgressHUD.dismiss()
            }
            else if error == nil && result!["status"] as! Bool == false
            {
                SVProgressHUD.showErrorWithStatus("No users found")
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }

    }

    func fetchAllUsers (){
        dispatch_async(dispatch_get_main_queue()){
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
        }
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue(User.currentUser().userId, key: "userid")
        
        ServiceManager.fetchDataFromService("apilinks/allusers.php?", parameters: params) { (success, result, error) -> Void in
            
            if error == nil && result!["status"] as! Bool == true
            {
                self.arrayUsers.removeAllObjects()
                self.arrayUsers.addObjectsFromArray(result!["users"] as! [AnyObject])
                self.tableViewUsers.reloadData()
                SVProgressHUD.dismiss()
            }
            else if error == nil && result!["status"] as! Bool == false
            {
                 SVProgressHUD.showErrorWithStatus("No users found")
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Please check your internet connection")
            }
        }

    }
    
    // MARK: - Table view data source

     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching == true ? arraySearch.count : arrayUsers.count
    }

     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        let dict : NSDictionary = isSearching == true ? self.arraySearch[indexPath.row] as! NSDictionary : self.arrayUsers[indexPath.row] as! NSDictionary
        let labelName : UILabel = cell?.viewWithTag(1) as! UILabel
        labelName.text = dict["username"] as? String
        let imageVw : BMImageView = cell?.viewWithTag(2) as! BMImageView
        let imageName : String = dict["profileimage"] as! String
        imageVw.sd_setImageWithURL(NSURL(string: _imagePathUser + "images/users/" + imageName), placeholderImage: UIImage(named: "UserPlaceholder"))
        return cell!
    }

     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        searchBarObj.resignFirstResponder()
        let dict : NSDictionary = isSearching == true ? self.arraySearch[indexPath.row] as! NSDictionary : self.arrayUsers[indexPath.row] as! NSDictionary
        isSearching = false
        let profilevc = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
        profilevc.userId = dict["id"] as? String
        profilevc.isFromPushNotification = false
        profilevc.followStatus = isListFollowers == true ? linkType == "apilinks/FollowingList.php?" ? "Yes" : dict["follower_status"] as? String : dict["status"] == nil ? "Yes" : dict["status"] as? String
        profilevc.userName = dict["username"] as? String
        self.navigationController?.pushViewController(profilevc, animated: true)
    }

}

extension UsersTableViewController : UISearchBarDelegate{

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        isSearching = true
        let predicate : NSPredicate = NSPredicate(format: "SELF.username contains[cd] %@", searchText)
        arraySearch.removeAllObjects()
        arraySearch.addObjectsFromArray(arrayUsers.filteredArrayUsingPredicate(predicate))
        self.tableViewUsers.reloadData()
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBarObj.resignFirstResponder()
        isSearching = false
        searchBarObj.text = ""
        self.tableViewUsers.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBarObj.resignFirstResponder()
    }

    
    
}
