//
//  WelcomePopUpViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 12/16/15.
//  Copyright © 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class WelcomePopUpViewController: UIViewController {
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBarImage()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        addInfoView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addInfoView(){
        let welcomeView: UIView = UIView(frame: CGRectMake(0,0,screenSize.width - 40,355))
        welcomeView.backgroundColor = UIColor.blackColor()
        
       
        
        
        let imagePinch : UIImageView =  UIImageView(frame: CGRectMake(12,20,67,46))
        imagePinch.image = UIImage(named: "pinch")
        imagePinch.contentMode = UIViewContentMode.ScaleAspectFill
        welcomeView.addSubview(imagePinch)
        let labelPinch = UILabel(frame: CGRectMake(75,0 + 30,200, 21))
        labelPinch.center.y = imagePinch.center.y
        labelPinch.textAlignment = NSTextAlignment.Left
        labelPinch.font = UIFont.init(name: "Futura-CondensedMedium", size: 17)
        labelPinch.textColor = UIColor.whiteColor()
        labelPinch.text = "Zoom in to explore what's popular"
        welcomeView.addSubview(labelPinch)
        
        
        let imageFilter : UIImageView =  UIImageView(frame: CGRectMake(30,CGRectGetMaxY(labelPinch.frame) + 30,27,26))
        imageFilter.image = UIImage(named: "filter")
        imageFilter.contentMode = UIViewContentMode.ScaleAspectFit
        welcomeView.addSubview(imageFilter)
        let labelFilter = UILabel(frame: CGRectMake(75,0 + 30,200, 21))
        labelFilter.center.y = imageFilter.center.y
        labelFilter.textAlignment = NSTextAlignment.Left
        labelFilter.font = UIFont.init(name: "Futura-CondensedMedium", size: 17)
        labelFilter.textColor = UIColor.whiteColor()
        labelFilter.text = "Filter based on your preferences"
        welcomeView.addSubview(labelFilter)
        
        
        
        let imageTime : UIImageView =  UIImageView(frame: CGRectMake(30,CGRectGetMaxY(labelFilter.frame) + 30,30,29))
        imageTime.image = UIImage(named: "clock")
        imageTime.contentMode = UIViewContentMode.ScaleAspectFit
        welcomeView.addSubview(imageTime)
        let labelTime = UILabel(frame: CGRectMake(75,0 + 30,200, 21))
        labelTime.center.y = imageTime.center.y
        labelTime.textAlignment = NSTextAlignment.Left
        labelTime.font = UIFont.init(name: "Futura-CondensedMedium", size: 17)
        labelTime.textColor = UIColor.whiteColor()
        labelTime.text = "Time travel"
        welcomeView.addSubview(labelTime)
        
        
        let imageSort : UIImageView =  UIImageView(frame: CGRectMake(25,CGRectGetMaxY(labelTime.frame) + 30,43,23))
        imageSort.image = UIImage(named: "sort")
        imageSort.contentMode = UIViewContentMode.ScaleAspectFit
        welcomeView.addSubview(imageSort)
        let labelSort = UILabel(frame: CGRectMake(75,0 + 30,200, 21))
        labelSort.center.y = imageSort.center.y
        labelSort.textAlignment = NSTextAlignment.Left
        labelSort.font = UIFont.init(name: "Futura-CondensedMedium", size: 17)
        labelSort.textColor = UIColor.whiteColor()
        labelSort.text = "Sort Results"
        welcomeView.addSubview(labelSort)
        
        let labelEarn = UILabel(frame: CGRectMake(0,CGRectGetMaxY(labelSort.frame) + 40,200, 30))
        labelEarn.center.x = welcomeView.center.x
        labelEarn.textAlignment = NSTextAlignment.Center
        labelEarn.font = UIFont.init(name: "Futura-CondensedMedium", size: 17)
        labelEarn.textColor = UIColor.whiteColor()
        labelEarn.text = "Earn points by rating Locations"
        welcomeView.addSubview(labelEarn)
        
        let labelInfo = UILabel(frame: CGRectMake(0,CGRectGetMaxY(labelEarn.frame) + 30,CGRectGetWidth(welcomeView.frame), 21))
        labelInfo.center.x = welcomeView.center.x
        labelInfo.textAlignment = NSTextAlignment.Center
        labelInfo.font = UIFont.init(name: "Futura-CondensedMedium", size: 17)
        labelInfo.textColor = UIColor.whiteColor()
        welcomeView.addSubview(labelInfo)
        
        let strInfo = NSMutableAttributedString(string: "more points ")
        let likeAttachment = NSTextAttachment()
        likeAttachment.image = UIImage(named: "arrow")
        let attachmentString = NSAttributedString(attachment: likeAttachment)
        strInfo.appendAttributedString(attachmentString)
        
        let attrString = NSAttributedString(string: " more ")
        strInfo.appendAttributedString(attrString)
        
        let attrStringSway = NSMutableAttributedString(string:"Sway ")
        attrStringSway.addAttribute(NSForegroundColorAttributeName, value: UIColor.greenColor(), range: NSRange(location: 0, length: attrStringSway.length ))
        strInfo.appendAttributedString(attrStringSway)
        
        strInfo.appendAttributedString(attachmentString)
        
        let attrString1 = NSAttributedString(string: " more influence")
        strInfo.appendAttributedString(attrString1)
        labelInfo.attributedText = strInfo
        
        
        self.view.addSubview(welcomeView)
        welcomeView.center = self.view.center
       

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
