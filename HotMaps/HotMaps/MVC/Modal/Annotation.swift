//
//  Annotation.swift
//  HotMaps
//
//  Created by Srishti Innovative on 06/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject, MKAnnotation {
   
    var coordinate: CLLocationCoordinate2D
    var callOutMessage: LocationDetails
    
   init(callOutMessage: LocationDetails){
        self.coordinate = CLLocationCoordinate2D(latitude: callOutMessage.latitude, longitude: callOutMessage.longitude)
        self.callOutMessage = callOutMessage
    }
  
    
    
}
