//
//  Constants.swift
//  HotMaps
//
//  Created by Srishti Innovative on 22/10/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import Foundation

//UIColors
let textColor = UIColor.whiteColor()
let sliderColor = UIColor(red: 78/255.0, green: 94/255.0, blue: 187/255.0, alpha: 1.0)
let sliderUnselectedColor = UIColor.whiteColor()

//UIImages
let sliderUnselectedThunb = UIImage(named: "sliderHandleUnselected")
let sliderThumb = UIImage(named: "sliderHandle")

//UIDevice Version
let Device = UIDevice.currentDevice()
let iosVersion = NSString(string: Device.systemVersion).doubleValue
let iOS8 = iosVersion >= 8
let iOS7 = iosVersion >= 7 && iosVersion < 8

//Encryption Key
let encryptionKey = "ax+l8in1veJ7M04="