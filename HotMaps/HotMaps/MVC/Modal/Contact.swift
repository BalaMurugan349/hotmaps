//
//  Contact.swift
//  HotMaps
//
//  Created by Srishti Innovative on 10/28/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class Contact: NSObject {
    var firstName : String
    var lastName : String
    var birthday: NSDate?
    var thumbnailImage: NSData?
    var originalImage: NSData?
    var phone: String
    
    // these two contain emails and phones in <label> = <value> format
    var emailsArray: Array<Dictionary<String, String>>?
    var phonesArray: Array<Dictionary<String, String>>?
    
    override var description: String { get {
        return "\(firstName) \(lastName) \nBirthday: \(birthday) \nPhones: \(phonesArray) \nEmails: \(emailsArray)\n\n"}
    }
    
    init(firstName: String, lastName: String, birthday: NSDate?, phone: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.birthday = birthday
        self.phone = phone
    }
}
