//
//  ContactsImporter.swift
//  HotMaps
//
//  Created by Srishti Innovative on 10/28/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import Foundation
//  ContactsImporter.swift

import AddressBook
import UIKit

class ContactsImporter {
    
     class func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
        if let ab = abRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    class func importContacts(callback: (Array<Contact>) -> Void) {
        if(ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Denied || ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Restricted) {
            let alert = UIAlertView(title: "Address Book Access Denied", message: "Please grant us access to your Address Book in Settings -> Privacy -> Contacts", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            return
        }
        else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.NotDetermined) {
            var errorRef: Unmanaged<CFError>? = nil
            let addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
            ABAddressBookRequestAccessWithCompletion(addressBook, { (accessGranted: Bool, error: CFError!) -> Void in
                if(accessGranted) {
                    let contacts = self.copyContacts()
                    callback(contacts)
                }
            })
        }
        else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Authorized) {
            let contacts = self.copyContacts()
            callback(contacts)
        }
    }
    
     class func copyContacts() -> Array<Contact> {
        var errorRef: Unmanaged<CFError>? = nil
        let addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        let contactsList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as NSArray
        
        var importedContacts = Array<Contact>()
        
        for record:ABRecordRef in contactsList {
            let contactPerson: ABRecordRef = record
            let firstName: String = ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty).takeRetainedValue() as! String
            let lastName: String = ABRecordCopyValue(contactPerson, kABPersonLastNameProperty).takeRetainedValue() as! String
            
          //  println("-------------------------------")
          //  println("\(firstName) \(lastName)")
            
            let phonesRef: ABMultiValueRef = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty).takeRetainedValue() as ABMultiValueRef
            var phn = ""
            var phonesArray  = Array<Dictionary<String,String>>()
            for i:Int in 0 ..< ABMultiValueGetCount(phonesRef) {
                let label: String = ABMultiValueCopyLabelAtIndex(phonesRef, i).takeRetainedValue() as String
                let value: String = ABMultiValueCopyValueAtIndex(phonesRef, i).takeRetainedValue() as!  String
                
              //  println("Phone: \(label) = \(value)")
                
                let phone = [label: value]
                phonesArray.append(phone)
                phn = "\(value)"
            }
            
          //  println("All Phones: \(phonesArray)")
            
            let emailsRef: ABMultiValueRef = ABRecordCopyValue(contactPerson, kABPersonEmailProperty).takeRetainedValue() as ABMultiValueRef
            var emailsArray = Array<Dictionary<String, String>>()
            for i:Int in 0 ..< ABMultiValueGetCount(emailsRef) {
                let label: String = ABMultiValueCopyLabelAtIndex(emailsRef, i).takeRetainedValue() as String
                let value: String = ABMultiValueCopyValueAtIndex(emailsRef, i).takeRetainedValue() as! String
                
            //    println("Email: \(label) = \(value)")
                
                let email = [label: value]
                emailsArray.append(email)
            }
            
          //  println("All Emails: \(emailsArray)")
            
         /*   var birthday: NSDate? = ABRecordCopyValue(contactPerson, kABPersonBirthdayProperty).takeRetainedValue() == nil ? NSDate() : ABRecordCopyValue(contactPerson, kABPersonBirthdayProperty).takeRetainedValue() as? NSDate
            
            println ("Birthday: \(birthday)")
            */
            var thumbnail: NSData? = nil
            var original: NSData? = nil
            if ABPersonHasImageData(contactPerson) {
                thumbnail = ABPersonCopyImageDataWithFormat(contactPerson, kABPersonImageFormatThumbnail).takeRetainedValue() as NSData
                original = ABPersonCopyImageDataWithFormat(contactPerson, kABPersonImageFormatOriginalSize).takeRetainedValue() as NSData
            }
            
            let currentContact = Contact(firstName: firstName, lastName: lastName, birthday: NSDate(), phone:phn)
            currentContact.phonesArray = phonesArray
            currentContact.emailsArray = emailsArray
            currentContact.thumbnailImage = thumbnail
            currentContact.originalImage = original
            
            importedContacts.append(currentContact)
        }
        
        return importedContacts
    }
    
}