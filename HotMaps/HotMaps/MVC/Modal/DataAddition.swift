//
//  DataAddition.swift
//  LoginSwift
//
//  Created by BALA on 14/11/14.
//  Copyright (c) 2014 BALA. All rights reserved.
//

import Foundation

extension NSMutableData
{
    //MARK:- //Service manager helper class
    class func postData () -> AnyObject {
        let data = NSMutableData ()
        data.appendPostBoundary()
        return data
    }
    
    func appendPostBoundary ()
    {
        let data = "\r\n--0xKhTmLbOuNdArY\r\n".dataUsingEncoding(NSUTF8StringEncoding)
        self.appendData(data!)
    }
    
    func addValue (value : AnyObject , key : String)
    {
        self.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
        self.appendPostBoundary()
    }
    
    func addValue (value : AnyObject , key : String , filename : String)
    {
        self.appendData("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        self.appendData("Content-Type: application/octet-stream\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        self.appendData(value as! NSData)
        self.appendPostBoundary()

    }
}
