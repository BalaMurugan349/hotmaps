//
//  LocationDetails.swift
//  HotMaps
//
//  Created by Srishti Innovative on 30/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class LocationDetails: NSObject {
    
    var locationName: NSString!
    var broadLoc: NSString!
    var trendUpValue: NSString!
    var trendDownValue: NSString!
    var likePercent: NSString!
    var distance: NSString!
    var time: NSString!
    
    var age: NSString!
    var fullness: NSString!
    var energy: NSString!
    var price: NSString!
    var wait: NSString!
    var casual: NSString!
    var inclusive: NSString!
    var gender: NSString!
    var single: NSString!
    
    var latitude: Double!
    var longitude: Double!
    var id: NSString!
    var durationInSec: NSString!
    
    var rateCount: Int!
    var totalRates: NSString!
    var locationCreatorId: NSString!
    
    // current user rated or not
    var rateStatus: Bool!
    var thumbsUpStatus: Bool!
    
    var isRatedToday:Bool!
    var isRatedThisWeek: Bool!
    var isRatedThisMonth: Bool!
    var isRatedThisYear: Bool!
    
    var locationTextRating : String!
    var locationImageName : String!
    var locationFullName : String!
    
    override init() {
        super.init()
    }
    
    init(locationDict: NSDictionary){
        super.init()
        
        var likes: Float = 0
        var trendup = 0
        var trenddown = 0
        var fullnessFilter: Float = 0
        var energyFilter: Float = 0
        var ageFilter: Float = 0
        var casualFilter: Float = 0
        var waitFilter: Float = 0
        var priceFilter: Float = 0
        var inclusiveFilter: Float = 0
        var genderFilter: Float = 0
        var singleFilter: Float = 0
        
        isRatedToday = false
        isRatedThisWeek = false
        isRatedThisMonth = false
        isRatedThisYear = false
        
        rateStatus = false
        thumbsUpStatus = false
        
        self.locationTextRating = locationDict["description"] as! String
        self.locationImageName = locationDict["loc_image"] as! String

        let userLocationDict  = NSUserDefaults.standardUserDefaults().objectForKey("userLocation") as! NSDictionary
        let userLocation = CLLocation(latitude: userLocationDict.objectForKey("lat")as! CLLocationDegrees, longitude: userLocationDict.objectForKey("long")as! CLLocationDegrees)
        latitude = (locationDict["latitude"] as! NSString).doubleValue
        longitude = (locationDict["longitude"] as! NSString).doubleValue
        let location = CLLocation(latitude: latitude , longitude: longitude)
        
        let dist = userLocation.distanceFromLocation(location) * 0.000621371
//        if locationDict["id"] as! NSString == "12"{
//        }
        distance = String(format: "%.1f mi", dist)
        id = locationDict["id"] as! NSString
        locationFullName = locationDict["locationname"] as! String
        let loc = locationDict["locationname"] as! String
        let strArray = loc.componentsSeparatedByString("_") as NSArray
        locationName = strArray[0] as! NSString
        broadLoc = strArray[1] as! NSString
        
        locationCreatorId = locationDict["userid"] as! NSString
        let rates = locationDict["rates"] as! NSArray
        totalRates = String(rates.count)
        
        var token: NSString?
        if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
            token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
        }
        token = token == nil ? "12345" : token
        
        var likeRateCount: Float = 0
        var fullnessRateCount: Float = 0
        var energyRateCount: Float = 0
        var ageRateCount: Float = 0
        var casualRateCount: Float = 0
        var waitRateCount: Float = 0
        var priceRateCount: Float = 0
        var inclusiveRateCount: Float = 0
        var genderRateCount: Float = 0
        var singleRateCount: Float = 0
        
       
        if rates.count > 0{
            rates.enumerateObjectsUsingBlock ({ obj, idx, stop  in
                
                let dict = obj as! NSDictionary
                trendup = trendup + (dict["trendUpCount"] as! NSString).integerValue
                trenddown = trenddown + (dict["trendDownCount"] as! NSString).integerValue
                
                likeRateCount = (dict["likecount"] as! NSString) != "" ? likeRateCount + 1 : likeRateCount
                fullnessRateCount = (dict["fullness"] as! NSString) != "" ? fullnessRateCount + 1 : fullnessRateCount
                energyRateCount = (dict["energy"] as! NSString) != "" ? energyRateCount + 1 : energyRateCount
                ageRateCount = (dict["age"] as! NSString) != "" ? ageRateCount + 1 : ageRateCount
                casualRateCount = (dict["casualFilter"] as! NSString) != "" ? casualRateCount + 1 : casualRateCount
                waitRateCount = (dict["waitFilter"] as! NSString) != "" ? waitRateCount + 1 : waitRateCount
                priceRateCount = (dict["priceFilter"] as! NSString) != "" ? priceRateCount + 1 : priceRateCount
                inclusiveRateCount = (dict["inclusiveFilter"] as! NSString) != "" ? inclusiveRateCount + 1 : inclusiveRateCount
                genderRateCount = (dict["genderFilter"] as! NSString) != "" ? genderRateCount + 1 : genderRateCount
                singleRateCount = (dict["singleFilter"] as! NSString) != "" ? singleRateCount + 1 : singleRateCount
                
                likes = likes + (dict["likecount"] as! NSString).floatValue
                fullnessFilter = fullnessFilter + (dict["fullness"] as! NSString).floatValue
                energyFilter = energyFilter + (dict["energy"] as! NSString).floatValue
                ageFilter = ageFilter + (dict["age"] as! NSString).floatValue
                
                genderFilter = genderFilter + (dict["genderFilter"] as! NSString).floatValue
                casualFilter = casualFilter + (dict["casualFilter"] as! NSString).floatValue
                singleFilter = singleFilter + (dict["singleFilter"] as! NSString).floatValue
                waitFilter = waitFilter + (dict["waitFilter"] as! NSString).floatValue
                inclusiveFilter = inclusiveFilter + (dict["inclusiveFilter"] as! NSString).floatValue
                priceFilter = priceFilter + (dict["priceFilter"] as! NSString).floatValue
                if User.currentUser().userId != nil{
                if (dict["userid"] as! NSString) == User.currentUser().userId{
                    self.rateStatus = true
                    self.thumbsUpStatus = (dict["likecount"] as! NSString).integerValue <= 50 ? true : false
                }
                }
                
            })
        }
        
    
     //  self.likePercent = String(format: "%.0f", likes)
     //   self.likePercent = "0"
 
        self.trendUpValue = String(trendup)
        self.trendDownValue = String(trenddown)
        
        self.likePercent = likeRateCount > 0 ? String(format: "%.0f", Float(likes / likeRateCount)) : ""
        self.fullness = fullnessRateCount > 0 ? String(format: "%.0f", Float(fullnessFilter / fullnessRateCount)) : ""
        self.energy = energyRateCount > 0 ? String(format: "%.0f", Float(energyFilter / energyRateCount)) : ""
        self.age = ageRateCount > 0 ? String(format: "%.2f", Float(ageFilter / ageRateCount)) : ""
        
        self.gender = genderRateCount > 0 ? String(format: "%.0f", Float(genderFilter / genderRateCount)) : ""
        self.casual = casualRateCount > 0 ? String(format: "%.0f", Float(casualFilter / casualRateCount)) : ""
        self.single = singleRateCount > 0 ? String(format: "%.0f", Float(singleFilter / singleRateCount)) : ""
        self.wait = waitRateCount > 0 ? String(format: "%.0f", Float(waitFilter / waitRateCount)) : ""
        self.inclusive = inclusiveRateCount > 0 ? String(format: "%.0f", Float(inclusiveFilter / inclusiveRateCount)) : ""
        self.price = priceRateCount > 0 ? String(format: "%.0f", Float(priceFilter / priceRateCount)) : ""
        
        var indx  = 0
        var timeStr: NSString = ""
        for (index,item) in rates.enumerate(){
            let dict = item as! NSDictionary
            let timeString = dict["rate_date"] == nil ? (dict["date"] as! String) : (dict["rate_date"] as! String)
            
            if index == 0 {
                indx = 0
                timeStr = timeString
            }
            else{
                let formatter = NSDateFormatter()
                formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                let date1 = formatter.dateFromString(timeStr as String)
                let date2 = formatter.dateFromString(timeString)
                let result  = date1!.compare(date2!)
                if result == NSComparisonResult.OrderedAscending{
                    indx = index
                    timeStr = timeString
                }
                
            }
        }
        
        if(rates.count == 0) {return}
        let dict = rates.objectAtIndex(indx) as! NSDictionary
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        
        let dateString = formatter.stringFromDate(NSDate())
        let today = formatter.dateFromString(dateString)
        
        let str =  dict["rate_date"] == nil ? (dict["date"] as! String) : (dict["rate_date"] as! String)
        
        let dateFormat = "YYYY-MM-dd HH:mm:ss"
        let inputTimeZone = NSTimeZone(name: "UTC")
        //     var inputTimeZone = NSTimeZone(forSecondsFromGMT: 0)
        let inputDateFormatter = NSDateFormatter()
        inputDateFormatter.timeZone = inputTimeZone
        inputDateFormatter.dateFormat = dateFormat
        let serverDate = inputDateFormatter.dateFromString(str)
        let localDate = toLocalTime(serverDate!)
        
        // let str = (locationDict["time"] as! String)
        
        
        // var time = formatter.dateFromString(locationDict["time"] as! String)
        let str1 = NSDate.changeServerTimeZoneToLocalForDate(str)
        let date = formatter.dateFromString(str1)
        
        var duration = abs(Int(today!.timeIntervalSinceDate(date!)))
        if(duration < 60)
        {
            time = "Just now"
        }
        
       else if  duration < 3600{
            duration = Int (duration / (60))
            time = duration > 1 ? "\(duration) minutes ago" : "\(duration) minute ago"
        }
        else if duration < 86400{
            duration = Int (duration / (60*60))
            time = duration > 1 ? "\(duration) hours ago" : "\(duration) hour ago"
        }
        else if duration >= 86400{
            duration = Int (duration / (60*60*24))
            time = duration > 1 ? "\(duration) days ago" : "\(duration) day ago"
            if duration > 10{
                let dateFormatter1: NSDateFormatter = NSDateFormatter()
                dateFormatter1.locale = NSLocale.systemLocale()
                dateFormatter1.timeZone = NSTimeZone.systemTimeZone()
                dateFormatter1.dateFormat = "YYYY-MM-dd HH:mm:ss"
           //     time = dateFormatter1.stringFromDate(localDate)
                
            }
        }
        
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        
        // Replace the hour (time) of both dates with 00:00
        if #available(iOS 8.0, *) {
            let date1 = calendar.startOfDayForDate(localDate)
            let date2 = calendar.startOfDayForDate(today!)
        
        let flags = NSCalendarUnit.Day
        let components = calendar.components(flags, fromDate: date1, toDate: date2, options: NSCalendarOptions.MatchFirst)
        durationInSec = String(components.day)
        
        checkRateDuration(date1,todaysDate: date2)
        }
        
        
    }


func checkRateDuration(dateToCheck: NSDate, todaysDate: NSDate){

    let calendar: NSCalendar = NSCalendar.currentCalendar()
    let calendarUnit: NSCalendarUnit = [NSCalendarUnit.WeekOfMonth , NSCalendarUnit.Year , NSCalendarUnit.Month , NSCalendarUnit.Day]
    let otherDay: NSDateComponents = calendar.components(calendarUnit, fromDate: dateToCheck)
    let today: NSDateComponents = calendar.components(calendarUnit, fromDate: todaysDate)
   
  if    today.day == otherDay.day &&
        today.month == otherDay.month &&
        today.year == otherDay.year{
        self.isRatedToday = true
    }
    
    if today.year == otherDay.year && today.month == otherDay.month && today.weekOfMonth == otherDay.weekOfMonth{
        self.isRatedThisWeek = true
    }
    if  today.year == otherDay.year && today.month == otherDay.month {
            self.isRatedThisMonth = true
    }
    if today.year == otherDay.year{
            self.isRatedThisYear = true
    }
    
    
}

}
func toLocalTime(serverdate: NSDate) -> NSDate {
    let timezone: NSTimeZone = NSTimeZone.localTimeZone()
    let seconds: NSTimeInterval = NSTimeInterval(timezone.secondsFromGMTForDate(serverdate))
    return NSDate(timeInterval: seconds, sinceDate: serverdate)
}


