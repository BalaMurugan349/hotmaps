//
//  LoginTableViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 29/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


class LoginTableViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var activetextField: UITextField!
    @IBOutlet var arrTextFields: Array<UITextField>!
    var alertForgotPassword : UIAlertView!
    var loggeduserId : String = ""
    var detailsDict : NSMutableDictionary!

    var showCancelBarButton: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // if showCancelBarButton == true{
            let backBtn: UIButton = UIButton(type: UIButtonType.Custom)
            backBtn.frame = CGRectMake(0, 0, 25, 25) ;
            backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
            backBtn.addTarget(self, action:#selector(LoginTableViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
            
            let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
            self.navigationItem.leftBarButtonItem = backButton
     //   }

          }

    func backAction(){
        if showCancelBarButton == true {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.createMenuView()

        }else{
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.setNavBarImage()
        self.addTapGesture()
        for textField in arrTextFields
        {
            textField.layer.borderWidth = 1.0;
            textField.layer.borderColor = UIColor.whiteColor().CGColor;
            textField.clipsToBounds      = true;
            textField.attributedPlaceholder = NSAttributedString(string:textField.placeholder!,
                attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
            textField.setLeftPadding(10)
        }
        
            }
    
    @IBAction func actionLogin(sender: AnyObject) {
        //http://52.88.60.242/HotMaps/Api.php?type=userLogin&username=leemada&password=123
      actionLogin()
    }
    
    @IBAction func actionForgotPassword(sender: UIButton){
        
        if #available(iOS 8.0, *) {
            var inputTextField: UITextField?
            let passwordPrompt = UIAlertController(title: "Password Recovery", message: "Enter your Email:", preferredStyle: UIAlertControllerStyle.Alert)
            passwordPrompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
            passwordPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                if !self.isValidEmail(inputTextField!.text!){
                    SVProgressHUD.showErrorWithStatus("Please enter a valid Email")
                    inputTextField!.text = ""
                }
                else{
                    self.actionSendResetLink(inputTextField!.text!)
                    
                }
            }))
            passwordPrompt.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
                textField.placeholder = "Email"
                textField.keyboardType = UIKeyboardType.EmailAddress
                textField.font = UIFont.init(name: "TrebuchetMS", size: 14.0)
                inputTextField = textField
            })
            
            presentViewController(passwordPrompt, animated: true, completion: nil)
        }
        
    }
    func actionSendResetLink(email: NSString){
        let params = NSMutableData.postData() as! NSMutableData
        params.addValue("forgotpassword", key: "type")
        params.addValue(email, key: "email")
        
        ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
            if error == nil && result!["result"] as! String == "success"{
               SVProgressHUD.showSuccessWithStatus("Reset link sent. Please check your mail")
                
            }
            else if result != nil
            {
                SVProgressHUD.showErrorWithStatus(result!["error"] as! String)
            }
            else if error != nil
            {
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    func actionLogin(){
        if usernameTextField.text != "" && passwordTextField.text != ""
        {
            let params = NSMutableData.postData() as! NSMutableData
            params.addValue("userLogin", key: "type")
            params.addValue(usernameTextField.text!, key: "username")
            
            let encryptedPassword =  SCCryptor.encryptText(passwordTextField.text!, withIV: encryptionKey)
            params.addValue(encryptedPassword, key: "password")
             params.addValue("1", key: "version")
          //  params.addValue(passwordTextField.text!, key: "password")
            var token: NSString?
            if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
                token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
            }
            token = token == nil ? "12345" : token
            params.addValue(token!, key: "devicetoken")
            
            ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
                if error == nil && result!["Result"] as! String == "Success"{
                    let userDict = result!["Details"] as! Dictionary<String,String>
                     User.userWithDetails(userDict)
                     User.saveUserDetails(userDict)
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.didLogin()
                    
                }
                else if result != nil
                {
                    SVProgressHUD.showErrorWithStatus("Incorrect Username or Password")
                }
                else if error != nil
                {
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            }
            
        }
        else
        {
            UIAlertView(title: "Warning", message: "Please enter all fields", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    
    @IBAction func actionSignUp(sender: AnyObject) {
        let signUpViewContrtoller = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpViewController") as! SignUpTableViewController
        signUpViewContrtoller.showMapPage = false
        signUpViewContrtoller.showBackButton = true
        self.navigationController?.pushViewController(signUpViewContrtoller, animated: true)
    }
    //MARK:- UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if passwordTextField.isFirstResponder(){
            passwordTextField.resignFirstResponder()
            actionLogin()
        }
        else{
            usernameTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        };        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        activetextField = textField
    }
    // MARK: - Email Validation
    func isValidEmail(email: String) -> Bool
    {
        let emailRegex: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(email)
    }
   
    //MARK:- FACEBOOK LOGIN
    @IBAction func onLoginWithFacebookButtonPressed (sender : UIButton)
    {
        let login : FBSDKLoginManager = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.Browser
        login.logOut()
        login.logInWithReadPermissions(["public_profile", "email" , "user_friends"], fromViewController: self) { (result, error) -> Void in
            if(error == nil && result.isCancelled == false){
                SVProgressHUD.showWithStatus("Processing...")
                let request = FBSDKGraphRequest(graphPath:"me", parameters:["fields" : "id, name, email"])
                request.startWithCompletionHandler {
                    (connection, result, error) in
                    if error != nil {
                        // SVProgressHUD.dismiss()
                    }
                    else if let userData = result as? [String:AnyObject] {
                        print("\(userData)")
                        self.fbLogin(userData)
                    }
                }
            }else{
                SVProgressHUD.showErrorWithStatus("Login Failed")
            }
        }
    }

    func  fbLogin(data : NSDictionary)  {
        let params  = NSMutableData.postData() as! NSMutableData
        params.addValue(data["name"] as! String, key: "username")
        params.addValue(data["email"] as! String, key: "email")
        params.addValue(data["id"] as! String, key: "facebookid")
        let imgData : NSData? = NSData(contentsOfURL: NSURL(string: "https://graph.facebook.com/\(data["id"] as! String)/picture?type=large&return_ssl_resources=1")!)
        let imag = UIImage(data: imgData!)
        params.addValue(UIImageJPEGRepresentation(imag!, 0.7)!, key: "profilePic", filename: User.makeFileName())
        var token: NSString?
        if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
            token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
        }
        token = token == nil ? "12345" : token
        params.addValue(token!, key: "devicetkn")
        
        ServiceManager.fetchDataFromService("apilinks/Register.php?", parameters: params) { (success, result, error) in
            if success{
                if result!["status"] as! Bool == true{
                    SVProgressHUD.dismiss()
                    print(result)
                    self.detailsDict  = NSMutableDictionary(dictionary:result!["details"] as! NSDictionary)
                    self.loggeduserId = self.detailsDict["id"] as! String
                    if self.detailsDict["firsttimelogin"] as! String == "yes" {
                        let oldUserName : String = (self.detailsDict["username"] as! String).stringByReplacingOccurrencesOfString(" ", withString: "")
                        self.alertForgotPassword = UIAlertView(title: "", message: "By default your username has been set to \(oldUserName). If you want to change this, type a new username below.", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Submit")
                        self.alertForgotPassword.alertViewStyle = UIAlertViewStyle.PlainTextInput;
                        self.alertForgotPassword.delegate = self;
                        self.alertForgotPassword.tag = 1;
                        self.alertForgotPassword.textFieldAtIndex(0)?.delegate = self
                        self.alertForgotPassword.textFieldAtIndex(0)?.keyboardType = UIKeyboardType.Default
                        self.alertForgotPassword.show()
                    }else{
                        User.userWithDetails(self.detailsDict)
                        User.saveUserDetails(self.detailsDict)
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.didLogin()
                    }
                    
                }else{
                    SVProgressHUD.showErrorWithStatus("Please try again")
                }
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
}

extension LoginTableViewController : UIAlertViewDelegate{
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 1 && alertForgotPassword.textFieldAtIndex(0)!.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 0){
            SVProgressHUD.showWithStatus("processing...")
            let params : NSMutableData = NSMutableData.postData() as! NSMutableData
            params.addValue(alertForgotPassword.textFieldAtIndex(0)!.text!, key: "username")
            params.addValue(loggeduserId, key: "userid")
            ServiceManager.fetchDataFromService("apilinks/EditUsername.php?", parameters: params, withCompletion: { (success, result, error) in
                if success{
                    print(result)
                    if result!["status"] as! Bool == true{
                        SVProgressHUD.dismiss()
                        self.detailsDict.setValue(self.alertForgotPassword.textFieldAtIndex(0)!.text!, forKey: "username")
                        User.userWithDetails(self.detailsDict)
                        User.saveUserDetails(self.detailsDict)
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.didLogin()
                        
                    }else{
                        SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                    }
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            })
            
        }else{
            User.userWithDetails(self.detailsDict)
            User.saveUserDetails(self.detailsDict)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.didLogin()
        }
        
    }
}
