//
//  SignUpTableViewController.swift
//  HotMaps
//
//  Created by Srishti Innovative on 29/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SignUpTableViewController: UITableViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet var arrTextFields: Array<UITextField>!
    var showMapPage : Bool!
    var activetextField: UITextField!
    @IBOutlet var buttonGuest : UIButton!
    var showBackButton : Bool!
    var alertForgotPassword : UIAlertView!
    var loggeduserId : String = ""
    var detailsDict : NSMutableDictionary!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.addTapGesture()
        if showBackButton == true { addBackButton() }
        self.setNavBarImage()
        for textField in arrTextFields
        {
            textField.layer.borderWidth = 1.0;
            textField.layer.borderColor = UIColor.whiteColor().CGColor;
            textField.clipsToBounds      = true;
            textField.attributedPlaceholder = NSAttributedString(string:textField.placeholder!,
                attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
            textField.setLeftPadding(10)
        }
        
        buttonGuest.frame = CGRectMake(0, screenSize.height - 40, screenSize.width, 28)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.addSubview(buttonGuest)

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        buttonGuest.removeFromSuperview()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    func addBackButton(){
        let backBtn: UIButton = UIButton(type:UIButtonType.Custom)
        backBtn.frame = CGRectMake(0, 0, 25, 25) ;
        backBtn.setImage(UIImage(named:"back.png"), forState: UIControlState.Normal)
        backBtn.addTarget(self, action:#selector(SignUpTableViewController.backAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backButton
    }
    func backAction()
    {
        if(showMapPage == true){
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.createMenuView()
        }else{
            self.navigationController?.popViewControllerAnimated(true)

        }
    }

    @IBAction func actionSignUp(sender: AnyObject) {
        actionSignUp()
    }
 
    @IBAction func onSignInButtonPressed (sender : UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginTableViewController
        mainViewController.showCancelBarButton = false
        self.navigationController?.pushViewController(mainViewController, animated: true)
    }
    
    @IBAction func onGuestButtonPressed (sender : UIButton){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.createMenuView()

    }
    
    func actionSignUp(){
        
        if usernameTextField.text != "" && passwordTextField.text != "" && emailTextField.text != ""
        {
            if passwordTextField.text!.characters.count < 5{
                UIAlertView(title: "Warning", message: "Password length should not be less than 5", delegate: nil, cancelButtonTitle: "OK").show()
            }
                
            else  if !isValidEmail(emailTextField.text!)
            {
                UIAlertView(title: "Warning", message: "Please enter a valid email", delegate: nil, cancelButtonTitle: "OK").show()
            }
                /*  else if buttonMale.selected == false &&  buttonFemale.selected == false{
                UIAlertView(title: "Warning", message: "Please enter all fields", delegate: nil, cancelButtonTitle: "OK").show()
                }*/
            else{
                let params = NSMutableData.postData() as! NSMutableData
                params.addValue("userSignup", key: "type")
                params.addValue(usernameTextField.text!, key: "username")
                
                let encryptedPassword = SCCryptor.encryptText(passwordTextField.text!, withIV: encryptionKey)
                
                params.addValue(encryptedPassword, key: "password")
               //  params.addValue(passwordTextField.text!, key: "password")
                params.addValue(emailTextField.text!, key: "email")
                 params.addValue("1", key: "version")
                var token: NSString?
                if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
                    token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
                }
                token = token == nil ? "12345" : token
                params.addValue(token!, key: "devicetoken")

                SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Black)
                ServiceManager.fetchDataFromService("Api.php?", parameters: params) { (success, result, error) -> Void in
                    if error == nil && result!["Result"] as! String == "Success"
                    {
                        SVProgressHUD.dismiss()
                        self.detailsDict  = NSMutableDictionary(dictionary:result!["Details"] as! NSDictionary)
                        self.loggeduserId = self.detailsDict["id"] as! String

//                        let userDict = result!["Details"] as! Dictionary<String,String>
//                        User.userWithDetails(userDict)
//                        User.saveUserDetails(userDict)
//                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                        appDelegate.didLogin()
                        self.alertForgotPassword = UIAlertView(title: "", message: "Enter referral code now if you have one, otherwise just leave this blank", delegate: self, cancelButtonTitle: "Ok")
                        self.alertForgotPassword.alertViewStyle = UIAlertViewStyle.PlainTextInput;
                        self.alertForgotPassword.delegate = self;
                        self.alertForgotPassword.tag = 2;
                        self.alertForgotPassword.textFieldAtIndex(0)?.delegate = self
                        self.alertForgotPassword.textFieldAtIndex(0)?.keyboardType = UIKeyboardType.Default
                        self.alertForgotPassword.show()

                    }
                    else if result != nil
                    {
                        SVProgressHUD.showErrorWithStatus("User already exists")
                    }
                    else if error != nil
                    {
                        SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                    }
                }
            }
        }
        else
        {
            var msg:String
            msg = usernameTextField.text == "" ? "Please enter your Username"
                :  passwordTextField.text == "" ? "Please enter your Password"
                : emailTextField.text == "" ? "Please enter your Email" : ""
            
            UIAlertView(title: "Warning", message: msg, delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    // MARK: - Email Validation
    func isValidEmail(email: String) -> Bool
    {
        let emailRegex: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(email)
    }
    
    func isValidAge(age: String) -> Bool
    {
        let ageRegex: String = "^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$"
        let ageTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", ageRegex)
        return ageTest.evaluateWithObject(age)
    }
    
    
    //MARK:- UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        /*  if passwordTextField.isFirstResponder(){
        passwordTextField.resignFirstResponder()
        actionSignUp()
        }
        else if emailTextField.isFirstResponder(){
        emailTextField.resignFirstResponder()
        }
        else if phon
         eTextField.isFirstResponder(){
        usernameTextField.becomeFirstResponder()
        }
        else
        {
        usernameTextField.resignFirstResponder()
        passwordTextField.becomeFirstResponder()
        };    */
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        activetextField = textField
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == emailTextField{
            if !isValidEmail(emailTextField.text!){
                SVProgressHUD.showErrorWithStatus("Invalid Email")
                textField.text = ""
            }
        }
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == usernameTextField {
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 20
        }
        else{
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 35
        }
       // return true
    }

    //MARK:- FACEBOOK LOGIN
    @IBAction func onLoginWithFacebookButtonPressed (sender : UIButton)
    {
        if activetextField != nil { activetextField.resignFirstResponder() }
        let login : FBSDKLoginManager = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.Browser
        login.logOut()
        login.logInWithReadPermissions(["public_profile", "email" , "user_friends"], fromViewController: self) { (result, error) -> Void in
            if(error == nil && result.isCancelled == false){
                SVProgressHUD.showWithStatus("Processing...")
                let request = FBSDKGraphRequest(graphPath:"me", parameters:["fields" : "id, name, email"])
                request.startWithCompletionHandler {
                    (connection, result, error) in
                    if error != nil {
                       // SVProgressHUD.dismiss()
                    }
                    else if let userData = result as? [String:AnyObject] {
                        print("\(userData)")
                        self.fbLogin(userData)
                    }
                }
            }else{
                SVProgressHUD.showErrorWithStatus("Login Failed")
            }
        }
    }
    
    func  fbLogin(data : NSDictionary)  {
        let params  = NSMutableData.postData() as! NSMutableData
        params.addValue(data["name"] as! String, key: "username")
        params.addValue(data["email"] as! String, key: "email")
        params.addValue(data["id"] as! String, key: "facebookid")
        let imgData : NSData? = NSData(contentsOfURL: NSURL(string: "https://graph.facebook.com/\(data["id"] as! String)/picture?type=large&return_ssl_resources=1")!)
        let imag = UIImage(data: imgData!)
        params.addValue(UIImageJPEGRepresentation(imag!, 0.7)!, key: "profilePic", filename: User.makeFileName())
        var token: NSString?
        if  NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") != nil {
            token = NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! NSString!
        }
        token = token == nil ? "12345" : token
        params.addValue(token!, key: "devicetkn")

        ServiceManager.fetchDataFromService("apilinks/Register.php?", parameters: params) { (success, result, error) in
            if success{
                if result!["status"] as! Bool == true{
                    SVProgressHUD.dismiss()
                     print(result)
                    self.detailsDict  = NSMutableDictionary(dictionary:result!["details"] as! NSDictionary)
                    self.loggeduserId = self.detailsDict["id"] as! String
                    if self.detailsDict["firsttimelogin"] as! String == "yes" {
                        let oldUserName : String = (self.detailsDict["username"] as! String).stringByReplacingOccurrencesOfString(" ", withString: "")
                        self.alertForgotPassword = UIAlertView(title: "", message: "By default your username has been set to \(oldUserName). If you want to change this, type a new username below.", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Submit")
                        self.alertForgotPassword.alertViewStyle = UIAlertViewStyle.PlainTextInput;
                        self.alertForgotPassword.delegate = self;
                        self.alertForgotPassword.tag = 1;
                        self.alertForgotPassword.textFieldAtIndex(0)?.delegate = self
                        self.alertForgotPassword.textFieldAtIndex(0)?.keyboardType = UIKeyboardType.Default
                        self.alertForgotPassword.show()
                    }else{
                        User.userWithDetails(self.detailsDict)
                        User.saveUserDetails(self.detailsDict)
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.didLogin()
                    }
                    
                }else{
                    SVProgressHUD.showErrorWithStatus("Please try again")
                }
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }

}

extension SignUpTableViewController : UIAlertViewDelegate{
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.tag == 1{
        if(buttonIndex == 1 && alertForgotPassword.textFieldAtIndex(0)!.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 0){
            SVProgressHUD.showWithStatus("processing...")
            let params : NSMutableData = NSMutableData.postData() as! NSMutableData
            params.addValue(alertForgotPassword.textFieldAtIndex(0)!.text!, key: "username")
            params.addValue(loggeduserId, key: "userid")
            ServiceManager.fetchDataFromService("apilinks/EditUsername.php?", parameters: params, withCompletion: { (success, result, error) in
                if success{
                    print(result)
                    if result!["status"] as! Bool == true{
                     SVProgressHUD.dismiss()
                    self.detailsDict.setValue(self.alertForgotPassword.textFieldAtIndex(0)!.text!, forKey: "username")
                     User.userWithDetails(self.detailsDict)
                     User.saveUserDetails(self.detailsDict)
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.didLogin()

                    }else{
                        SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                    }
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            })

        }else{
            User.userWithDetails(self.detailsDict)
            User.saveUserDetails(self.detailsDict)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.didLogin()
        }
        }else{
        if alertForgotPassword.textFieldAtIndex(0)!.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 0{
            SVProgressHUD.showWithStatus("processing...")
            let params : NSMutableData = NSMutableData.postData() as! NSMutableData
            params.addValue(alertForgotPassword.textFieldAtIndex(0)!.text!, key: "code")
            params.addValue(loggeduserId, key: "userid")
            ServiceManager.fetchDataFromService("apilinks/referralcode.php?", parameters: params, withCompletion: { (success, result, error) in
                if success{
                    print(result)
                    if result!["status"] as! String == "success"{
                        SVProgressHUD.dismiss()
                        User.userWithDetails(self.detailsDict)
                        User.saveUserDetails(self.detailsDict)
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.didLogin()
                        
                    }else{
                        SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                    }
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            })

        }else{
            User.userWithDetails(self.detailsDict)
            User.saveUserDetails(self.detailsDict)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.didLogin()
            }
        }
        
    }
}

