//
//  MapViewExtension.swift
//  HotMaps
//
//  Created by Srishti Innovative on 1/29/16.
//  Copyright © 2016 Srishti Innovative. All rights reserved.
//

import MapKit

extension MKMapView {
    
    func containsAnnotation(annotation: MKAnnotation) -> Bool {
        
        if let existingAnnotations = self.annotations as? [Annotation]  {
            for existingAnnotation in existingAnnotations {
               if  existingAnnotation.coordinate.latitude == annotation.coordinate.latitude
                    && existingAnnotation.coordinate.longitude == annotation.coordinate.longitude {
                        return true
                }
            }
        }
        return false
    }
}