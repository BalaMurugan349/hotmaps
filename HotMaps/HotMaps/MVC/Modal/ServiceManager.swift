//
//  ServiceManager.swift
//  Closito
//
//  Created by Bose on 11/7/14.
//  Copyright (c) 2014 Srishti Innovative. All rights reserved.
//

import UIKit

class ServiceManager: NSObject, NSURLConnectionDataDelegate {
    var urlData : NSMutableData!
    var mainUrl : String!
    typealias completionBlock = (Bool? ,AnyObject? ,NSError?) -> Void
    var finishLoading: completionBlock!
    
    
    override init() {
        super.init()
        //setting main url
       // mainUrl = "http://sicsglobal.com/projects/App_projects/HotMaps/"
        mainUrl =   "http://52.43.177.218/HotMaps/"//"http://52.88.60.242/HotMaps/"  //"http://www.hotmapsapp.com/HotMaps/"
    }
    
    class func fetchDataFromService (serviceName: String, parameters : NSData?, withCompletion :(success : Bool, result: AnyObject?, error: NSError?)-> Void){
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        let serviceManager = ServiceManager()
        serviceManager.httpRequestForService(serviceName, parameters: parameters) { (success, result, error) -> Void in
            withCompletion(success: success!, result: result, error: error)
        }
    }
    
    func httpRequestForService(serviceName: String, parameters : NSData?, completion :(success : Bool?, result: AnyObject?, error: NSError?)-> Void) {
        finishLoading = completion
        let url = NSURL(string: String("\(mainUrl)\(serviceName)"))
        let request = prepareRequestWithURL(url!, parameters: parameters)
        let urlConnection = NSURLConnection(request: request, delegate: self, startImmediately: true)
    }
    
    
    func prepareRequestWithURL(url: NSURL, parameters : NSData?) -> NSURLRequest {
        let request = NSMutableURLRequest()
        request.URL = url
        if(parameters != "")
        {
            request.HTTPMethod = "POST"
            let charset = CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding))
            let contentType = String("multipart/form-data; charset=\(charset); boundary=0xKhTmLbOuNdArY")
            request.addValue(contentType, forHTTPHeaderField: "Content-Type")
            request.HTTPBody = parameters
        }
        return request
    }
    
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        urlData = NSMutableData()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        urlData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
       /* var error:NSError? = NSError()
        var json: AnyObject? = NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions.allZeros, error: &error)
        finishLoading(true , json, nil)*/
        var error : NSError?
        var json: AnyObject?
        do {
            json = try NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions())
        } catch let error1 as NSError {
            error = error1
            json = nil
        }
        finishLoading(true , json, nil)
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        finishLoading(false, nil, error)
    }
    
    class func  handleError(error:NSError?)
    {
       UIAlertView(title: "Alert!", message: error?.localizedDescription, delegate: nil, cancelButtonTitle: "OK").show()
    }
}
