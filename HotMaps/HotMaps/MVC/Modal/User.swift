//
//  User.swift
//  HotMaps
//
//  Created by Srishti Innovative on 21/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

var _user: User?
var _imagePathUser = "http://52.43.177.218/HotMaps/apilinks/"
//var _imagePathLocation = "http://52.88.60.242/HotMaps/apilinks/images/users/location/"

class User: NSObject {
   
    var userId: NSString!
    var email: NSString!
    var phone: NSString!
    var username: NSString!
    var password: NSString!
    var rank: NSString!
    var points: NSString!
    var facebookId : String!
    
    override init() {
        super.init()
    }
    
    //Store User Details
    init(dict : Dictionary<String, String>) {
        super.init()
        self.userId = dict["id"]
        self.email = dict["email"]
        self.username = dict["username"]
        self.password = dict["password"]
        self.phone = dict["phone_no"]
        self.rank = ""
        self.points = dict["points"]
        self.facebookId = dict["facebookid"] != nil ? dict["facebookid"]! : ""
    }
    
    //Singleton Function
    class func currentUser() -> User
    {
        if (_user == nil)
        {
            _user = User()
        }
        return _user!
    }
    
    //MARK:- MAKE FILE NAME
    class func makeFileName() -> String{
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let dateString = dateFormatter.stringFromDate(NSDate())
        let randomValue  = arc4random() % 1000
        let fileName : String = "B\(dateString)\(randomValue).jpg"
        return fileName
    }
    
    class func userWithDetails (userDetails : NSDictionary) -> User
    {
        _user = User(dict: userDetails as! Dictionary<String, String>)
        return _user!
    }
    
    class func saveUserDetails(userDetails : NSDictionary){
        let userData = NSKeyedArchiver.archivedDataWithRootObject(userDetails)
        NSUserDefaults.standardUserDefaults().setObject(userData, forKey: "CurrentUserDetails")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    class func logout(){
        _user = nil
    }
   
}
