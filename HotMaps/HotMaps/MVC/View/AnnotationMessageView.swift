//
//  AnnotationMessageView.swift
//  HotMaps
//
//  Created by Srishti Innovative on 06/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

protocol AnnotationViewDelegate
{
    func annotationClicked(sender: UIButton)
}
class AnnotationMessageView: UIView {
    @IBOutlet weak var btn: UIButton!
     @IBOutlet weak var annotBg: UIImageView!
    var delegate: AnnotationViewDelegate?
    
    //MARK:- setMessage
    
    func setMessage(location: LocationDetails){
        
       
        
//        let imageViewTrend = self.viewWithTag(11) as! UIImageView
        let locationName = self.viewWithTag(12) as! UILabel
        let likePercent = self.viewWithTag(13) as! UILabel
       // let bg = self.viewWithTag(15) as! UIImageView
        
//        imageViewTrend.image = location.trendUpValue.integerValue > location.trendDownValue.integerValue ?
//        UIImage(named: "up") : UIImage(named: "down")
        
        let locationNameStr = location.locationName as String
        let locationNameArr = locationNameStr.componentsSeparatedByString(",")
        let loc = locationNameArr[0]
        
        locationName.text = loc
        btn.tag = location.id.integerValue
        
        
        likePercent.text = location.likePercent as String
        likePercent.textColor = location.likePercent.integerValue >= 50 ? UIColor(red: 0, green: 147/255.0, blue: 0, alpha: 1.0) : UIColor.redColor()
       
        let width = locationName.intrinsicContentSize().width
        likePercent.frame = CGRectMake(CGRectGetMinX(locationName.frame) + width + 6, likePercent.frame.origin.y, likePercent.frame.size.width, likePercent.frame.size.height)
        
 self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, CGRectGetMaxX(likePercent.frame) - 10, self.frame.size.height)
    
        //  likePercent.text = String(format: "\(location.likePercent)") + "%"
      //  likePercent.textColor = location.likePercent.integerValue >= 50 ? UIColor(red: 0, green: 147/255.0, blue: 0, alpha: 1.0) : UIColor.redColor()
        
    }
    
    @IBAction func annotationPressed (sender : UIButton){
        self.delegate!.annotationClicked(sender)
    }
    
    //MARK:- Calculate height of Label
    
    func messageSize(message : NSString, font : UIFont, width : CGFloat) -> CGSize
    {
        let darwOptions: NSStringDrawingOptions = [.UsesLineFragmentOrigin , .UsesFontLeading]
        return message.boundingRectWithSize(CGSizeMake(width, CGFloat.max), options: darwOptions, attributes: NSDictionary(object: font, forKey: NSFontAttributeName) as? [String : AnyObject], context: nil).size
        
        
//        return message.boundingRectWithSize(CGSizeMake(width, CGFloat.max), options: darwOptions, attributes: NSDictionary(objectsAndKeys: font, NSFontAttributeName) as [NSObject : AnyObject], context: nil).size
    }

}
