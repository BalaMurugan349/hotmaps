//
//  AnnotationView.swift
//  HotMaps
//
//  Created by Srishti Innovative on 06/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class AnnotationView: MKAnnotationView {

    var pinImage : UIImageView!
    override init(annotation: MKAnnotation!, reuseIdentifier: String!) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        pinImage = UIImageView()
    }
//    init(frame: CGRect) {
//        super.init(frame: frame)
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
}
