//
//  BMImageView.swift
//  HotMaps
//
//  Created by Bala on 04/04/2016.
//  Copyright © 2016 Srishti Innovative. All rights reserved.
//

import UIKit

class BMImageView: UIImageView {

    @IBInspectable var cornerRadius:CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }

}
