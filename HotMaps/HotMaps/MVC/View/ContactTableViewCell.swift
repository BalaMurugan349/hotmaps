//
//  ContactTableViewCell.swift
//  HotMaps
//
//  Created by Srishti Innovative on 10/28/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    @IBOutlet weak var labelContactName: UILabel!
    @IBOutlet weak var imageViewTickMark: UIImageView!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelPhone: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
