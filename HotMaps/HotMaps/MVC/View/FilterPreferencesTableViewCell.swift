//
//  FilterPreferencesTableViewCell.swift
//  HotMaps
//
//  Created by Srishti Innovative on 07/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//


import UIKit

class FilterPreferencesTableViewCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var sliderPreference: ANPopoverSlider!
    @IBOutlet weak var ageRangeLabel1: UILabel!
    @IBOutlet weak var ageRangeLabel2: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var ageValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

        ageRangeLabel1.hidden = true
        ageRangeLabel2.hidden = true
        
        ageValueLabel = UILabel(frame: CGRectMake(0, 0, 21, 21))
        ageValueLabel.textColor = UIColor.blueColor()
        ageValueLabel.textAlignment = NSTextAlignment.Center
        ageValueLabel.font = UIFont.init(name: "Futura-Medium", size: 14)
    }

//    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    override func prepareForReuse() {
        leftLabel.text = ""
        rightLabel.text = ""

    }

//    func initWithDetails(filterDetails: NSDictionary){
//       leftLabel.text = filterDetails.objectForKey("left") as? String
//       rightLabel.text = filterDetails.objectForKey("right") as? String
//       
//    }
//    
    func initWithLikeDetails(filterDetails: NSDictionary){
        let likeAttachment = NSTextAttachment()
        likeAttachment.image = UIImage(named: "up1")
        let likeAttachmentString = NSAttributedString(attachment: likeAttachment)
      
        let dislikeAttachment = NSTextAttachment()
        dislikeAttachment.image = UIImage(named: "down1")
        let dislikeAttachmentString = NSAttributedString(attachment: dislikeAttachment)
        
        leftLabel.attributedText = dislikeAttachmentString
        rightLabel.attributedText = likeAttachmentString
        
        topConstraint.constant = 6
        
               
    }

    
    
}
