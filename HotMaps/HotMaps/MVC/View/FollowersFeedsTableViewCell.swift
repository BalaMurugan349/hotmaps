//
//  FollowersFeedsTableViewCell.swift
//  HotMaps
//
//  Created by Bala on 04/04/2016.
//  Copyright © 2016 Srishti Innovative. All rights reserved.
//

import UIKit

class FollowersFeedsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewProfile : UIImageView!
    @IBOutlet weak var labelLikeCount : UILabel!
    @IBOutlet weak var labelLocationName : UILabel!
    @IBOutlet weak var labelLocationTextRating : UILabel!
    @IBOutlet weak var labelUserName : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(dict : NSDictionary) {
        self.labelLikeCount.text = dict["likecount"] as? String
        let loc = dict["locationname"] as? String == nil ? "" : dict["locationname"] as? String
        if loc != ""{
            let strArray = loc!.componentsSeparatedByString("_") as NSArray
             self.labelLocationName.text = strArray[0] as? String
        }
        else{
             self.labelLocationName.text = ""
        }
       // print("location id == \(dict["id"] as! String)  date == \(dict["date"]as! String) userid == \(dict["userid"] as! String)")
        let imageName : String = dict["profileimage"] as! String
        self.imageViewProfile.sd_setImageWithURL(NSURL(string: _imagePathUser + imageName), placeholderImage: UIImage(named: "UserPlaceholder"))
        self.labelUserName.text = dict["username"] as? String

        
        self.labelLocationTextRating.text =  dict["location_description"] as? String

        
    }

}
