//
//  RankTableViewCell.swift
//  HotMaps
//
//  Created by Srishti Innovative on 30/09/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class RankTableViewCell: UITableViewCell {

    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelPoints: UILabel!
    @IBOutlet weak var labellastRatedLocation: UILabel!
    @IBOutlet weak var labelRank: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
