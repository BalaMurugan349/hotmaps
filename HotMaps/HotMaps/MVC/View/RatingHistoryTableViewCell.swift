//
//  RatingHistoryTableViewCell.swift
//  HotMaps
//
//  Created by Srishti Innovative on 27/07/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit

class RatingHistoryTableViewCell: UITableViewCell {

   // @IBOutlet weak var thumbsUpStatusImageView: UIImageView!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var labelLikeCount: UILabel!
    @IBOutlet weak var buttonUserName : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
