//
//  TopLocationsTableViewCell.swift
//  HotMaps
//
//  Created by Srishti Innovative on 30/06/15.
//  Copyright (c) 2015 Srishti Innovative. All rights reserved.
//

import UIKit
let screenSize = UIScreen.mainScreen().bounds.size
class TopLocationsTableViewCell: UITableViewCell {

    @IBOutlet weak var trendImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
   
    var distanceLabel: UILabel!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var likeImage: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        distanceLabel = UILabel(frame: CGRectMake(0, 9, screenSize.width / 5 + 5, screenSize.width / 7.4))
        distanceLabel.numberOfLines = 1
        distanceLabel.font = UIFont.init(name: "Futura-Medium", size: 13)
        distanceLabel.textColor = UIColor.whiteColor()
        self.contentView.addSubview(distanceLabel)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
